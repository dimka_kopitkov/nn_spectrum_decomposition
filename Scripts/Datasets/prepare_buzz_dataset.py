import pandas as pd
import numpy as np

# Downloaded from:
# http://ama.liglab.fr/data/buzz/regression/Twitter/
data = pd.read_table('./Twitter.data', sep=',')


rows = data.iloc[ : ,0:78]
rows = np.copy(rows)
np.random.shuffle(rows)

points = rows[:,0:77]
labels = rows[:,77:78]

content = dict();
content['points'] = points
content['labels'] = labels

np.savez('buzz.npz', **content)