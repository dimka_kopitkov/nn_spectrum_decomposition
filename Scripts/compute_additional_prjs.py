import argparse
import numpy as np
import sys
import os
sys.path.insert(0, './Utils/')
from scipy.sparse.linalg import eigsh
from scipy.linalg import eigh
from os import listdir
from os.path import isfile, join
from utils import *
from scipy.linalg import svdvals
import scipy as sp


# Load configuration
config = read_config('./train.properties')
res_dir = config['res_dir']
opt_task = config['opt_task']
files_dir = res_dir + '/nn_eigs_dec/'

info_file = np.load(files_dir + '/summery.npz')
Ms = info_file['Ms']
ts = info_file['ts']

basename = 'nn_cov_'
onlyfiles = [f for f in listdir(files_dir) if isfile(join(files_dir, f))]
onlyfiles = [f for f in onlyfiles if f.startswith(basename) and f.endswith(".npz")]
onlyfiles = sorted(onlyfiles)

Ms_prjs_list = []
for i, f_name in enumerate(onlyfiles):
    #if i > 3:
    #    break;
    dt_file = np.load(join(files_dir, f_name))
    curr_vecs = dt_file['eigenvecs']
    curr_vecs = curr_vecs.T

    Ms_prjs = np.matmul(curr_vecs.astype(np.float64).T, Ms.astype(np.float64).T)
    Ms_prjs_list.append(Ms_prjs)

    print(f_name)


# First dimension - time of G
# Second dimension - N spectral directions with the projections
# Third dimension - time of m
all_Ms_prjs = np.stack(Ms_prjs_list)
all_Ms_prjs = all_Ms_prjs.astype(np.float32)

res_file = join(files_dir, 'more_projections.npz')
content = dict();
content['all_Ms_prjs'] = all_Ms_prjs
np.savez(res_file, **content)

    
print('end')


