import sys
import random as rn
import numpy as np
from shutil import copyfile
np.random.seed(1000)
rn.seed(1254)
import tensorflow as tf
tf.set_random_seed(1000)
from tensorflow.keras import datasets
sys.path.insert(0, './Models/')
sys.path.insert(0, './Utils/')
from utils import *
from image_utils import *
from ImageFnctn import *
from CNNModel import *
from NNSpectrumCollector import *


# Read run configuration
config_fn = './train.properties'
config = read_config(config_fn)
ensure_path_exists(res_dir)
copyfile(config_fn, res_dir + config_fn)
model_dir = res_dir + '/trained_model/'
ensure_path_exists(model_dir)
temp_dir = config['temp_dir']
ensure_path_exists(temp_dir)
image_output_dir = res_dir + '/images/'
ensure_path_exists(image_output_dir)
it_num = int(float(config['it_num']))
dtype = np.float32
dtype_tf = npdtype_2_tfdtype(dtype)
lrate = float(config['lrate'])


def reduce_dataset_size(train_images, train_labels, dataset_size):
    class_num = np.unique(train_labels).shape[0]
    points_per_class = int(dataset_size/class_num)

    points_to_use = []
    labels_to_use = []
    for curr_class in range(class_num):
        point_indeces = np.where(train_labels == curr_class)[0]
        point_indeces_to_use = point_indeces[0:points_per_class]
        curr_points = train_images[point_indeces_to_use]
        curr_labels = train_labels[point_indeces_to_use]
        points_to_use.append(curr_points)
        labels_to_use.append(curr_labels)
    reduced_train_images = np.concatenate(points_to_use)
    reduced_train_labels = np.concatenate(labels_to_use)
    return [reduced_train_images, reduced_train_labels]

# Prepare dataset
dataset_size = int(float(config['dataset_size']))
(train_images, train_labels), (test_images, test_labels) = datasets.cifar100.load_data()
[train_images, train_labels] = reduce_dataset_size(train_images, train_labels, dataset_size)
[test_images, test_labels] = reduce_dataset_size(test_images, test_labels, dataset_size)
train_map = dict()
train_map['points'] = train_images
train_map['labels'] = train_labels
train_map['test_points'] = test_images
train_map['test_labels'] = test_labels
generated_dataset_file = res_dir + 'generated_dataset.npz'
np.savez(generated_dataset_file, **train_map)


def construct_loss(points, labels, model):
    X_tensor = tf.convert_to_tensor(points, dtype=dtype_tf)
    Y_tensor = tf.convert_to_tensor(labels, dtype=dtype_tf)

    nn_output = model.create_model_graph(X_tensor)

    # Implement simple binary cross-entropy loss:
    # First 5 classes will be represented by 0
    # Last 5 classes will be represented by 1
    # labels_bool = Y_tensor >= 5
    # labels = tf.cast(labels_bool, dtype_tf)
    # cross_ent_errors = tf.nn.sigmoid_cross_entropy_with_logits(labels = labels, logits = nn_output)
    # cross_ent_loss = tf.reduce_mean(cross_ent_errors)
    # f_sigmoid = tf.math.sigmoid(nn_output)
    # predictions = f_sigmoid > 0.5
    # equality = tf.equal(predictions, labels_bool)
    # accuracy = tf.reduce_mean(tf.cast(equality, tf.float32)
    # return cross_ent_loss, accuracy

    sqr_errors = tf.square(nn_output - Y_tensor)
    sqr_loss = 0.5*tf.reduce_mean(sqr_errors)
    accuracy = tf.no_op()

    return sqr_loss, accuracy


def construct_lr_tensor(init_lrate, global_step):
    return tf.constant(init_lrate, dtype=dtype_tf)

    lr_tensor = tf.train.exponential_decay(
                                        learning_rate = init_lrate,
                                        global_step = global_step,
                                        decay_steps = 200000,
                                        decay_rate = 0.5,
                                        staircase = True)

    return lr_tensor


# Create model on gpu for better performance
with tf.device(config['device']):
    model = CNNModel(config)

    # Create train and test losses
    train_loss, train_accuracy = construct_loss(train_map['points'], train_map['labels'], model)
    test_loss, test_accuracy = construct_loss(train_map['test_points'], train_map['test_labels'], model)

    # Define learning rate
    global_step = tf.train.get_or_create_global_step()
    lr_tensor = construct_lr_tensor(lrate, global_step)

    # Define an optimizer
    optimizer = tf.train.GradientDescentOptimizer(learning_rate=lr_tensor)
    total_model_vars = model.get_list_of_model_variables()
    print_dim_of_variables(total_model_vars)
    train_op = optimizer.minimize(train_loss, var_list = total_model_vars, global_step = global_step)

    # Create spectrum collector
    files_output_dir = res_dir + '/nn_stats/'
    ensure_path_exists(files_output_dir)
    spec_collector = NNSpectrumCollector(dtype = dtype, output_dir = files_output_dir, config = config, train_map = train_map)
    spec_collector.create_tensors(model, lr_tensor)



saver = tf.train.Saver(total_model_vars)
fmt = PartialFormatter()

with tf.Session() as sess:
    # Intialize the model
    sess.run(tf.global_variables_initializer())
        
    # Optimize NN
    ts = []
    train_losses = []
    train_accurcs = []
    test_losses = []
    test_accurcs = []
    for step in range(it_num):

        # Collect NN spectrum
        spec_collector.update_before_iteration_start(sess, step);

        # Compute current performance
        if (step % 100 == 0) or (step == it_num - 1):
            data = sess.run([train_loss, train_accuracy, test_loss, test_accuracy, lr_tensor])
            curr_train_loss = data[0]
            curr_train_accuracy = data[1]
            curr_test_loss = data[2]
            curr_test_accuracy = data[3]
            curr_lr = data[4]
            ts.append(step)
            train_losses.append(curr_train_loss)
            train_accurcs.append(curr_train_accuracy)
            test_losses.append(curr_test_loss)
            test_accurcs.append(curr_test_accuracy)

            #msg = "\r- Iteration: {0:>6}, Train Cross Entropy Loss is: {1:<13}, Accuracy is: {2:<13}, Learning rate: {3:<13}".format(step, curr_loss, curr_accuracy, curr_lr)
            #msg = "\n Iteration: {0:>6}, Train Loss is: {1:<10}, Train Accuracy is: {2:<10}, Test Loss is: {3:<10}, Test Accuracy is: {4:<10}, Learning rate: {5:<13}".format(
            #    step, curr_train_loss, curr_train_accuracy, curr_test_loss, curr_test_accuracy, curr_lr)
            msg = fmt.format(
                "\n Iteration: {0:>6}, Train Loss is: {1:<10}, Train Accuracy is: {2:<10}, Test Loss is: {3:<10}, Test Accuracy is: {4:<10}, Learning rate: {5:<13}",
                step, curr_train_loss, curr_train_accuracy, curr_test_loss, curr_test_accuracy, curr_lr)
            sys.stdout.write(msg)
            sys.stdout.flush()

        # Perform training operation
        data = sess.run([train_op])

    # Save performance during the optimization to file
    ts = np.array(ts)
    train_losses = np.array(train_losses)
    train_accurcs = np.array(train_accurcs)
    test_losses = np.array(test_losses)
    test_accurcs = np.array(test_accurcs)
    losses_file = res_dir + 'losses.npz'
    content = dict();
    content['ts'] = ts
    content['train_losses'] = train_losses
    content['train_accurcs'] = train_accurcs
    content['test_losses'] = test_losses
    content['test_accurcs'] = test_accurcs
    np.savez(losses_file, **content)

    # Save the variables to disk.
    save_path = saver.save(sess, model_dir + "model.ckpt")
    print("Model saved in path: %s" % save_path)