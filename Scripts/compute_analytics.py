import sys
import os
sys.path.insert(0, './Utils/')
from utils import *


scripts_path = os.getcwd()
def perform_script(script_filename):
    script_path = scripts_path + '/Analyze/'
    os.chdir(script_path)
    os.system("python " + script_filename)


config = read_config('./train.properties')
opt_task = config['opt_task']

# Create all figures and plots
perform_script('draw_train_loss.py')
perform_script('draw_test_loss.py')
perform_script('draw_lrate.py')
perform_script('compute_df_approximation_error.py')
perform_script('draw_kernel_histograms.py')
perform_script('draw_signal_energy.py')
perform_script('draw_signal_norm.py')
perform_script('draw_eigvals.py')
perform_script('draw_spectrum_preservation.py')
perform_script('draw_M_energy_evolution.py')
perform_script('draw_kernel_histograms.py')

if (opt_task == 'regression'):
    perform_script('draw_samples.py')
    perform_script('draw_eigvecs.py')
    perform_script('draw_kernel_per_point.py')
    perform_script('draw_differentials.py')
    perform_script('draw_M_frequency_evolution.py')
