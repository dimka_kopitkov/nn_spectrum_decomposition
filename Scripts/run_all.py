import sys
import os
sys.path.insert(0, './Utils/')
from utils import *


scripts_path = os.getcwd()
def perform_script(script_filename):
    os.chdir(scripts_path)
    os.system("python " + script_filename)


config = read_config('./train.properties')
opt_task = config['opt_task']

# Run all

# Train model
if (opt_task == 'classification'):
    perform_script('train_model_classification.py')
elif (opt_task == 'regression'):
    perform_script('train_model_regression.py')
elif (opt_task == 'nce'):
    perform_script('train_model_nce.py')

# Compute eigendecomposition of all Gramians
perform_script('compute_eigs_dcmpstn.py')

# Compute additional projections between Ms and Gs
perform_script('compute_additional_prjs.py')

# Compute analytics / draw figures for the paper
perform_script('compute_analytics.py')
