import numpy as np
import sys
import os
os.chdir('../')
sys.path.insert(0, './Utils/')
from os import listdir
from os.path import isfile, join
from utils import *
import matplotlib.pyplot as plt
plt.ioff()
from matplotlib import rc
#rc('font',**{'family':str('sans-serif'),'sans-serif':['Helvetica'], 'size': 30})
#rc('font',**{'family':'serif','serif':['Palatino'], 'size': 30})
#rc('text', usetex=True)
plt.rcParams.update({'font.size': 20})
#plt.rcParams.update({'font.weight': 'bold'})


# Load configuration
config = read_config('./train.properties')
res_dir = config['res_dir']
eig_path = res_dir + '/nn_eigs_dec/'

output_path = res_dir + '/df_approximation_error'
ensure_path_exists(output_path)

summ_file = np.load(eig_path + '/summery.npz')
ts = summ_file['ts']
fs = summ_file['fs']
GMs = summ_file['GMs']
lrates = summ_file['lrates']
points_num = fs.shape[1]


# Times at which to compute differential
before_times = np.arange(np.min(ts), np.max(ts), 5000)
after_times = before_times + 1
res = np.intersect1d(ts, before_times, return_indices=True)
before_indeces = res[1]
before_times = ts[before_indeces]
res = np.intersect1d(ts, after_times, return_indices=True)
after_indeces = res[1]
after_times = ts[after_indeces]

# Compute real differentials
f_before = fs[before_indeces,:]
f_after = fs[after_indeces,:]
real_differential = f_after - f_before

# Compute estimated differentials
GMs_before = GMs[before_indeces,:]
lrates_before = lrates[before_indeces]
est_differential = -GMs_before*lrates_before[:,np.newaxis]/float(points_num)

# Compute approximation error between real differential and estimated differential
real_norm = np.linalg.norm(real_differential, axis = 1)
est_norm = np.linalg.norm(est_differential, axis = 1)
appr_error = np.linalg.norm(real_differential - est_differential, axis = 1)/real_norm
dot_pr = np.sum(real_differential*est_differential, axis = 1)
angle_cos = dot_pr/(real_norm*est_norm)

# Plot approximation error
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(before_times, appr_error, linewidth=2)
plt.xlabel('$t$');
plt.ylabel('$error_t$');
filename = output_path + '/df_first_order_approximation.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()


# Plot angle cosine similarity
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(before_times, angle_cos, linewidth=2)
plt.xlabel('$t$');
plt.ylabel('$\\cos(\\alpha_t)$');
filename = output_path + '/df_cos_alpha.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()


# Plot angle cosine distance
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(before_times, 1.0 - angle_cos, linewidth=2)
plt.xlabel('$t$');
plt.ylabel('$1 - \\cos(\\alpha_t)$');
filename = output_path + '/df_cos_alpha_dist.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()



# Plot approximation error together with angle cosine similarity
fig, axes = plt.subplots(nrows=2, ncols=1)

ax = axes[0]
for x in range(100000, 500001, 100000):
    ax.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
ax.plot(before_times, appr_error, linewidth=2)
ax.set_ylabel('$error_t$');
ax.tick_params(
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom=False,      # ticks along the bottom edge are off
    top=False,         # ticks along the top edge are off
    labelbottom=False)

ax = axes[1]
for x in range(100000, 500001, 100000):
    ax.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
ax.plot(before_times, angle_cos, linewidth=2)
ax.set_xlabel('$t$');
ax.set_ylabel('$\\cos(\\alpha_t)$');

plt.subplots_adjust(hspace=0.1)
filename = output_path + '/df_first_order_approximation_and_cos.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()


# Plot norm of real differential
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(before_times, real_norm, linewidth=2)
plt.xlabel('$t$');
plt.ylabel('$|d f_{t}|$');
ax.set_yscale('log')
filename = output_path + '/df_real_norm.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()

# Plot norm of estimated differential
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(before_times, est_norm, linewidth=2)
plt.xlabel('$t$');
plt.ylabel('$|d f_{t}|$');
ax.set_yscale('log')
filename = output_path + '/df_est_norm.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()

# Plot norm ratio
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(before_times, np.log(est_norm) - np.log(real_norm), linewidth=2)
plt.xlabel('$t$');
plt.ylabel('$\\log \\frac{|d \\bar{f}_{t}|}{|d \\tilde{f}_{t}|}$');
#ax.set_yscale('log')
filename = output_path + '/df_norm_ratio.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()


