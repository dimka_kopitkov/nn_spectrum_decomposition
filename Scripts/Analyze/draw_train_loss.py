import numpy as np
import sys
import os
os.chdir('../')
sys.path.insert(0, './Utils/')
from os import listdir
from os.path import isfile, join
from utils import *
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 30})


# Load configuration
config = read_config('./train.properties')
res_dir = config['res_dir']
opt_task = config['opt_task']
eig_path = res_dir + '/nn_eigs_dec/'

output_path = res_dir + '/figures'
ensure_path_exists(output_path)

summ_file = np.load(eig_path + '/summery.npz')
ts = summ_file['ts']
fs = summ_file['fs']

info_file = np.load(res_dir + '/nn_stats/nn_spectrum_info.npz')
labels = info_file['labels']
labels = labels.ravel()

# Times at which to consider NN dynamics
draw_times = np.arange(np.min(ts), np.max(ts), 5000)
res = np.intersect1d(ts, draw_times, return_indices=True)
draw_indeces = res[1]

# Compute train loss
if (opt_task == 'classification'):
    labels = labels >= 5
    labels = labels.astype(fs.dtype)
    # Using numerically stable implementation of cross-entropy loss:
    # https://www.tensorflow.org/api_docs/python/tf/nn/sigmoid_cross_entropy_with_logits
    ce_error_pos = fs - fs*labels[np.newaxis, :] + np.log1p(np.exp(-fs))
    ce_error_neg = np.maximum(fs, 0) - fs * labels[np.newaxis, :] + np.log1p(np.exp(-np.abs(fs)))
    ce_error_pos[fs<0.0] = 0.0
    ce_error_neg[fs >= 0.0] = 0.0
    ce_error = ce_error_pos + ce_error_neg
    train_loss = np.mean(np.square(ce_error), axis=1)
else:
    residual = fs - labels[np.newaxis,:]
    train_loss = 0.5*np.mean(np.square(residual), axis = 1)

# Plot training loss
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(ts[draw_indeces], train_loss[draw_indeces], linewidth=2)
plt.yscale('log')
plt.xlabel('$t$');
plt.ylabel('Train Loss');
filename = output_path + '/train_loss.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()
