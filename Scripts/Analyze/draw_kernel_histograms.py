import sys
import os
os.chdir('../')
import os.path
import numpy as np
import sklearn
sys.path.insert(0, './Utils/')
from utils import *
from image_utils import *
import matplotlib.pyplot as plt
plt.ioff()
plt.rcParams.update({'font.size': 30})


config = read_config('./train.properties')
output_path = config['res_dir']
eig_path = output_path + '/nn_eigs_dec'

info_file = np.load(output_path + '/nn_stats/nn_spectrum_info.npz')
points_dist = info_file['dist']
points = info_file['points']
points_num = points.shape[0]

# Output path
kernel_draw_path = output_path + '/kernel_histograms'
ensure_path_exists(kernel_draw_path)

def reject_outliers(data, m = 2.0):
    d = np.abs(data - np.median(data))
    mdev = np.median(d)
    s = d/mdev if mdev else 0.0
    valid_data_indeces = np.where(s < m)[0]
    return valid_data_indeces

# Steps at which to draw point kernels
steps = [0, 1, 10, 100, 5000, 10000, 20000, 50000, 100000, 105000, 120000, 200000, 300000, 400000, 500000, 595000]
for s in steps:
    step = str(s).zfill(8)
    curr_eig_file_name = eig_path + '/nn_cov_' + step + '.npz'
    if (not os.path.isfile(curr_eig_file_name)):
        continue;

    curr_eig_file = np.load(curr_eig_file_name)
    curr_vecs = curr_eig_file['eigenvecs']
    curr_vecs = curr_vecs.T
    curr_vals = curr_eig_file['eigenvals']
    curr_G = np.matmul(curr_vecs*curr_vals[np.newaxis,:], curr_vecs.T)

    curr_draw_path = kernel_draw_path + '/time_' + step
    ensure_path_exists(curr_draw_path)

    # Draw a histogram of point distance and kernel value
    uni_indeces = np.triu_indices(points_num);
    G_values = curr_G[uni_indeces];
    points_dist_values = points_dist[uni_indeces];
    data_indeces = reject_outliers(G_values, m=4.0)
    G_values = G_values[data_indeces]
    points_dist_values = points_dist_values[data_indeces]

    fig = plt.figure(figsize=(8, 6))
    ax = fig.add_subplot(111)
    plt.hist2d(points_dist_values, G_values, bins=100)
    #divider = make_axes_locatable(ax)
    #cax = divider.append_axes("right", size="5%", pad=0.05)
    #cbar = plt.colorbar(cax=cax)
    plt.xlabel('$d(X,X\')$');
    plt.ylabel('$g_{\\theta}(X,X\')$');
    filename = curr_draw_path + '/kernel_dist_hist.eps'
    plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
    plt.close()

    rel_G = curr_G/(np.diag(curr_G)[np.newaxis,:])
    G_values = rel_G[uni_indeces];
    points_dist_values = points_dist[uni_indeces];
    data_indeces = reject_outliers(G_values, m=4.0)
    G_values = G_values[data_indeces]
    points_dist_values = points_dist_values[data_indeces]

    fig = plt.figure(figsize=(8, 6))
    ax = fig.add_subplot(111)
    plt.hist2d(points_dist_values, G_values, bins=100)
    plt.xlabel('$d(X,X\')$');
    plt.ylabel('$\\frac{g_{\\theta}(X,X\')}{g_{\\theta}(X,X)}$');
    filename = curr_draw_path + '/kernel_rel_dist_hist.eps'
    plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
    plt.close()

    # Draw a histogram of kernel sigma^2 (bandwidth)

    # Filter negative similarities
    uni_indeces = np.triu_indices(points_num, k = 1);
    rel_log_G = np.log(curr_G) - np.log(np.diag(curr_G)[np.newaxis, :])
    log_G_values = rel_log_G[uni_indeces];
    points_dist_values = points_dist[uni_indeces];
    valid_indeces = np.where(np.isfinite(log_G_values))[0]
    log_G_values = log_G_values[valid_indeces]
    points_dist_values = points_dist_values[valid_indeces]
    var_values = np.square(points_dist_values)/log_G_values
    var_values = var_values*(-0.5)
    data_indeces = reject_outliers(var_values, m=3.0)
    var_values_filtered = var_values[data_indeces]

    fig = plt.figure(figsize=(8, 6))
    ax = fig.add_subplot(111)
    plt.hist(var_values_filtered, bins=100)
    plt.xlabel('$\sigma^2$');
    filename = curr_draw_path + '/kernel_variance_hist_filtered.eps'
    plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
    plt.close()

    # Use absolute value of similarities
    uni_indeces = np.triu_indices(points_num, k = 1);
    rel_log_G = np.log(np.abs(curr_G)) - np.log(np.diag(curr_G)[np.newaxis, :])
    log_G_values = rel_log_G[uni_indeces];
    points_dist_values = points_dist[uni_indeces];
    valid_indeces = np.where(np.isfinite(log_G_values))[0]
    log_G_values = log_G_values[valid_indeces]
    points_dist_values = points_dist_values[valid_indeces]
    var_values = np.square(points_dist_values)/log_G_values
    var_values = var_values*(-0.5)
    data_indeces = reject_outliers(var_values, m=3.0)
    var_values_filtered = var_values[data_indeces]

    fig = plt.figure(figsize=(8, 6))
    ax = fig.add_subplot(111)
    plt.hist(var_values_filtered, bins=100)
    plt.xlabel('$\sigma^2$');
    filename = curr_draw_path + '/kernel_variance_hist_abs.eps'
    plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
    plt.close()
