import numpy as np
import sys
import os
os.chdir('../')
sys.path.insert(0, './Utils/')
from os import listdir
from os.path import isfile, join
from utils import *
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 30})


# Load configuration
res_dir = '/media/dima/Storage01/NNSpectrum'
path_2L = res_dir + '/TrainOutcome_2L_256W_losses/'
path_4L = res_dir + '/TrainOutcome_4L_256W_losses/'
path_6L = res_dir + '/TrainOutcome_6L/'
path_2L_33000 = res_dir + '/TrainOutcome_2L_33000W_losses/'
path_2L_66000 = res_dir + '/TrainOutcome_2L_66000W_losses/'

output_path = res_dir + '/figures'
ensure_path_exists(output_path)


def get_testing_error_and_eigvals(path):
    summ_file = np.load(path + '/losses.npz')
    ts = summ_file['ts']
    test_loss = summ_file['test_losses']

    return [ts, test_loss]

# Compute testing error for each trained NN
[ts, te_2L] = get_testing_error_and_eigvals(path_2L)
[ts, te_4L] = get_testing_error_and_eigvals(path_4L)
[ts, te_6L] = get_testing_error_and_eigvals(path_6L)
[ts, te_2L_33000] = get_testing_error_and_eigvals(path_2L_33000)
[ts, te_2L_66000] = get_testing_error_and_eigvals(path_2L_66000)

# Times at which to consider NN dynamics
draw_times = np.arange(np.min(ts), np.max(ts), 5000)
res = np.intersect1d(ts, draw_times, return_indices=True)
draw_indeces = res[1]

# Plot training error
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(ts[draw_indeces], te_2L[draw_indeces], linewidth=2)
plt.plot(ts[draw_indeces], te_4L[draw_indeces], linewidth=2)
plt.plot(ts[draw_indeces], te_6L[draw_indeces], linewidth=2)
plt.plot(ts[draw_indeces], te_2L_33000[draw_indeces], linewidth=2)
plt.plot(ts[draw_indeces], te_2L_66000[draw_indeces], linewidth=2)
plt.legend(['2L, 256W, $|\\theta| = 1025$',
            '4L, 256W, $|\\theta| = 132609$',
            '6L, 256W, $|\\theta| = 264193$',
            '2L, 33000W, $|\\theta| = 132001$',
            '2L, 66000W, $|\\theta| = 264001$'], loc='upper right', prop={'size': 16})
plt.xlabel('$t$');
plt.ylabel('Test Loss');
plt.yscale('log')
filename = output_path + '/test_loss_dfrt_nns.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()

