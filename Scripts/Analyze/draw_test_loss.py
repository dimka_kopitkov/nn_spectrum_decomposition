import numpy as np
import sys
import os
os.chdir('../')
sys.path.insert(0, './Utils/')
from os import listdir
from os.path import isfile, join
from utils import *
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 30})


# Load configuration
config = read_config('./train.properties')
res_dir = config['res_dir']

output_path = res_dir + '/losses'
ensure_path_exists(output_path)

losses_file = np.load(res_dir + '/losses.npz')
train_losses = losses_file['train_losses']
test_losses = losses_file['test_losses']
ts = losses_file['ts']


# Plot training loss
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(ts, train_losses, linewidth=2)
plt.yscale('log')
plt.xlabel('$t$');
plt.ylabel('Train Loss');
filename = output_path + '/train_loss.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()


# Plot testing loss
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(ts, test_losses, linewidth=2)
plt.yscale('log')
plt.xlabel('$t$');
plt.ylabel('Test Loss');
filename = output_path + '/test_loss.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()
