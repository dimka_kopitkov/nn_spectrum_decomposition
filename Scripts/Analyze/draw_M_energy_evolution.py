import numpy as np
import sys
import os
os.chdir('../')
sys.path.insert(0, './Utils/')
from os import listdir
from os.path import isfile, join
from utils import *
import matplotlib.pyplot as plt
plt.ioff()
plt.rcParams.update({'font.size': 30})


# Load configuration
config = read_config('./train.properties')
res_dir = config['res_dir']
eig_path = res_dir + '/nn_eigs_dec/'

output_path = res_dir + '/figures'
ensure_path_exists(output_path)

# Output path
M_draw_path = res_dir + '/M_drawings'
ensure_path_exists(M_draw_path)

info_file = np.load(res_dir + '/nn_stats/nn_spectrum_info.npz')
points = info_file['points']
points_num = points.shape[0]
dim = points.shape[1]
summ_file = np.load(eig_path + '/summery.npz')
ts = summ_file['ts']
Ms = summ_file['Ms']
M_prjs = summ_file['M_prjs']
f_prjs = summ_file['f_prjs']
lrates = summ_file['lrates']
all_vals = summ_file['all_vals']


# Compute energy of M along time
Ms_l2 = np.sum(np.square(Ms), axis = 1)
M_projections_energy = np.square(M_prjs)
M_projections_rel_energy = M_projections_energy / Ms_l2[:,np.newaxis]
M_projections_acc_energy = np.cumsum(M_projections_energy, axis = 1)
M_projections_rel_acc_energy = M_projections_acc_energy / Ms_l2[:,np.newaxis]
# M_projections_energy_weighted = M_projections_energy*np.square(all_vals)
# M_projections_energy_weighted = M_projections_energy_weighted*np.square(lrates[:,np.newaxis]/float(points_num))
# f_prjs_squared = np.square(f_prjs)
# M_projections_energy = f_prjs_squared/M_projections_energy_weighted
# M_projections_energy = np.exp(np.log(np.abs(f_prjs)) - 0.5*np.log(np.abs(all_vals)))


# Times at which to consider NN dynamics
draw_times = np.arange(np.min(ts), np.max(ts), 5000)
res = np.intersect1d(ts, draw_times, return_indices=True)
draw_indeces = res[1]
draw_times = ts[draw_indeces]


# Draw M interpolation along time
if (dim == 2):
    x1 = np.linspace(0.0, 1.0, 1192)
    x2 = np.linspace(0.0, 1.0, 800)
    mesh_x1, mesh_x2 = np.meshgrid(x1, x2)
    temp_path = res_dir + '/temp/M_draws'
    filename = M_draw_path + '/M_draws.avi'
    draw_func_interpolation_along_time(Ms[draw_indeces], draw_times, filename, temp_path, points, mesh_x1, mesh_x2)


# Plot M projections evolution as video
temp_path = res_dir + '/temp/M_evolution'
ensure_path_exists(temp_path)

fns = []
n = 60
ymin = np.sort(M_projections_energy.ravel())[3]
#ymin = 1e-3
ymax = np.max(M_projections_energy)
for i in draw_indeces:
    t = ts[i]
    fig, axes = plt.subplots(nrows=1, ncols=2)
    fig.suptitle('Time: ' + str(t), fontsize = 10)

    ax = axes[0]
    x = M_projections_energy[i, 0:n]
    ax.plot(x, linewidth=2)
    x = running_mean(x, 5)
    ax.plot(x, linewidth=1)
    ax.set_ylim(np.min(M_projections_energy[:,0:n]), np.max(M_projections_energy[:,0:n]))
    ax.set_xlim(0, n)
    ax.set_yscale('log')
    ax.tick_params(labelsize=10)
    ax.grid(b=True)
    ax.set_title('First ' + str(n) + ' M projections', fontsize = 10)
    ax.set_xlabel('$i$', fontsize = 10);

    ax = axes[1]
    x = running_mean(M_projections_energy[i, :], 40)
    ax.plot(x, linewidth=2)
    x = running_mean(x, 100)
    ax.plot(x, linewidth=1)
    ax.set_ylim(ymin, ymax)
    ax.set_xlim(0, M_projections_energy.shape[1])
    ax.set_yscale('log')
    ax.tick_params(labelsize=10)
    ax.grid(b = True)
    ax.set_title('All M projections', fontsize = 10)
    ax.set_xlabel('$i$', fontsize = 10);
    #plt.ylabel('$\lambda_i$');

    manager = plt.get_current_fig_manager()
    manager.resize(*manager.window.maxsize())
    filename = temp_path + '/' + str(t).zfill(8) + '.png'
    plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
    plt.close()
    fns.append(filename)

# Create video with M energy evolution
filename = M_draw_path + '/M_energy_evolution.avi'
create_video_from_images(fns, filename, 5)
remove_if_path_exists(temp_path)


# Draw M L2 norm along time
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(ts[draw_indeces], Ms_l2[draw_indeces], linewidth=2)
ax.set_yscale('log')
plt.xlabel('$t$');
plt.ylabel('$|M_t|^{2}_{2}$');
filename = M_draw_path + '/M_norm_along_opt.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()


# Draw mean of M along time
Ms_mean = np.mean(Ms, axis = 1)
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(ts[draw_indeces], Ms_mean[draw_indeces], linewidth=2)
#ax.set_yscale('log')
plt.legend(range(1, 6), loc='upper right', prop={'size': 20})
plt.xlabel('$t$');
plt.ylabel('$mean(M_t)$');
filename = M_draw_path + '/M_mean_along_opt.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()


# Draw M projections at start and end of the optimization
t1 = 20000
#t2 = 595000
t2 = 590000
if (np.max(ts) < t2):
    t2 = np.max(ts)
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
index = np.where(ts == t1)[0][0]
x = M_projections_energy[index, :]
x = running_mean(x, 40)
plt.plot(x, linewidth=2, color = '#3026b5')
x = running_mean(x, 100)
plt.plot(x, linewidth=1, color = '#59b6f0', label='_nolegend_')
index = np.where(ts == t2)[0][0]
x = M_projections_energy[index, :]
x = running_mean(x, 40)
plt.plot(x, linewidth=2, color = '#21991f')
x = running_mean(x, 100)
plt.plot(x, linewidth=1, color = '#8ede3e', label='_nolegend_')
plt.xlabel('$i$');
plt.ylabel("$<\\upsilon_i^t, M_t>^2$");
ax.set_yscale('log')
plt.legend(['$t = ' + str(t1) + '$', '$t = 600000$'], loc='upper right', prop={'size': 20})
filename = M_draw_path + '/M_prjs_along_opt.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()


# Draw M projections along entire optimization
M_projections_energy_smoothed = np.zeros_like(M_projections_energy)
for i in range(M_projections_energy.shape[0]):
    M_projections_energy_smoothed[i] = running_mean(M_projections_energy[i], 100)

fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
sp_thrs = [10, 100, 1000, 2000, 4000, 6000, 8000]
sp_thrs = [t for t in sp_thrs if t < points_num]
start_index = np.where(ts == 5000)[0][0]
for sp_thr in sp_thrs:
    x = M_projections_energy_smoothed[draw_indeces, sp_thr]
    plt.plot(ts[draw_indeces], x, linewidth=2)
plt.xlabel('$t$');
plt.ylabel("$<\\upsilon_i^t, M_t>^2$");
ax.set_yscale('log')
sp_thrs_str = ['$i = ' + str(t) + '$' for t in sp_thrs]
plt.legend(sp_thrs_str, loc='center left', bbox_to_anchor=(1, 0.5))
filename = M_draw_path + '/M_prjs_along_opt2.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()

fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
sp_thrs = [10, 100, 1000, 2000, 4000, 6000, 8000]
sp_thrs = [t for t in sp_thrs if t < points_num]
start_index = np.where(ts == 5000)[0][0]
for sp_thr in sp_thrs:
    x = M_projections_energy_smoothed[draw_indeces, sp_thr] / M_projections_energy_smoothed[start_index, sp_thr]
    plt.plot(ts[draw_indeces], x, linewidth=2)
plt.xlabel('$t$');
plt.ylabel("$<\\upsilon_i^t, M_t>^2$");
ax.set_yscale('log')
sp_thrs_str = ['$i = ' + str(t) + '$' for t in sp_thrs]
plt.legend(sp_thrs_str, loc='center left', bbox_to_anchor=(1, 0.5))
filename = M_draw_path + '/M_prjs_along_opt2_normalized.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()


if (False):
    t1_index = np.where(ts == t1)[0][0]
    t2_index = np.where(ts == t2)[0][0]
    Ms_1 = Ms[t1_index,:]
    Ms_2 = Ms[t2_index,:]
    cov0_file = np.load(res_dir + '/nn_eigs_dec/nn_cov_00000000.npz')
    vecs0 = cov0_file['eigenvecs']
    vecs0 = vecs0.T
    Ms_1_prjs = np.matmul(vecs0.T, Ms_1)
    Ms_2_prjs = np.matmul(vecs0.T, Ms_2)
    Ms_1_prjs_energy = np.square(Ms_1_prjs)
    Ms_2_prjs_energy = np.square(Ms_2_prjs)

    fig = plt.figure(figsize=(8, 6))
    ax = fig.add_subplot(111)
    x = Ms_1_prjs_energy
    x = running_mean(x, 40)
    plt.plot(x, linewidth=2, color = '#3026b5')
    x = running_mean(x, 100)
    plt.plot(x, linewidth=1, color = '#59b6f0', label='_nolegend_')
    index = np.where(ts == t2)[0][0]
    x = Ms_2_prjs_energy
    x = running_mean(x, 40)
    plt.plot(x, linewidth=2, color = '#21991f')
    x = running_mean(x, 100)
    plt.plot(x, linewidth=1, color = '#8ede3e', label='_nolegend_')
    plt.xlabel('$i$');
    plt.ylabel("$<\\upsilon_i^0, M_t>^2$");
    ax.set_yscale('log')
    plt.legend(['$t = ' + str(t1) + '$', '$t = ' + str(t2)], loc='upper right', prop={'size': 20})
    filename = M_draw_path + '/M_prjs_on_G0.eps'
    plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
    plt.close()


if (False):
    t1 = 75000
    t2 = 75001
    t1_index = np.where(ts == t1)[0][0]
    Ms_1 = Ms[t1_index,:]
    cov1_file = np.load(res_dir + '/nn_eigs_dec/nn_cov_' + str(t1).zfill(8) + '.npz')
    vecs1 = cov1_file['eigenvecs']
    vecs1 = vecs1.T
    cov2_file = np.load(res_dir + '/nn_eigs_dec/nn_cov_' + str(t2).zfill(8) + '.npz')
    vecs2 = cov2_file['eigenvecs']
    vecs2 = vecs2.T
    Ms_1_prjs = np.matmul(vecs1.T, Ms_1)
    Ms_2_prjs = np.matmul(vecs2.T, Ms_1)
    Ms_1_prjs_energy = np.square(Ms_1_prjs)
    Ms_2_prjs_energy = np.square(Ms_2_prjs)
    np.sum(Ms_2_prjs_energy[0:100]) - np.sum(Ms_1_prjs_energy[0:100])
    np.mean(np.log(Ms_2_prjs_energy) - np.log(Ms_1_prjs_energy))

    fig = plt.figure(figsize=(8, 6))
    ax = fig.add_subplot(111)
    plt.plot(np.log(Ms_2_prjs_energy) - np.log(Ms_1_prjs_energy))
    plt.show()

    fig = plt.figure(figsize=(8, 6))
    ax = fig.add_subplot(111)
    x = Ms_1_prjs_energy
    x = running_mean(x, 40)
    plt.plot(x, linewidth=2, color = '#3026b5')
    x = running_mean(x, 100)
    plt.plot(x, linewidth=1, color = '#59b6f0', label='_nolegend_')
    index = np.where(ts == t2)[0][0]
    x = Ms_2_prjs_energy
    x = running_mean(x, 40)
    plt.plot(x, linewidth=2, color = '#21991f')
    x = running_mean(x, 100)
    plt.plot(x, linewidth=1, color = '#8ede3e', label='_nolegend_')
    plt.xlabel('$i$');
    #plt.ylabel("$<\\upsilon_i^0, M_t>^2$");
    ax.set_yscale('log')
    #plt.legend(['$t = ' + str(t1) + '$', '$t = 600000$'], loc='upper right', prop={'size': 20})
    filename = M_draw_path + '/M_prjs_between_itrtns.eps'
    plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
    plt.close()

    ratios = []
    for t in draw_times:
        if (t > 100000):
            break;
        t1 = t
        t2 = t+1
        t1_index = np.where(ts == t1)[0][0]
        Ms_1 = Ms[t1_index, :]
        cov1_file = np.load(res_dir + '/nn_eigs_dec/nn_cov_' + str(t1).zfill(8) + '.npz')
        vecs1 = cov1_file['eigenvecs']
        vecs1 = vecs1.T
        cov2_file = np.load(res_dir + '/nn_eigs_dec/nn_cov_' + str(t2).zfill(8) + '.npz')
        vecs2 = cov2_file['eigenvecs']
        vecs2 = vecs2.T
        Ms_1_prjs = np.matmul(vecs1.T, Ms_1)
        Ms_2_prjs = np.matmul(vecs2.T, Ms_1)
        Ms_1_prjs_energy = np.square(Ms_1_prjs)
        Ms_2_prjs_energy = np.square(Ms_2_prjs)
        ratio = np.sum(Ms_2_prjs_energy[0:2000]) - np.sum(Ms_1_prjs_energy[0:2000])
        ratios.append(ratio)
        print(t)


