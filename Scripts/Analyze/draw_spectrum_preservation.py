import numpy as np
import sys
import os
os.chdir('../')
sys.path.insert(0, './Utils/')
from os import listdir
from os.path import isfile, join
from utils import *
import matplotlib.pyplot as plt
plt.ioff()
plt.rcParams.update({'font.size': 30})


# Load configuration
config = read_config('./train.properties')
res_dir = config['res_dir']
eig_path = res_dir + '/nn_eigs_dec/'

output_path = res_dir + '/figures'
ensure_path_exists(output_path)

# Load eigenvectors of final Gramian
final_nn_cov_file = np.load(eig_path + '/nn_cov_00595000.npz')
final_eigenvecs = final_nn_cov_file['eigenvecs']
final_eigenvecs = final_eigenvecs.T

# Load eigenvectors of Gramian at t = 20000
begin_nn_cov_file = np.load(eig_path + '/nn_cov_00020000.npz')
begin_eigenvecs = begin_nn_cov_file['eigenvecs']
begin_eigenvecs = begin_eigenvecs.T

# Draw relative energy of final eigenvectors within begin Gramian
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
v_indeces = range(10)
for v_index in v_indeces:
    v = final_eigenvecs[:,v_index]
    v_projections = np.matmul(begin_eigenvecs.T, v)
    curr_rel_acc_energy = compute_rel_energy(v, v_projections)
    plt.plot(curr_rel_acc_energy, linewidth=2)
    #plt.scatter(v_index, curr_rel_acc_energy[v_index], s=200, label='_nolegend_')
plt.legend(np.array(v_indeces) + 1, loc='lower right', prop={'size': 18})
ax.grid(b=True)
plt.xscale('log')
plt.xlabel('$k$');
plt.ylabel('Energy');
filename = output_path + '/sp_stability1.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()


fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
v_indeces = [10, 30, 50, 100, 200, 400, 1000]
for v_index in v_indeces:
    v = final_eigenvecs[:,v_index]
    v_projections = np.matmul(begin_eigenvecs.T, v)
    curr_rel_acc_energy = compute_rel_energy(v, v_projections)
    plt.plot(curr_rel_acc_energy, linewidth=2)
    #plt.scatter(v_index, curr_rel_acc_energy[v_index], s=200, label='_nolegend_')
plt.legend(v_indeces, loc='upper left', prop={'size': 18})
ax.grid(b=True)
plt.xscale('log')
plt.xlabel('$k$');
plt.ylabel('Energy');
filename = output_path + '/sp_stability2.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()
