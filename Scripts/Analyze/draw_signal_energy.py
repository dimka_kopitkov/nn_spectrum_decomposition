import numpy as np
import sklearn
from scipy.linalg import eigh
from sklearn.preprocessing import normalize
import sys
import os
from os import path
os.chdir('../')
sys.path.insert(0, './Utils/')
from os import listdir
from os.path import isfile, join
from utils import *
import matplotlib.pyplot as plt
plt.ioff()
plt.rcParams.update({'font.size': 30})


# Load configuration
config = read_config('./train.properties')
res_dir = config['res_dir']
eig_path = res_dir + '/nn_eigs_dec/'

output_path = res_dir + '/signal_energy'
ensure_path_exists(output_path)

info_file = np.load(res_dir + '/nn_stats/nn_spectrum_info.npz')
labels = info_file['labels']
labels = labels.ravel()
points = info_file['points']
points_num = points.shape[0]
points_num_float = float(points_num)
summ_file = np.load(eig_path + '/summery.npz')
ts = summ_file['ts']
lrates = summ_file['lrates']
# Relative energy is square of cosine, where angle is between vector and its projection to k first eigenvectors
gt_rel_energies = summ_file['gt_rel_energies']
f_rel_energies = summ_file['f_rel_energies']
Ms = summ_file['Ms']
fs = summ_file['fs']
M_prjs = summ_file['M_prjs']
M0_prjs = summ_file['M0_prjs']
f_prjs = summ_file['f_prjs']
gt_prjs = summ_file['gt_prjs']
GMs = summ_file['GMs']
all_vals = summ_file['all_vals']

Ms_normalized = sklearn.preprocessing.normalize(Ms)
GMs_normalized = sklearn.preprocessing.normalize(GMs)
fs_normalized = sklearn.preprocessing.normalize(fs)
gt_normalized = sklearn.preprocessing.normalize(labels[np.newaxis,:])
fM_cos = np.sum(fs_normalized*Ms_normalized, axis = 1)
fGM_cos = np.sum(fs_normalized*GMs_normalized, axis = 1)
fy_cos = np.sum(fs_normalized*gt_normalized, axis = 1)
yM_cos = np.sum(Ms_normalized*gt_normalized, axis = 1)

# Bootstrap times at which to consider NN dynamics
draw_times = np.arange(0, 10001, 1000)
res = np.intersect1d(ts, draw_times, return_indices=True)
draw_indeces = res[1]
draw_times = ts[draw_indeces]

fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
sp_thrs = [5, 10, 20, 50, 100, 1000, 4000]
sp_thrs = [t for t in sp_thrs if t < points_num]
for sp_thr in sp_thrs:
    plt.plot(ts[draw_indeces], gt_rel_energies[draw_indeces,sp_thr], linewidth=2)
plt.legend(sp_thrs, loc='center left', bbox_to_anchor=(1, 0.5))
plt.xlabel('$t$');
plt.ylabel('Energy');
filename = output_path + '/gt_energy_along_opt1_bootstrap.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()

# Times at which to consider NN dynamics
draw_times = np.arange(np.min(ts), np.max(ts), 5000)
res = np.intersect1d(ts, draw_times, return_indices=True)
draw_indeces = res[1]
draw_times = ts[draw_indeces]

# Plot relative energy of target function in specific part of NN spectrum
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
sp_thrs = [5, 10, 20, 50, 100, 1000, 4000]
sp_thrs = [t for t in sp_thrs if t < points_num]
for sp_thr in sp_thrs:
    plt.plot(ts[draw_indeces], gt_rel_energies[draw_indeces,sp_thr], linewidth=2)
plt.legend(sp_thrs, loc='center left', bbox_to_anchor=(1, 0.5))
plt.xlabel('$t$');
plt.ylabel('Energy');
filename = output_path + '/gt_energy_along_opt1.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()

fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
sp_thrs = [60, 90, 120, 200, 400, 1000, 4000]
sp_thrs = [t for t in sp_thrs if t < points_num]
for sp_thr in sp_thrs:
    plt.plot(ts[draw_indeces], gt_rel_energies[draw_indeces,sp_thr], linewidth=2)
en1 = gt_rel_energies[draw_indeces,:]
en1 = en1[:,sp_thrs]
plt.ylim(np.min(en1), 1.0)
#plt.ylim(0.955, 1.0)
plt.legend(sp_thrs, loc='center left', bbox_to_anchor=(1, 0.5))
plt.xlabel('$t$');
plt.ylabel('Energy');
filename = output_path + '/gt_energy_along_opt2.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()


# prj_file = np.load(eig_path + '/more_projections.npz')
# all_Ms_prjs = prj_file['all_Ms_prjs']
# all_MM_norms = np.linalg.norm(Ms, axis = 1)
#
# s = 180000
# index = np.where(ts == s)[0][0]
# MM_prjs = all_Ms_prjs[:,:,index].astype(np.float64)
# MM_norm = all_MM_norms[index]
# MM_projections_energy = np.square(MM_prjs)
# MM_projections_acc_energy = np.cumsum(MM_projections_energy, axis = 1)
# MM_projections_acc_energy = MM_projections_acc_energy / (MM_norm**2)
#
#
# fig = plt.figure(figsize=(8, 6))
# ax = fig.add_subplot(111)
# for x in range(100000, 500001, 100000):
#     plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
# sp_thrs = [5, 10, 20, 50, 100, 1000, 4000]
# for sp_thr in sp_thrs:
#     plt.plot(ts[draw_indeces], MM_projections_acc_energy[draw_indeces,sp_thr], linewidth=2)
# plt.legend(sp_thrs, loc='center left', bbox_to_anchor=(1, 0.5))
# plt.xlabel('$t$');
# plt.ylabel('Energy');
# plt.show()




# Plot relative energy of M0 in specific part of NN spectrum
M0 = Ms[0,:]
M0_l2 = np.sum(np.square(M0))
M0_projections_energy = np.square(M0_prjs)
M0_projections_acc_energy = np.cumsum(M0_projections_energy, axis = 1)
M0_projections_rel_acc_energy = M0_projections_acc_energy / M0_l2

fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
sp_thrs = [5, 10, 20, 50, 100, 1000, 4000]
sp_thrs = [t for t in sp_thrs if t < points_num]
for sp_thr in sp_thrs:
    plt.plot(ts[draw_indeces], M0_projections_rel_acc_energy[draw_indeces,sp_thr], linewidth=2)
plt.legend(sp_thrs, loc='center left', bbox_to_anchor=(1, 0.5))
plt.xlabel('$t$');
plt.ylabel('Energy');
filename = output_path + '/M0_energy_along_opt1.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()

fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
sp_thrs = [60, 90, 120, 200, 400, 1000, 4000]
sp_thrs = [t for t in sp_thrs if t < points_num]
for sp_thr in sp_thrs:
    plt.plot(ts[draw_indeces], M0_projections_rel_acc_energy[draw_indeces,sp_thr], linewidth=2)
en1 = M0_projections_rel_acc_energy[draw_indeces,:]
en1 = en1[:,sp_thrs]
plt.ylim(np.min(en1), 1.0)
plt.legend(sp_thrs, loc='center left', bbox_to_anchor=(1, 0.5))
plt.xlabel('$t$');
plt.ylabel('Energy');
filename = output_path + '/M0_energy_along_opt2.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()


# Plot relative energy of NN output in specific part of NN spectrum
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
sp_thrs = [5, 10, 20, 50, 100, 1000, 4000]
sp_thrs = [t for t in sp_thrs if t < points_num]
for sp_thr in sp_thrs:
    plt.plot(ts[draw_indeces], f_rel_energies[draw_indeces,sp_thr], linewidth=2)
plt.legend(sp_thrs, loc='center left', bbox_to_anchor=(1, 0.5))
plt.xlabel('$t$');
plt.ylabel('Energy');
filename = output_path + '/f_energy_along_opt1.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()

fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
sp_thrs = [60, 90, 120, 200, 400, 1000, 4000]
sp_thrs = [t for t in sp_thrs if t < points_num]
for sp_thr in sp_thrs:
    plt.plot(ts[draw_indeces], f_rel_energies[draw_indeces,sp_thr], linewidth=2)
plt.ylim(0.955, 1.01)
plt.legend(sp_thrs, loc='center left', bbox_to_anchor=(1, 0.5))
plt.xlabel('$t$');
plt.ylabel('Energy');
filename = output_path + '/f_energy_along_opt2.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()


# Plot projections of NN output vector f, as video evolution
temp_path = res_dir + '/temp/f_proj_evolution'
ensure_path_exists(temp_path)
fns = []
f_prjs_square = np.square(f_prjs)
# These are eigenvalues of gradient similarity kernel,
# which define a density over feasible functions
kernel_eigvals = all_vals / points_num_float
max_y = np.max(kernel_eigvals)
n = 100
max_y2 = np.max(f_prjs_square[:, 0:n])
for i in draw_indeces:
    t = ts[i]
    fig, axes = plt.subplots(nrows=1, ncols=2)
    fig.suptitle('Time: ' + str(t), fontsize = 10)

    ax = axes[0]
    x = f_prjs_square[i, 0:n]
    ax.plot(x, linewidth=2, color = '#3026b5')
    x = running_mean(x, 10)
    ax.plot(x, linewidth=1, color = '#59b6f0')
    x = kernel_eigvals[i, 0:n]
    ax.plot(x, linewidth=2, color = 'green')
    ax.set_yscale('log')
    ax.tick_params(labelsize=10)
    ax.set_ylim(1e-6, max_y2)
    ax.set_xlim(0, n)
    ax.set_xlabel('$i$', fontsize = 10);
    ax.set_title('First ' + str(n) + ' f projections', fontsize = 10)

    ax = axes[1]
    x = f_prjs_square[i, :]
    ax.plot(x, linewidth=2, color = '#3026b5')
    x = running_mean(x, 20)
    ax.plot(x, linewidth=1, color = '#59b6f0')
    x = kernel_eigvals[i, :]
    ax.plot(x, linewidth=2, color = 'green')
    ax.set_yscale('log')
    ax.tick_params(labelsize=10)
    ax.set_ylim(1e-12, max_y)
    ax.set_xlim(0, points_num)
    ax.set_xlabel('$i$', fontsize = 10);
    ax.set_title('All f projections', fontsize = 10)

    fig.legend(['$<\\bar{f}_t, \\bar{v}_i^t>^2$',
                'smoothed',
                '$\\frac{\lambda_i^t}{N}$'],
               fontsize = 10,
               #bbox_to_anchor=(1.05, 0),
               loc='lower center', borderaxespad=0.)
    manager = plt.get_current_fig_manager()
    manager.resize(*manager.window.maxsize())
    filename = temp_path + '/' + str(i).zfill(8) + '.png'
    plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
    plt.close()
    fns.append(filename)

# Create video with f evolution
filename = output_path + '/f_proj_evolution.avi'
create_video_from_images(fns, filename, 5)
remove_if_path_exists(temp_path)


# Compare cumulative energy of f and of squared eigenvalues
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
x = f_rel_energies[draw_indeces[-1],:]
plt.plot(x, linewidth=2)
x = all_vals[draw_indeces[-1],:]
x = np.square(x)
x = np.cumsum(x)
x = x/np.nanmax(x)
plt.plot(x, linewidth=2)
ax.set_yscale('log')
plt.legend(['$E_{t} (\\bar{f}_t, k), t = ' + str(draw_times[-1]) + '$',
            '$\\frac{\\sum_{i = 1}^{k} (\\lambda_i^t)^2}{\\sum_{i = 1}^{N} (\\lambda_i^t)^2}$'],
           loc='lower right', prop={'size': 20})
plt.xlabel('$k$');
filename = output_path + '/f_vs_eigvals_energy.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()


# Plot relative energy of M in specific part of NN spectrum
Ms_l2 = np.sum(np.square(Ms), axis = 1)
M_projections_energy = np.square(M_prjs)
M_projections_acc_energy = np.cumsum(M_projections_energy, axis = 1)
M_projections_rel_acc_energy = M_projections_acc_energy / Ms_l2[:,np.newaxis]

fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
#sp_thrs = [5, 10, 20, 50, 100, 1000, 4000]
sp_thrs = [5, 1000, 4000]
sp_thrs = [t for t in sp_thrs if t < points_num]
for sp_thr in sp_thrs:
    plt.plot(ts[draw_indeces], M_projections_rel_acc_energy[draw_indeces,sp_thr], linewidth=2)
plt.legend(sp_thrs, loc='center left', bbox_to_anchor=(1, 0.5))
plt.xlabel('$t$');
plt.ylabel('Energy');
filename = output_path + '/M_energy_along_opt1.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()

fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
sp_thrs = [90, 120, 200, 400, 1000, 2000, 4000]
sp_thrs = [t for t in sp_thrs if t < points_num]
for sp_thr in sp_thrs:
    plt.plot(ts[draw_indeces], M_projections_rel_acc_energy[draw_indeces,sp_thr], linewidth=2)
plt.ylim(0.0, 1.0001)
plt.legend(sp_thrs, loc='center left', bbox_to_anchor=(1, 0.5))
plt.xlabel('$t$');
plt.ylabel('Energy');
filename = output_path + '/M_energy_along_opt2.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()


# Plot relative energy of GM in specific part of NN spectrum
GMs_l2 = np.sum(np.square(GMs), axis = 1)
M_projections_energy = np.square(M_prjs)
GM_projections_energy = np.square(all_vals)*M_projections_energy
GM_projections_acc_energy = np.cumsum(GM_projections_energy, axis = 1)
GM_projections_rel_acc_energy = GM_projections_acc_energy / GMs_l2[:,np.newaxis]

fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
sp_thrs = [5, 10, 20, 50, 100, 1000, 4000]
sp_thrs = [t for t in sp_thrs if t < points_num]
for sp_thr in sp_thrs:
    plt.plot(ts[draw_indeces], GM_projections_rel_acc_energy[draw_indeces,sp_thr], linewidth=2)
plt.legend(sp_thrs, loc='center left', bbox_to_anchor=(1, 0.5))
plt.xlabel('$t$');
plt.ylabel('Energy');
filename = output_path + '/GM_energy_along_opt1.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()


fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
sp_thrs = [60, 90, 120, 200, 400, 1000, 4000]
sp_thrs = [t for t in sp_thrs if t < points_num]
for sp_thr in sp_thrs:
    plt.plot(ts[draw_indeces], GM_projections_rel_acc_energy[draw_indeces,sp_thr], linewidth=2)
plt.ylim(0.99, 1.0001)
plt.legend(sp_thrs, loc='center left', bbox_to_anchor=(1, 0.5))
plt.xlabel('$t$');
plt.ylabel('Energy');
filename = output_path + '/GM_energy_along_opt2.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()


# Plot angles between f, m, y, Gm
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(ts[draw_indeces], fM_cos[draw_indeces], linewidth=2)
plt.plot(ts[draw_indeces], fy_cos[draw_indeces], linewidth=2)
plt.plot(ts[draw_indeces], yM_cos[draw_indeces], linewidth=2)
plt.plot(ts[draw_indeces], fGM_cos[draw_indeces], linewidth=2)
plt.legend(['$\\cos \\left( \\bar{f}_t, \\bar{m}_t \\right)$',
            '$\\cos \\left( \\bar{f}_t, \\bar{y} \\right)$',
            '$\\cos \\left( \\bar{y}, \\bar{m}_t \\right)$',
            '$\\cos \\left( \\bar{f}_t, G_t \\cdot \\bar{m}_t \\right)$'], loc='center left', bbox_to_anchor=(1, 0.5))
plt.xlabel('$t$');
filename = output_path + '/vectors_angle.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()

#
# fM_dot
# fM_dot = np.sum(fs*Ms, axis = 1)
# fGM_dot = np.sum(fs*GMs, axis = 1)
# fGM_dot_weighted = (lrates/points_num_float)*fGM_dot
# fM_weighted_dot = fM_dot - fGM_dot_weighted
#
# MGGM_dot = np.sum(GMs*GMs, axis = 1)
# MGM_dot = np.sum(GMs*Ms, axis = 1)
# MGM_weighted_dot = MGM_dot - (lrates/points_num_float)*MGGM_dot
#
# fig = plt.figure(figsize=(8, 6))
# ax = fig.add_subplot(111)
# for x in range(100000, 500001, 100000):
#     plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
# plt.axhline(y=0.0, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
# plt.plot(ts[draw_indeces], fM_dot[draw_indeces], linewidth=2)
# plt.plot(ts[draw_indeces], fM_weighted_dot[draw_indeces] - ((lrates/points_num_float)*MGM_weighted_dot)[draw_indeces], linewidth=2)
# #plt.plot(ts[draw_indeces], ((lrates/points_num_float)*MGM_weighted_dot)[draw_indeces], linewidth=2)
# #plt.plot(ts, fGM_dot, 'ro--', linewidth=2)
# # plt.plot(ts[draw_indeces], fGM_dot_weighted[draw_indeces], linewidth=2)
# # plt.plot(ts[draw_indeces], fM_dot[draw_indeces]/fGM_dot_weighted[draw_indeces], linewidth=2)
# plt.xlabel('$t$');
# #filename = output_path + '/vectors_angle.eps'
# #plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
# #plt.close()
# plt.show()

# Plot angles between m of different times
Ms_selected = Ms_normalized[draw_indeces,:]
Ms_angles = np.matmul(Ms_selected, Ms_selected.T)
filename = output_path + '/Ms_angles.eps'
contourf_with_colorbar_and_save(Ms_angles, ts[draw_indeces], ts[draw_indeces], filename, cm = 'jet', font_size = 15, dpi = 50)


# Plot alignment between ground truth and G's (and its inverse) diagonal
all_d_entries = []
all_inv_diag_entries = []
for i, t in enumerate(draw_times):
    step = str(t).zfill(8)
    curr_eig_file = np.load(eig_path + '/nn_cov_' + step + '.npz')
    curr_d_entries = curr_eig_file['d_entries'].flatten()
    curr_inv_diag_entries = curr_eig_file['inv_diag_entries'].flatten()
    all_d_entries.append(curr_d_entries)
    all_inv_diag_entries.append(curr_inv_diag_entries)
all_d_entries = np.stack(all_d_entries)
all_inv_diag_entries = np.stack(all_inv_diag_entries)

d_entries_prjs = np.matmul(all_d_entries, labels)
d_entries_norms = np.linalg.norm(all_d_entries, axis = 1)
d_entries_y_cos = d_entries_prjs/(d_entries_norms*np.linalg.norm(labels))
inv_diag_entries_prjs = np.matmul(all_inv_diag_entries, labels)
inv_diag_entries_norms = np.linalg.norm(all_inv_diag_entries, axis = 1)
inv_diag_entries_y_cos = inv_diag_entries_prjs/(inv_diag_entries_norms*np.linalg.norm(labels))
# x = Ms[draw_indeces]
# x_norm = np.linalg.norm(x, axis = 1)
# d_entries_prjs = np.sum(all_d_entries*x, axis = 1)
# d_entries_norms = np.linalg.norm(all_d_entries, axis = 1)
# d_entries_y_cos = d_entries_prjs/(d_entries_norms*x_norm)
# inv_diag_entries_prjs = np.sum(all_inv_diag_entries*x, axis = 1)
# inv_diag_entries_norms = np.linalg.norm(all_inv_diag_entries, axis = 1)
# inv_diag_entries_y_cos = inv_diag_entries_prjs/(inv_diag_entries_norms*x_norm)

fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(draw_times, d_entries_y_cos, linewidth=2)
plt.plot(draw_times, inv_diag_entries_y_cos, linewidth=2)
plt.xlabel('$t$');
plt.legend(['$\\cos \\left( \\bar{d}_t, \\bar{y} \\right)$',
            '$\\cos \\left( \\bar{invd}_t, \\bar{y} \\right)$'], loc='center left', bbox_to_anchor=(1, 0.5))
filename = output_path + '/diag_entries_angle.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()

fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(draw_times, d_entries_norms, linewidth=2)
plt.xlabel('$t$');
filename = output_path + '/diag_entries_norm.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()

fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(draw_times, inv_diag_entries_norms, linewidth=2)
plt.xlabel('$t$');
filename = output_path + '/inv_diag_entries_norm.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()



#---------------------------------------------------------
# Draw evolution of m and f, according to different basises


def read_and_draw_evolution_via_specific_basis(basis_time):
    time_str = str(basis_time).zfill(8)
    curr_output_path = output_path + '/basis_' + time_str

    cov_file_name = res_dir + '/nn_eigs_dec/nn_cov_' + time_str + '.npz'
    if (not path.exists(cov_file_name)):
        return

    cov_file = np.load(cov_file_name)
    vecs = cov_file['eigenvecs']
    vecs = vecs.astype(np.float64)
    vecs = vecs.T
    #vals = cov_file['eigenvals']

    draw_evolution_via_specific_basis(vecs, curr_output_path)


def draw_evolution_via_specific_basis(vecs, curr_output_path):
    ensure_path_exists(curr_output_path)

    Ms_0_prjs = np.matmul(vecs.T, Ms.astype(np.float64).T)
    Ms_0_prjs_squared = np.square(Ms_0_prjs)
    fs_0_prjs = np.matmul(vecs.T, fs.astype(np.float64).T)
    fs_0_prjs_squared = np.square(fs_0_prjs)


    # Draw f at different times
    draw_times = [0, 1, 10, 5000, 50000, 100000, 200000, 300000, 400000, 500000, 595000]
    draw_times = [t for t in draw_times if t in ts]
    fig = plt.figure(figsize=(8, 6))
    ax = fig.add_subplot(111)
    for s in draw_times:
        index = np.where(ts == s)[0][0]
        x = fs_0_prjs_squared[:, index]
        x = running_mean(x, 100)
        ax.plot(x, linewidth=0.5)
    lgnd_strs = ['$t = ' + str(t) + '$' for t in draw_times]
    plt.legend(lgnd_strs, loc='center left', bbox_to_anchor=(1, 0.5), prop={'size': 20})
    plt.yscale('log')
    plt.xlabel('$i$')
    plt.ylabel('$< \\bar{f}_t, \\bar{v}_i >^2$, smoothed');
    filename = curr_output_path + '/f_energy_for_dfrt_times.eps'
    plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
    plt.close()


    # Draw m at different times
    #draw_times = [0, 1, 10, 5000, 50000, 100000, 200000, 300000, 400000, 500000, 595000]
    #draw_times = [t for t in draw_times if t in ts]
    fig = plt.figure(figsize=(8, 6))
    ax = fig.add_subplot(111)
    for s in draw_times:
        index = np.where(ts == s)[0][0]
        x = Ms_0_prjs_squared[:, index]
        x = running_mean(x, 100)
        ax.plot(x, linewidth=0.5)
    lgnd_strs = ['$t = ' + str(t) + '$' for t in draw_times]
    plt.legend(lgnd_strs, loc='center left', bbox_to_anchor=(1, 0.5), prop={'size': 20})
    plt.yscale('log')
    plt.xlabel('$i$')
    plt.ylabel('$< \\bar{m}_t, \\bar{v}_i >^2$, smoothed');
    filename = curr_output_path + '/M_energy_for_dfrt_times.eps'
    plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
    plt.close()


    # Draw temporal spectral histogram of f
    imm = fs_0_prjs_squared[:,draw_indeces]
    for i in range(imm.shape[1]):
        imm[:,i] = running_mean(imm[:,i], 50)
    imm = np.log(imm)
    imm = np.flipud(imm)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    plt.set_cmap('jet')
    im = plt.imshow(imm, aspect = 'auto', extent = (np.min(draw_times), np.max(draw_times), 0, imm.shape[1]))
    plt.xlabel('$t$')
    plt.ylabel('$i$')
    for x in range(100000, 500001, 100000):
        plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
    #[i.set_linewidth(0.0) for i in ax.spines.itervalues()]
    [ax.spines[i].set_linewidth(0.0) for i in ax.spines]
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(im, cax=cax)
    cbar.set_label('$\\log \\left[ < \\bar{f}_t, \\bar{v}_i >^2 \\right]$, smoothed', size = 15)
    filename = curr_output_path + '/f_freq_ev.eps'
    plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
    plt.close()

    fig = plt.figure()
    ax = fig.add_subplot(111)
    plt.set_cmap('jet')
    first_n = 200
    im = plt.imshow(imm[-first_n:,:], aspect = 'auto', extent = (np.min(draw_times), np.max(draw_times), 0, first_n))
    plt.xlabel('$t$')
    plt.ylabel('$i$')
    for x in range(100000, 500001, 100000):
        plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
    #[i.set_linewidth(0.0) for i in ax.spines.itervalues()
    [ax.spines[i].set_linewidth(0.0) for i in ax.spines]
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(im, cax=cax)
    cbar.set_label('$\\log \\left[ < \\bar{f}_t, \\bar{v}_i >^2 \\right]$, smoothed', size = 15)
    filename = curr_output_path + '/f_freq_ev_zoom.eps'
    plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
    plt.close()


    # Draw temporal spectral histogram of m
    imm = Ms_0_prjs_squared[:,draw_indeces]
    for i in range(imm.shape[1]):
        imm[:,i] = running_mean(imm[:,i], 50)
    imm = np.log(imm)
    imm = np.flipud(imm)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    plt.set_cmap('jet')
    im = plt.imshow(imm, aspect = 'auto', extent = (np.min(draw_times), np.max(draw_times), 0, imm.shape[1]))
    plt.xlabel('$t$')
    plt.ylabel('$i$')
    for x in range(100000, 500001, 100000):
        plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
    #[i.set_linewidth(0.0) for i in ax.spines.itervalues()]
    [ax.spines[i].set_linewidth(0.0) for i in ax.spines]
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(im, cax=cax)
    cbar.set_label('$\\log \\left[ < \\bar{m}_t, \\bar{v}_i >^2 \\right]$, smoothed', size = 15)
    filename = curr_output_path + '/M_freq_ev.eps'
    plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
    plt.close()

    fig = plt.figure()
    ax = fig.add_subplot(111)
    plt.set_cmap('jet')
    first_n = 200
    im = plt.imshow(imm[-first_n:,:], aspect = 'auto', extent = (np.min(draw_times), np.max(draw_times), 0, first_n))
    plt.xlabel('$t$')
    plt.ylabel('$i$')
    for x in range(100000, 500001, 100000):
        plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
    #[i.set_linewidth(0.0) for i in ax.spines.itervalues()
    [ax.spines[i].set_linewidth(0.0) for i in ax.spines]
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(im, cax=cax)
    cbar.set_label('$\\log \\left[ < \\bar{m}_t, \\bar{v}_i >^2 \\right]$, smoothed', size = 15)
    filename = curr_output_path + '/M_freq_ev_zoom.eps'
    plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
    plt.close()



#---------------------------------------------------------
# Basis of eigenvectors at various times:
read_and_draw_evolution_via_specific_basis(0);
read_and_draw_evolution_via_specific_basis(100000);
read_and_draw_evolution_via_specific_basis(200000);
read_and_draw_evolution_via_specific_basis(300000);
read_and_draw_evolution_via_specific_basis(400000);
read_and_draw_evolution_via_specific_basis(500000);
read_and_draw_evolution_via_specific_basis(595000);



#---------------------------------------------------------
# Basis of eigenvectors of Relu NTK:
curr_output_path = output_path + '/relu_ntk_basis'

points_normalized = normalize(points.astype(np.float64), axis = 1)
points_normalized_cov = np.matmul(points_normalized, points_normalized.T)
points_normalized_cov = np.clip(points_normalized_cov, -1.0, 1.0 - 1e-16)
points_normalized_angles = np.arccos(points_normalized_cov)
relu_gramian = (points_normalized_cov*(np.pi - points_normalized_angles))/(2.0*np.pi)
[eigenvals, eigenvecs] = eigh(relu_gramian)
indeces = np.argsort(eigenvals)
indeces = np.flip(indeces)
eigenvals = eigenvals[indeces]
eigenvecs = (eigenvecs.T)[indeces, :]

draw_evolution_via_specific_basis(eigenvecs, curr_output_path)
