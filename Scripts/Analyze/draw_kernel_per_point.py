import sys
import os
os.chdir('../')
import numpy as np
import sklearn
sys.path.insert(0, './Utils/')
from utils import *
from image_utils import *
import matplotlib.pyplot as plt
plt.ioff()
plt.rcParams.update({'font.size': 30})


config = read_config('./train.properties')
output_path = config['res_dir']
eig_path = output_path + '/nn_eigs_dec'

summ_file = np.load(eig_path + '/summery.npz')
ts = summ_file['ts']

info_file = np.load(output_path + '/nn_stats/nn_spectrum_info.npz')
points = info_file['points']
points_num = points.shape[0]

# This script handles only 2D input space
if (points.shape[1] > 2):
    exit();

# Output path
kernel_draw_path = output_path + '/kernel_per_point_drawings'
ensure_path_exists(kernel_draw_path)

# Space grid for interpolation
x1 = np.linspace(0.0, 1.0, 1192)
x2 = np.linspace(0.0, 1.0, 800)
mesh_x1, mesh_x2 = np.meshgrid(x1, x2)

# For each evaluated point find its index within the training points
eval_points = np.array([[0.0, 0.0], [1.0, 0.0], [0.0, 1.0], [1.0, 1.0], [0.5, 0.5], [0.25, 0.5], [0.5, 0.25], [0.75, 0.5], [0.5, 0.75]])
eval_point_indeces = []
for i in range(eval_points.shape[0]):
    point = eval_points[i, :]
    point = point[np.newaxis, :]
    dists = sklearn.metrics.pairwise.euclidean_distances(points, point)
    point_index = np.argmin(dists)
    eval_point_indeces.append(point_index)

# Steps at which to draw point kernels
steps = [0, 1, 10, 100, 5000, 10000, 20000, 50000, 100000, 105000, 120000, 200000, 300000, 400000, 500000, 595000]
steps = [t for t in steps if t in ts]
for s in steps:
    step = str(s).zfill(8)
    curr_eig_file = np.load(eig_path + '/nn_cov_' + step + '.npz')
    curr_vecs = curr_eig_file['eigenvecs']
    curr_vecs = curr_vecs.T
    curr_vals = curr_eig_file['eigenvals']

    curr_draw_path = kernel_draw_path + '/time_' + step
    ensure_path_exists(curr_draw_path)

    # Draw kernels for chosen points in [0, 1]^2
    for i, point_index in enumerate(eval_point_indeces):
        # Compute gradiet dot-product between current point and rest of the points
        point_coefs = curr_vecs[point_index, :]
        point_kernel = (curr_vals*point_coefs)[np.newaxis,:]*curr_vecs
        point_kernel = np.sum(point_kernel, axis = 1)

        v = interpolate_surf_image(point_kernel, points, mesh_x1, mesh_x2)
        filename = curr_draw_path + '/point_' + str(i).zfill(8) + '.eps'
        draw_image_and_save(v, filename, draw_point = points[point_index], font_size=15, dpi=50)

