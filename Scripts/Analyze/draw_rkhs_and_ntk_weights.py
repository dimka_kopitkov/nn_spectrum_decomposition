import numpy as np
import sys
import os
os.chdir('../')
sys.path.insert(0, './Utils/')
from os import listdir
from os.path import isfile, join
from utils import *
import matplotlib.pyplot as plt
from matplotlib import rc
plt.rcParams.update({'font.size': 30})

max_power = 8
min_power = -8
eigmax = np.power(10.0, max_power, dtype = np.float128)
eigmin = np.power(10.0, min_power, dtype = np.float128)
eigvals = np.logspace(min_power, max_power, 1000, dtype = np.float128)

rkhs_norm_weights = 1.0/eigvals

coef = 1.0/eigmax
# What happens for twice bound?
#coef = coef*2.0

def compute_ntk_norm_weights(t):
    # ntk_norm_weights = 1.0/(np.power(1.0 - coef*eigvals, -t) - 1.0)
    p1 = t * np.log(1.0 - coef * eigvals)
    p2 = np.log1p(-np.power(1.0 - coef * eigvals, t))
    ntk_norm_weights = p1 - p2
    ntk_norm_weights = np.exp(ntk_norm_weights)
    # ntk_norm_weights = ntk_norm_weights*np.square(eigvals)
    return ntk_norm_weights


times = [100, 1000, 10000, 100000, 1000000, 10000000]
ntk_norm_weights_list = []
for t in times:
    ntk_norm_weights = compute_ntk_norm_weights(t)
    ntk_norm_weights_list.append(ntk_norm_weights)

# Plot regularization weights
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for i, t in enumerate(times):
    plt.plot(eigvals, ntk_norm_weights_list[i], linewidth=2)
plt.plot(eigvals, rkhs_norm_weights, linewidth=2)
plt.yscale('log')
plt.xscale('log')
plt.xlabel('$\lambda$');
lgnd_strs = [str(t) for t in times]
lgnd_strs.append('rkhs weights')
plt.legend(lgnd_strs, loc='center left', bbox_to_anchor=(1, 0.5))
plt.show()


# Compute coefficients of RKHS solution
rkhs_coefs = [0.001, 0.01, 0.1, 1.0, 10.0, 100.0]
rkhs_sol_coefs_list = []
for r in rkhs_coefs:
    sol_coefs = eigvals/(eigvals + r)
    rkhs_sol_coefs_list.append(sol_coefs)


# Plot RKHS solution coefficients
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for i, r in enumerate(rkhs_coefs):
    plt.plot(eigvals, rkhs_sol_coefs_list[i], linewidth=2)
plt.yscale('log')
plt.xscale('log')
plt.xlabel('$\lambda$');
lgnd_strs = [str(r) for r in rkhs_coefs]
plt.legend(lgnd_strs, loc='center left', bbox_to_anchor=(1, 0.5))
plt.show()

# Compute coefficients of NTK solution
def compute_ntk_solution_coefs(t):
    sol_coefs = 1.0 - np.power(1.0 - coef * eigvals, t)
    return sol_coefs

ntk_sol_coefs_list = []
for t in times:
    sol_coefs = compute_ntk_solution_coefs(t)
    ntk_sol_coefs_list.append(sol_coefs)

# Plot NTK solution coefficients
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for i, t in enumerate(times):
    plt.plot(eigvals, ntk_sol_coefs_list[i], linewidth=2)
plt.yscale('log')
plt.xscale('log')
plt.xlabel('$\lambda$');
lgnd_strs = [str(t) for t in times]
plt.legend(lgnd_strs, loc='center left', bbox_to_anchor=(1, 0.5))
plt.show()



# Plot solution coefficients together
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for i, r in enumerate(rkhs_coefs):
    plt.plot(eigvals, rkhs_sol_coefs_list[i], linewidth=2)
for i, t in enumerate(times):
    plt.plot(eigvals, ntk_sol_coefs_list[i], linewidth=2)
plt.yscale('log')
plt.xscale('log')
plt.xlabel('$\lambda$');
plt.show()


# NTK solution at time t is very close to to RKHS solution with
# regularization coefficient mu = 10^8/t
