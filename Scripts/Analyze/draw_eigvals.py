import numpy as np
import sys
import os
os.chdir('../')
sys.path.insert(0, './Utils/')
from os import listdir
from os.path import isfile, join
from utils import *
import matplotlib.pyplot as plt
plt.ioff()
plt.rcParams.update({'font.size': 30})
import cv2


# Load configuration
config = read_config('./train.properties')
res_dir = config['res_dir']
eig_path = res_dir + '/nn_eigs_dec/'

output_path = res_dir + '/eigvals_drawings'
ensure_path_exists(output_path)
temp_path = res_dir + '/temp/eigvals'
ensure_path_exists(temp_path)


summ_file = np.load(eig_path + '/summery.npz')
ts = summ_file['ts']
lrates = summ_file['lrates']
all_vals = summ_file['all_vals']
max_vals = all_vals[:,0]
points_num = all_vals.shape[1]


# Plot all eigenvalues for various times
draw_times = np.array([0, 100, 1000, 10000, 100000, 595000])
res = np.intersect1d(ts, draw_times, return_indices=True)
draw_indeces = res[1]
draw_vals = all_vals[draw_indeces]
draw_times = ts[draw_indeces]
fig = plt.figure(figsize=(8, 8))
ax = fig.add_subplot(111)
for i in range(draw_vals.shape[0]):
    plt.plot(draw_vals[i], linewidth=2)
plt.ylim(1e-8, np.max(draw_vals))
plt.xlim(-10, points_num)
plt.yscale('log')
ax.grid(b=True)
plt.xlabel('$i$');
plt.ylabel('$\lambda_i$');
plt.legend(['$t = 0$',
            '$t = 100$',
            '$t = 1000$',
            '$t = 10000$',
            '$t = 100000$',
            '$t = 600000$'], loc='upper right', prop={'size': 20})
filename = output_path + '/eigvals_evolution.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()


# Plot specific eigenvalues for various times
draw_times = np.arange(np.min(ts), np.max(ts), 5000)
res = np.intersect1d(ts, draw_times, return_indices=True)
draw_indeces = res[1]
draw_times = ts[draw_indeces]
fig = plt.figure(figsize=(8, 8))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
eig_indeces = [0, 9, 19, 49, 99, 299, 499, 999, 2999]
eig_indeces = [i for i in eig_indeces if i < points_num]
for i in eig_indeces:
    plt.plot(ts[draw_indeces], all_vals[draw_indeces,i], linewidth=2)
plt.yscale('log')
plt.xlabel('$t$');
plt.ylabel('$\lambda_i^t$');
lgnd_strs = np.array(eig_indeces) + 1
lgnd_strs = ['$i = ' + str(s) + '$' for s in lgnd_strs]
plt.legend(lgnd_strs, loc='lower right', prop={'size': 20})
filename = output_path + '/eigvals_evolution_spcfc.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()


# Plot speed of specific eigenvalues for various times
spectral_speeds = 1.0 - np.abs(1.0 - all_vals[draw_indeces,:]*lrates[draw_indeces,np.newaxis]/float(points_num))
fig = plt.figure(figsize=(8, 8))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
eig_indeces = [0, 3, 5, 8, 9, 99, 499, 999]
eig_indeces = [i for i in eig_indeces if i < points_num]
for i in eig_indeces:
    plt.plot(ts[draw_indeces], spectral_speeds[:,i], linewidth=2)
plt.yscale('log')
plt.xlabel('$t$');
plt.ylabel('$s_i^t$');
lgnd_strs = np.array(eig_indeces) + 1
lgnd_strs = ['$i = ' + str(s) + '$' for s in lgnd_strs]
plt.legend(lgnd_strs, loc='lower right', prop={'size': 20})
filename = output_path + '/eigvals_speed_evolution.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()

theta_speeds = all_vals[draw_indeces,:]*lrates[draw_indeces,np.newaxis]/float(points_num)
fig = plt.figure(figsize=(8, 8))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
eig_indeces = [0, 3, 5, 8, 9, 99, 499, 999]
eig_indeces = [i for i in eig_indeces if i < points_num]
for i in eig_indeces:
    plt.plot(ts[draw_indeces], theta_speeds[:,i], linewidth=2)
plt.yscale('log')
plt.xlabel('$t$');
plt.ylabel('$\\frac{\delta_t}{N}\lambda_i^t$');
lgnd_strs = np.array(eig_indeces) + 1
lgnd_strs = ['$i = ' + str(s) + '$' for s in lgnd_strs]
plt.legend(lgnd_strs, loc='lower right', prop={'size': 20})
filename = output_path + '/eigvals_speed_theta_evolution1.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()

fig = plt.figure(figsize=(8, 8))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
eig_indeces = [0, 1, 2, 3, 4, 5]
eig_indeces = [i for i in eig_indeces if i < points_num]
for i in eig_indeces:
    plt.plot(ts[draw_indeces], theta_speeds[:,i], linewidth=2)
plt.xlabel('$t$');
plt.ylabel('$\\frac{\delta_t}{N}\lambda_i^t$');
lgnd_strs = np.array(eig_indeces) + 1
lgnd_strs = ['$i = ' + str(s) + '$' for s in lgnd_strs]
plt.legend(lgnd_strs, loc='upper right', prop={'size': 20})
filename = output_path + '/eigvals_speed_theta_evolution2.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()

fig = plt.figure(figsize=(8, 8))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
for y in [0.0, 1.0, 2.0]:
    plt.axhline(y=y, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(ts[draw_indeces], theta_speeds[:,0], linewidth=2)
plt.xlabel('$t$');
plt.ylabel('$\\frac{\delta_t}{N}\lambda_{max}}^t$');
#plt.yscale('log')
filename = output_path + '/eigval_max_speed_theta_evolution.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()

# Times at which to consider NN dynamics
draw_times = np.arange(np.min(ts), np.max(ts), 5000)
res = np.intersect1d(ts, draw_times, return_indices=True)
draw_indeces = res[1]
draw_vals = all_vals[draw_indeces]
draw_times = ts[draw_indeces]

# Plot eigenvalues evolution as video
fns = []
for i, t in enumerate(draw_times):
    fig = plt.figure(figsize=(8, 8))
    ax = fig.add_subplot(111)
    plt.plot(draw_vals[i], linewidth=2)
    plt.ylim(1e-8, np.max(draw_vals))
    plt.xlim(-10, points_num)
    plt.yscale('log')
    plt.xlabel('$i$');
    plt.ylabel('$\lambda_i$');
    ax.grid(b=True)
    plt.title('Time: ' + str(t))
    filename = temp_path + '/' + str(t).zfill(8) + '.png'
    plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
    plt.close()
    fns.append(filename)

# Create video with eigenvalues evolution
filename = output_path + '/eigvals_evolution.avi'
create_video_from_images(fns, filename, 5)
remove_if_path_exists(temp_path)