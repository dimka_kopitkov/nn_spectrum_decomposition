import numpy as np
import sys
import os
os.chdir('../')
sys.path.insert(0, './Utils/')
from os import listdir
from os.path import isfile, join
from utils import *
import matplotlib.pyplot as plt
plt.ioff()
plt.rcParams.update({'font.size': 30})


# Load configuration
config = read_config('./train.properties')
res_dir = config['res_dir']
eig_path = res_dir + '/nn_eigs_dec/'

output_path = res_dir + '/figures'
ensure_path_exists(output_path)

# Output path
M_draw_path = res_dir + '/M_drawings'
ensure_path_exists(M_draw_path)

info_file = np.load(res_dir + '/nn_stats/nn_spectrum_info.npz')
points = info_file['points']
points_num = points.shape[0]
labels = info_file['labels']
labels = labels.ravel()
summ_file = np.load(eig_path + '/summery.npz')
ts = summ_file['ts']
Ms = summ_file['Ms']
fs = summ_file['fs']
M_prjs = summ_file['M_prjs']

# Compute energy of M along time
Ms_l2 = np.sum(np.square(Ms), axis = 1)
M_projections_energy = np.square(M_prjs)
M_projections_rel_energy = M_projections_energy / Ms_l2[:,np.newaxis]
M_projections_acc_energy = np.cumsum(M_projections_energy, axis = 1)
M_projections_rel_acc_energy = M_projections_acc_energy / Ms_l2[:,np.newaxis]

# Space grid for interpolation
x1 = np.linspace(0.0, 1.0, 1192)
x2 = np.linspace(0.0, 1.0, 800)
mesh_x1, mesh_x2 = np.meshgrid(x1, x2)

# Frequencies to compute
lim = 40.0
f1 = np.linspace(-lim, lim, 100)
f2 = np.linspace(-lim, lim, 100)
mesh_f1, mesh_f2 = np.meshgrid(f1, f2)
frqs = np.array([np.reshape(mesh_f1, [-1]), np.reshape(mesh_f2, [-1])]).T
space_fr_dot = np.matmul(points, frqs.T)
fourier_part = - 2.0 * np.pi * 1j * space_fr_dot
fourier_part = np.exp(fourier_part)

# Compute Fourier Transform of target function
frq_func = np.matmul(labels, fourier_part)
frq_func_magn = np.abs(frq_func)
frq_func_magn = frq_func_magn / float(points_num)

f = np.reshape(frq_func_magn, mesh_f1.shape)
filename = M_draw_path + '/gt_freq.eps'
contourf_with_colorbar_and_save(f, f1, f2, filename, cm='jet', font_size=15, dpi=50)
filename = M_draw_path + '/gt_freq.png'
contourf_with_colorbar_and_save(f, f1, f2, filename, cm='jet', dpi=50)


# Compute Fourier Transform of f_0
frq_func = np.matmul(fs[0,:], fourier_part)
frq_func_magn = np.abs(frq_func)
frq_func_magn = frq_func_magn / float(points_num)

f = np.reshape(frq_func_magn, mesh_f1.shape)
filename = M_draw_path + '/f0_freq.eps'
contourf_with_colorbar_and_save(f, f1, f2, filename, cm='jet', font_size=15, dpi=50)
filename = M_draw_path + '/f0_freq.png'
contourf_with_colorbar_and_save(f, f1, f2, filename, cm='jet', dpi=50)


# Steps at which to draw M
steps = [0, 1, 10, 100, 5000, 10000, 20000, 50000, 95000, 100000, 105000, 120000, 195000, 200000, 205000, 295000, 300000, 305000, 400000, 500000, 595000]
steps = [t for t in steps if t in ts]
for s in steps:
    step = str(s).zfill(8)
    index = np.where(ts == s)[0][0]
    curr_M = Ms[index,:]

    v = interpolate_surf_image(curr_M, points, mesh_x1, mesh_x2)
    filename = M_draw_path + '/M_' + step + '.eps'
    draw_image_and_save(v, filename, font_size=15, dpi=50)

    # Compute Fourier Transform
    frq_func = np.matmul(curr_M, fourier_part)
    frq_func_magn = np.abs(frq_func)
    frq_func_magn = frq_func_magn / float(points_num)

    f = np.reshape(frq_func_magn, mesh_f1.shape)
    filename = M_draw_path + '/frq_' + step + '.eps'
    contourf_with_colorbar_and_save(f, f1, f2, filename, cm='jet', font_size = 15, dpi=50)
    filename = M_draw_path + '/frq_' + step + '.png'
    contourf_with_colorbar_and_save(f, f1, f2, filename, cm='jet', dpi=50)


# Times at which to consider NN dynamics
draw_times = np.arange(np.min(ts), np.max(ts), 5000)
res = np.intersect1d(ts, draw_times, return_indices=True)
draw_indeces = res[1]

# Plot M frequency evolution
temp_path = res_dir + '/temp/M_evolution'
ensure_path_exists(temp_path)

fns = []
for i in draw_indeces:
    t = ts[i]
    curr_M = Ms[i,:]

    # Compute Fourier Transform
    frq_func = np.matmul(curr_M, fourier_part)
    frq_func_magn = np.abs(frq_func)
    frq_func_magn = frq_func_magn / float(points_num)

    f = np.reshape(frq_func_magn, mesh_f1.shape)
    filename = temp_path + '/' + str(t).zfill(8) + '.png'

    fig = plt.figure()
    ax = fig.add_subplot(111)
    set_ax_font_size(ax, 10)
    plt.set_cmap('viridis')
    im = ax.contourf(f1, f2, f, 100)
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(im, cax=cax)
    cbar.ax.tick_params(labelsize=10)
    ax.set_title('Time: ' + str(t), fontsize = 15)
    plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
    plt.close()
    fns.append(filename)

# Create video with eigenvalues evolution
filename = M_draw_path + '/M_frq_evolution.avi'
create_video_from_images(fns, filename, 5)
remove_if_path_exists(temp_path)


# Compute evolution of m along various frequencies
angles_num = 100
r_num = 50
lim = 40.0
rs = np.linspace(0.0, lim, r_num);
angles = np.linspace(0, 2.0*np.pi, angles_num, endpoint=False);
angles_cos = np.cos(angles)
angles_sin = np.sin(angles)
circle_xy = np.stack([angles_cos, angles_sin])
circle_xy = circle_xy.T

frqsss = [r*circle_xy for r in rs]
frqsss = np.concatenate(frqsss)
space_fr_dot = np.matmul(points, frqsss.T)
fourier_part = - 2.0 * np.pi * 1j * space_fr_dot
fourier_part = np.exp(fourier_part)

frq_func = np.matmul(Ms, fourier_part)
frq_func_magn = np.abs(frq_func)
frq_func_magn = frq_func_magn / float(points_num)
frq_func_magn = np.reshape(frq_func_magn, [Ms.shape[0], r_num, angles_num])
M_spectums_mat = np.mean(frq_func_magn, axis=2)


def draw_frequency_at_times(draw_times, f_name):
    draw_times = [t for t in draw_times if t in ts]
    fig = plt.figure(figsize=(8, 6))
    ax = fig.add_subplot(111)
    for s in draw_times:
        index = np.where(ts == s)[0][0]
        ax.plot(rs, M_spectums_mat[index, :])
    lgnd_strs = ['$t = $' + str(t) for t in draw_times]
    plt.legend(lgnd_strs, loc='center left', bbox_to_anchor=(1, 0.5))
    plt.yscale('log')
    plt.xlabel('$f$')
    filename = M_draw_path + '/' + f_name
    plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
    plt.close()


# Draw frequency magnitudes at different times
draw_times = [0, 1, 10, 100000, 200000, 300000, 400000, 500000, 595000, 600000, 700000, 800000, 900000, 1000000, 1100000, 1200000, 1300000, 1400000, 1500000]
draw_frequency_at_times(draw_times, 'M_freq_for_dfrnt_times1.eps')
draw_times = [90000, 100000, 110000, 120000, 130000, 140000, 150000, 160000, 170000, 180000, 190000, 200000, 210000]
draw_frequency_at_times(draw_times, 'M_freq_for_dfrnt_times2.eps')
draw_times = [90000, 100000, 110000, 120000]
draw_frequency_at_times(draw_times, 'M_freq_for_dfrnt_times3.eps')
draw_times = [120000, 130000, 140000, 150000, 160000, 170000]
draw_frequency_at_times(draw_times, 'M_freq_for_dfrnt_times4.eps')
draw_times = [1100000, 1200000, 1300000, 1400000, 1490000]
draw_frequency_at_times(draw_times, 'M_freq_for_dfrnt_times5.eps')
draw_times = [600000, 700000, 800000, 900000, 1000000]
draw_frequency_at_times(draw_times, 'M_freq_for_dfrnt_times6.eps')
draw_times = [500000, 600000, 700000, 800000, 900000]
draw_frequency_at_times(draw_times, 'M_freq_for_dfrnt_times7.eps')


# Draw frequency magnitudes along time as image
draw_times = np.arange(np.min(ts), np.max(ts), 5000)
res = np.intersect1d(ts, draw_times, return_indices=True)
draw_indeces = res[1]

imm = np.log(M_spectums_mat[draw_indeces,:]).T
imm = np.flipud(imm)
fig = plt.figure()
ax = fig.add_subplot(111)
plt.set_cmap('jet')
im = plt.imshow(imm, aspect = 'auto', extent = (np.min(draw_times), np.max(draw_times), 0, imm.shape[1]))
plt.xlabel('$t$')
plt.ylabel('$f$')
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
cbar = plt.colorbar(im, cax=cax)
cbar.set_label('$\\log \\left[ F \\right]$')
filename = M_draw_path + '/freq_ev.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()
