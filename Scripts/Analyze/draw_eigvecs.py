import sys
import os
os.chdir('../')
import numpy as np
sys.path.insert(0, './Utils/')
from utils import *
from image_utils import *
import matplotlib.pyplot as plt
plt.ioff()
plt.rcParams.update({'font.size': 30})


config = read_config('./train.properties')
output_path = config['res_dir']
eig_path = output_path + '/nn_eigs_dec'

info_file = np.load(output_path + '/nn_stats/nn_spectrum_info.npz')
labels = info_file['labels']
labels = labels.ravel()
points = info_file['points']
points_num = points.shape[0]
dim = points.shape[1]
summ_file = np.load(eig_path + '/summery.npz')
fs = summ_file['fs']
f_prjs = summ_file['f_prjs']
ts = summ_file['ts']

# Output path
eigvec_draw_path = output_path + '/eigvecs_drawings'
ensure_path_exists(eigvec_draw_path)

# Frequencies to compute
lim = 40.0
f1 = np.linspace(-lim, lim, 100)
f2 = np.linspace(-lim, lim, 100)
mesh_f1, mesh_f2 = np.meshgrid(f1, f2)
frqs = np.array([np.reshape(mesh_f1, [-1]), np.reshape(mesh_f2, [-1])]).T
space_fr_dot = np.matmul(points, frqs.T)
fourier_part = - 2.0 * np.pi * 1j * space_fr_dot
fourier_part = np.exp(fourier_part)

# Space grid for interpolation
x1 = np.linspace(0.0, 1.0, 1192)
x2 = np.linspace(0.0, 1.0, 800)
mesh_x1, mesh_x2 = np.meshgrid(x1, x2)
v = interpolate_surf_image(labels, points, mesh_x1, mesh_x2)
filename = eigvec_draw_path + '/ground_truth.png'
draw_image_and_save(v, filename, draw_colorbar = True, cm = 'gray')

# Indeces for components that we will draw
vec_i = np.arange(0, 100).tolist()
vec_i = vec_i + np.arange(100, 1000, 100).tolist()
vec_i = vec_i + np.arange(1000, 10000, 500).tolist()
vec_i = vec_i + np.arange(9900, 10000, 5).tolist()
vec_i.append(points_num - 1)
vec_i = [i for i in vec_i if i < points_num]

# Indeces for timesteps that we will draw
steps = [0, 1, 10, 100, 5000, 10000, 20000, 50000, 100000, 120000, 200000, 300000, 400000, 500000, 595000]
steps = [s for s in steps if s in ts]
for s in steps:
    step = str(s).zfill(8)
    curr_eig_file = np.load(eig_path + '/nn_cov_' + step + '.npz')
    curr_vecs = curr_eig_file['eigenvecs']
    curr_vecs = curr_vecs.T
    curr_d_entries = curr_eig_file['d_entries']
    curr_inv_diag_entries = curr_eig_file['inv_diag_entries']
    curr_ind = np.where(ts == s)[0][0]
    curr_f = fs[curr_ind, :]
    curr_f_prj = f_prjs[curr_ind, :]

    curr_draw_path = eigvec_draw_path + '/time_' + step
    ensure_path_exists(curr_draw_path)

    # Draw interpolation of NN output
    v = interpolate_surf_image(curr_f, points, mesh_x1, mesh_x2)
    filename = curr_draw_path + '/nn_output.png'
    draw_image_and_save(v, filename, draw_colorbar = False, cm = 'gray')

    # Draw interpolation of G's diagonal
    v = interpolate_surf_image(curr_d_entries, points, mesh_x1, mesh_x2)
    filename = curr_draw_path + '/d_entries.png'
    draw_image_and_save(v, filename, draw_colorbar = False, cm = 'gray')

    # Draw interpolation of diagonal of G inverse
    v = interpolate_surf_image(curr_inv_diag_entries, points, mesh_x1, mesh_x2)
    filename = curr_draw_path + '/inv_diag_entries.png'
    draw_image_and_save(v, filename, draw_colorbar = False, cm = 'gray')

    # Compute Fourier Transform
    frq_func = np.matmul(curr_vecs.T, fourier_part)
    frq_func_magn = np.abs(frq_func)
    frq_func_magn = frq_func_magn/float(points_num)

    # Draw eigenvectors and their fourier transform
    for i in vec_i:
        # Eigenvectors
        v = curr_vecs[:,i]
        v = interpolate_surf_image(v, points, mesh_x1, mesh_x2)
        filename = curr_draw_path + '/v_' + str(i).zfill(8) + '.eps'
        draw_image_and_save(v, filename, font_size = 15, dpi = 50)
        filename = curr_draw_path + '/v_' + str(i).zfill(8) + '.jpg'
        draw_image_and_save(v, filename, font_size = 15, dpi = 50)

        # # "Eigencolumns"
        # v = curr_vecs[i,:]
        # v = interpolate_surf_image(v, points, mesh_x1, mesh_x2)
        # filename = curr_draw_path + '/z_' + str(i).zfill(8) + '.eps'
        # draw_image_and_save(v, filename, font_size = 15, dpi = 50)

        # Eigenvector frequencies
        f = np.reshape(frq_func_magn[i,:], mesh_f1.shape)
        filename = curr_draw_path + '/frq_' + str(i).zfill(8) + '.eps'
        contourf_with_colorbar_and_save(f, f1, f2, filename, cm = 'jet', font_size = 15, dpi = 50)
        filename = curr_draw_path + '/frq_' + str(i).zfill(8) + '.jpg'
        contourf_with_colorbar_and_save(f, f1, f2, filename, cm='jet', dpi=50)


    # Draw linear transformation of several first eigenvectors
    for i in [10, 100, 200, 300, 400, 500, 700, 900, 1000]:
        v = np.matmul(curr_vecs[:, 0:i], curr_f_prj[0:i])
        v = interpolate_surf_image(v, points, mesh_x1, mesh_x2)
        filename = curr_draw_path + '/c_' + str(i).zfill(8) + '.jpg'
        draw_image_and_save(v, filename, draw_colorbar = False, cm = 'gray', dpi = 50)


# Draw diagonal/inverse entries interpolation along time
draw_times = np.arange(np.min(ts), np.max(ts), 5000)
res = np.intersect1d(ts, draw_times, return_indices=True)
draw_indeces = res[1]
draw_times = ts[draw_indeces]

all_d_entries = []
all_inv_diag_entries = []
for i, t in enumerate(draw_times):
    step = str(t).zfill(8)
    curr_eig_file = np.load(eig_path + '/nn_cov_' + step + '.npz')
    curr_d_entries = curr_eig_file['d_entries'].flatten()
    curr_inv_diag_entries = curr_eig_file['inv_diag_entries'].flatten()
    all_d_entries.append(curr_d_entries)
    all_inv_diag_entries.append(curr_inv_diag_entries)
all_d_entries = np.stack(all_d_entries)
all_inv_diag_entries = np.stack(all_inv_diag_entries)

temp_path = output_path + '/temp/d_entries_draws'
filename = eigvec_draw_path + '/d_entries_draws.avi'
draw_func_interpolation_along_time(all_d_entries, draw_times, filename, temp_path, points, mesh_x1, mesh_x2, fig_size = [3, 8])
temp_path = output_path + '/temp/inv_diag_entries_draws'
filename = eigvec_draw_path + '/inv_diag_entries_draws.avi'
draw_func_interpolation_along_time(all_inv_diag_entries, draw_times, filename, temp_path, points, mesh_x1, mesh_x2, fig_size = [3, 8])
