import numpy as np
import sys
import os
os.chdir('../')
sys.path.insert(0, './Utils/')
from os import listdir
from os.path import isfile, join
from utils import *
from image_utils import *
from skimage import color
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 40})


# Load configuration
config = read_config('./train.properties')
res_dir = config['res_dir']

output_path = res_dir + '/figures'
ensure_path_exists(output_path)

dataset_file = np.load(res_dir + '/generated_dataset.npz')
points = dataset_file['points']

# Plot sampled points
ratio = 1192.0/800.0
h_inch = 8.0
w_inch = h_inch*ratio
fig = plt.figure(figsize=(h_inch, w_inch))
ax = fig.add_subplot(111)
plt.scatter(points[:,1], points[:,0], s = 8)
plt.gca().invert_yaxis()
plt.xlabel('$x_1$');
plt.ylabel('$x_2$');
xleft, xright = ax.get_xlim()
ybottom, ytop = ax.get_ylim()
ax.set_aspect(abs((xright-xleft)/(ybottom-ytop))*ratio)
filename = output_path + '/sampled_points_scatter.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()

# Plot target function
img = read_float_image('lisa_blurred.jpg')
img = color.rgb2gray(img)
filename = output_path + '/target_func.png'
save_image(img, filename, clip_if_needed = True, range_low_limit = 0.0, range_high_limit = 1.0)
