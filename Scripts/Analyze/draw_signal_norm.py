import numpy as np
import sys
import os
import scipy as sp
os.chdir('../')
sys.path.insert(0, './Utils/')
from os import listdir
from os.path import isfile, join
from utils import *
import matplotlib.pyplot as plt
plt.ioff()
plt.rcParams.update({'font.size': 30})


# Load configuration
config = read_config('./train.properties')
res_dir = config['res_dir']
eig_path = res_dir + '/nn_eigs_dec/'

output_path = res_dir + '/signal_norm'
ensure_path_exists(output_path)

info_file = np.load(res_dir + '/nn_stats/nn_spectrum_info.npz')
labels = info_file['labels']
labels = labels.ravel()
labels = labels.astype(np.float64)
summ_file = np.load(eig_path + '/summery.npz')
ts = summ_file['ts']
lrates = summ_file['lrates']
# Relative energy is square of cosine, where angle is between vector and its projection to k first eigenvectors
gt_rel_energies = summ_file['gt_rel_energies']
f_rel_energies = summ_file['f_rel_energies']
Ms = summ_file['Ms']
M0 = Ms[0,:]
fs = summ_file['fs']
M_prjs = summ_file['M_prjs']
M0_prjs = summ_file['M0_prjs']
f_prjs = summ_file['f_prjs']
gt_prjs = summ_file['gt_prjs']
GMs = summ_file['GMs']
all_vals = summ_file['all_vals']
#all_vals[all_vals < 0] = 1e-16
points_num = all_vals.shape[1]
all_inv_vals = 1.0/all_vals
all_inv_vals[np.logical_not(np.isfinite(all_inv_vals))] = 0.0
log_all_vals = np.log(all_vals)
log_all_vals[np.logical_not(np.isfinite(log_all_vals))] = 0.0


# Compute norm of different signals
gt_prjs_squared = np.square(gt_prjs.astype(np.float64))
gt_norms = np.sum(gt_prjs_squared, axis = 1)
gt_norm = np.sum(np.square(labels))
gt_G_norms = np.sum(gt_prjs_squared*all_vals, axis = 1)
inv_terms = gt_prjs_squared*all_inv_vals
inv_terms[np.logical_not(np.isfinite(inv_terms))] = 0.0
gt_G_inv_norms = np.sum(inv_terms, axis = 1)
f_prjs_squared = np.square(f_prjs.astype(np.float64))
f_norms = np.sum(f_prjs_squared, axis = 1)
f_G_norms = np.sum(f_prjs_squared*all_vals, axis = 1)
inv_terms = f_prjs_squared*all_inv_vals
inv_terms[np.logical_not(np.isfinite(inv_terms))] = 0.0
f_G_inv_norms = np.sum(inv_terms, axis = 1)
M_prjs_squared = np.square(M_prjs.astype(np.float64))
M_norms = np.sum(M_prjs_squared, axis = 1)
M_G_norms = np.sum(M_prjs_squared*all_vals, axis = 1)
inv_terms = M_prjs_squared*all_inv_vals
inv_terms[np.logical_not(np.isfinite(inv_terms))] = 0.0
M_G_inv_norms = np.sum(inv_terms, axis = 1)
M0_prjs_squared = np.square(M0_prjs.astype(np.float64))
M0_norms = np.sum(M0_prjs_squared, axis = 1)
M0_norm = np.sum(np.square(M0))
M0_G_norms = np.sum(M0_prjs_squared*all_vals, axis = 1)
inv_terms = M0_prjs_squared*all_inv_vals
inv_terms[np.logical_not(np.isfinite(inv_terms))] = 0.0
M0_G_inv_norms = np.sum(inv_terms, axis = 1)
G_traces = np.sum(all_vals, axis = 1)
G_inv_traces = np.sum(all_inv_vals, axis = 1)
G_logdets = np.sum(log_all_vals, axis = 1)
G_inv_logdets = -G_logdets


# Times at which to consider NN dynamics
draw_times = np.arange(np.min(ts), np.max(ts), 5000)
res = np.intersect1d(ts, draw_times, return_indices=True)
draw_indeces = res[1]


# Plot traces of G
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(ts[draw_indeces], G_traces[draw_indeces], linewidth=2)
plt.plot(ts[draw_indeces], G_inv_traces[draw_indeces], linewidth=2)
plt.xlabel('$t$');
plt.yscale('log')
plt.legend(['$tr (G_t)$', '$tr (G_t^{-1})$'], loc='center left', bbox_to_anchor=(1, 0.5))
filename = output_path + '/G_traces.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()


# Plot logdets of G
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(ts[draw_indeces], G_logdets[draw_indeces], linewidth=2)
plt.plot(ts[draw_indeces], G_inv_logdets[draw_indeces], linewidth=2)
plt.xlabel('$t$');
#plt.yscale('log')
plt.legend(['$\log det (G_t)$', '$\log det (G_t^{-1})$'], loc='center left', bbox_to_anchor=(1, 0.5))
filename = output_path + '/G_logdets.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()


# Plot norm of target function
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.axhline(y = gt_norm, linewidth=2)
plt.xlim(np.min(ts), np.max(ts))
plt.xlabel('$t$');
plt.ylabel('$\\bar{y}^T \\cdot \\bar{y}$');
#plt.yscale('log')
filename = output_path + '/gt_norm.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()

fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(ts[draw_indeces], gt_G_norms[draw_indeces], linewidth=2)
plt.xlabel('$t$');
plt.ylabel('$\\bar{y}^T \\cdot G_t \\cdot \\bar{y}$');
plt.yscale('log')
filename = output_path + '/gt_G_norm.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()

fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(ts[draw_indeces], gt_G_inv_norms[draw_indeces], linewidth=2)
plt.xlabel('$t$');
plt.ylabel('$\\bar{y}^T \\cdot G_t^{-1} \\cdot \\bar{y}$');
plt.yscale('log')
filename = output_path + '/gt_G_inv_norm.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()


# Plot norm of M0
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.axhline(y = M0_norm, linewidth=2)
plt.xlim(np.min(ts), np.max(ts))
plt.xlabel('$t$');
plt.ylabel('$\\bar{m}_0^T \\cdot \\bar{m}_0$');
#plt.yscale('log')
filename = output_path + '/M0_norm.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()

fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(ts[draw_indeces], M0_G_norms[draw_indeces], linewidth=2)
plt.xlabel('$t$');
plt.ylabel('$\\bar{m}_0^T \\cdot G_t \\cdot \\bar{m}_0$');
plt.yscale('log')
filename = output_path + '/M0_G_norm.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()

fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(ts[draw_indeces], M0_G_inv_norms[draw_indeces], linewidth=2)
plt.xlabel('$t$');
plt.ylabel('$\\bar{m}_0^T \\cdot G_t^{-1} \\cdot \\bar{m}_0$');
plt.yscale('log')
filename = output_path + '/M0_G_inv_norm.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()


# Plot norm of NN outputs
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(ts[draw_indeces], f_norms[draw_indeces], linewidth=2)
plt.xlabel('$t$');
plt.ylabel('$\\bar{f}_t^T \\cdot \\bar{f}_t$');
plt.yscale('log')
filename = output_path + '/f_norm.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()

fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(ts[draw_indeces], f_G_norms[draw_indeces], linewidth=2)
plt.xlabel('$t$');
plt.ylabel('$\\bar{f}_t^T \\cdot G_t \\cdot \\bar{f}_t$');
plt.yscale('log')
filename = output_path + '/f_G_norm.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()

fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(ts[draw_indeces], f_G_inv_norms[draw_indeces], linewidth=2)
plt.xlabel('$t$');
plt.ylabel('$\\bar{f}_t^T \\cdot G_t^{-1} \\cdot \\bar{f}_t$');
plt.yscale('log')
filename = output_path + '/f_G_inv_norm.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()


# Plot norm of M
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(ts[draw_indeces], M_norms[draw_indeces], linewidth=2)
plt.xlabel('$t$');
plt.ylabel('$\\bar{m}_t^T \\cdot \\bar{m}_t$');
plt.yscale('log')
filename = output_path + '/M_norm.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()

fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(ts[draw_indeces], M_G_norms[draw_indeces], linewidth=2)
plt.xlabel('$t$');
plt.ylabel('$\\bar{m}_t^T \\cdot G_t \\cdot \\bar{m}_t$');
plt.yscale('log')
filename = output_path + '/M_G_norm.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()

fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(ts[draw_indeces], M_G_inv_norms[draw_indeces], linewidth=2)
plt.xlabel('$t$');
plt.ylabel('$\\bar{m}_t^T \\cdot G_t^{-1} \\cdot \\bar{m}_t$');
plt.yscale('log')
filename = output_path + '/M_G_inv_norm.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()

# Plot norms together
fig = plt.figure(figsize=(8, 6))
fnt_size = 10
ax = fig.add_subplot(221)
set_ax_font_size(ax, fnt_size)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(ts[draw_indeces], gt_G_norms[draw_indeces], linewidth=2)
plt.xlabel('$t$');
plt.ylabel('$\\bar{y}^T \\cdot G_t \\cdot \\bar{y}$');
plt.yscale('log')
ax = fig.add_subplot(222)
set_ax_font_size(ax, fnt_size)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(ts[draw_indeces], gt_G_inv_norms[draw_indeces], linewidth=2)
plt.xlabel('$t$');
plt.ylabel('$\\bar{y}^T \\cdot G_t^{-1} \\cdot \\bar{y}$');
plt.yscale('log')
ax = fig.add_subplot(223)
set_ax_font_size(ax, fnt_size)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(ts[draw_indeces], f_G_norms[draw_indeces], linewidth=2)
plt.xlabel('$t$');
plt.ylabel('$\\bar{f}_t^T \\cdot G_t \\cdot \\bar{f}_t$');
plt.yscale('log')
ax = fig.add_subplot(224)
set_ax_font_size(ax, fnt_size)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(ts[draw_indeces], f_G_inv_norms[draw_indeces], linewidth=2)
plt.xlabel('$t$');
plt.ylabel('$\\bar{f}_t^T \\cdot G_t^{-1} \\cdot \\bar{f}_t$');
plt.yscale('log')
filename = output_path + '/gt_f_norms.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()




# Plot norms together
fig = plt.figure(figsize=(8, 6))
fnt_size = 10
ax = fig.add_subplot(221)
set_ax_font_size(ax, fnt_size)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(ts[draw_indeces], (gt_G_norms/G_traces)[draw_indeces], linewidth=2)
plt.xlabel('$t$');
plt.ylabel('$\\bar{y}^T \\cdot G_t \\cdot \\bar{y}$');
plt.yscale('log')
ax = fig.add_subplot(222)
set_ax_font_size(ax, fnt_size)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(ts[draw_indeces], (gt_G_inv_norms/G_traces)[draw_indeces], linewidth=2)
plt.xlabel('$t$');
plt.ylabel('$\\bar{y}^T \\cdot G_t^{-1} \\cdot \\bar{y}$');
plt.yscale('log')
ax = fig.add_subplot(223)
set_ax_font_size(ax, fnt_size)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(ts[draw_indeces], (f_G_norms/G_traces)[draw_indeces], linewidth=2)
plt.xlabel('$t$');
plt.ylabel('$\\bar{f}_t^T \\cdot G_t \\cdot \\bar{f}_t$');
plt.yscale('log')
ax = fig.add_subplot(224)
set_ax_font_size(ax, fnt_size)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(ts[draw_indeces], (f_G_inv_norms/G_traces)[draw_indeces], linewidth=2)
plt.xlabel('$t$');
plt.ylabel('$\\bar{f}_t^T \\cdot G_t^{-1} \\cdot \\bar{f}_t$');
plt.yscale('log')
filename = output_path + '/gt_f_norms_normalized.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()


# cov_file = np.load(res_dir + '/nn_stats/nn_cov_00595000.npz')
# covE = cov_file['cov']
# GMs_end = np.matmul(Ms.astype(np.float64), covE.astype(np.float64))
# M_G_norms_end = np.sum(GMs_end*Ms, axis = 1)
# inv_covE , info = sp.linalg.lapack.dpotri(covE)
# GinvMs_end = np.matmul(Ms.astype(np.float64), inv_covE.astype(np.float64))
# M_G_inv_norms_end = np.sum(GinvMs_end*Ms, axis = 1)
#
# fig = plt.figure(figsize=(8, 6))
# ax = fig.add_subplot(111)
# for x in range(100000, 500001, 100000):
#     plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
# plt.plot(ts[draw_indeces], M_G_norms_end[draw_indeces], linewidth=2)
# plt.xlabel('$t$');
# plt.ylabel('$\\bar{m}_t^T \\cdot G_T \\cdot \\bar{m}_t$');
# plt.yscale('log')
# filename = output_path + '/M_G_norm_end_G.eps'
# plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
# plt.close()
#
# fig = plt.figure(figsize=(8, 6))
# ax = fig.add_subplot(111)
# for x in range(100000, 500001, 100000):
#     plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
# plt.plot(ts[draw_indeces], M_G_inv_norms_end[draw_indeces], linewidth=2)
# plt.xlabel('$t$');
# plt.ylabel('$\\bar{m}_t^T \\cdot (G_T)^{-1} \\cdot \\bar{m}_t$');
# plt.yscale('log')
# filename = output_path + '/M_G_inv_norm_end_G.eps'
# plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
# plt.close()
