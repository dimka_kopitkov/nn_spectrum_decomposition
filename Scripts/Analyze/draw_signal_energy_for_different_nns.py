import numpy as np
import sys
import os
os.chdir('../')
sys.path.insert(0, './Utils/')
from os import listdir
from os.path import isfile, join
from utils import *
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 30})


# Load configuration
res_dir = '/media/dima/Storage01/NNSpectrum'
eig_path_2L = res_dir + '/TrainOutcome_2L/nn_eigs_dec/'
eig_path_4L = res_dir + '/TrainOutcome_4L/nn_eigs_dec/'
eig_path_6L = res_dir + '/TrainOutcome_6L/nn_eigs_dec/'
eig_path_2L_33000 = res_dir + '/TrainOutcome_2L_33000/nn_eigs_dec/'
eig_path_2L_66000 = res_dir + '/TrainOutcome_2L_66000/nn_eigs_dec/'

output_path = res_dir + '/figures'
ensure_path_exists(output_path)


def get_gt_and_f_relative_energy(eig_path, sp_thr):
    summ_file = np.load(eig_path + '/summery.npz')
    ts = summ_file['ts']
    gt_rel_energies = summ_file['gt_rel_energies']
    f_rel_energies = summ_file['f_rel_energies']

    # Times at which to consider NN dynamics
    draw_times = np.arange(np.min(ts), np.max(ts), 5000)
    res = np.intersect1d(ts, draw_times, return_indices=True)
    draw_indeces = res[1]

    ts = ts[draw_indeces]
    gt_rel_energies = gt_rel_energies[draw_indeces,sp_thr]
    f_rel_energies = f_rel_energies[draw_indeces,sp_thr]

    return [ts, gt_rel_energies, f_rel_energies]

# Get relative energy of ground-truth for each trained NN
sp_thr = 400
[ts, gt_rel_energies_2L, f_rel_energies_2L] = get_gt_and_f_relative_energy(eig_path_2L, sp_thr)
[ts, gt_rel_energies_4L, f_rel_energies_4L] = get_gt_and_f_relative_energy(eig_path_4L, sp_thr)
[ts, gt_rel_energies_6L, f_rel_energies_6L] = get_gt_and_f_relative_energy(eig_path_6L, sp_thr)
[ts, gt_rel_energies_2L_33000, f_rel_energies_2L_33000] = get_gt_and_f_relative_energy(eig_path_2L_33000, sp_thr)
[ts, gt_rel_energies_2L_66000, f_rel_energies_2L_66000] = get_gt_and_f_relative_energy(eig_path_2L_66000, sp_thr)

# Plot relative energy of target function in specific part of NN spectrum
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(ts, gt_rel_energies_2L, linewidth=2)
plt.plot(ts, gt_rel_energies_4L, linewidth=2)
plt.plot(ts, gt_rel_energies_6L, linewidth=2)
plt.plot(ts, gt_rel_energies_2L_33000, linewidth=2)
plt.plot(ts, gt_rel_energies_2L_66000, linewidth=2)
plt.legend(['2L, 256W, $|\\theta| = 1025$',
            '4L, 256W, $|\\theta| = 132609$',
            '6L, 256W, $|\\theta| = 264193$',
            '2L, 33000W, $|\\theta| = 132001$',
            '2L, 66000W, $|\\theta| = 264001$'], loc='lower right', prop={'size': 20})
plt.xlabel('$t$');
plt.ylabel('Energy');
#plt.ylabel('$E_t(\\bar{y}, ' + str(sp_thr) + ')$');
filename = output_path + '/gt_energy_along_opt_dfrt_nns1.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()

# Plot angle between target function and its projection onto specific part of NN spectrum
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(ts, np.sqrt(gt_rel_energies_2L), linewidth=2)
plt.plot(ts, np.sqrt(gt_rel_energies_4L), linewidth=2)
plt.plot(ts, np.sqrt(gt_rel_energies_6L), linewidth=2)
plt.plot(ts, np.sqrt(gt_rel_energies_2L_33000), linewidth=2)
plt.plot(ts, np.sqrt(gt_rel_energies_2L_66000), linewidth=2)
plt.legend(['2L, 256W, $|\\theta| = 1025$',
            '4L, 256W, $|\\theta| = 132609$',
            '6L, 256W, $|\\theta| = 264193$',
            '2L, 33000W, $|\\theta| = 132001$',
            '2L, 66000W, $|\\theta| = 264001$'], loc='lower right', prop={'size': 20})
plt.xlabel('$t$');
plt.ylabel('$cos\\left[\\alpha_t \\left( \\bar{y}, ' + str(sp_thr) + ' \\right) \\right]$');
filename = output_path + '/gt_cos_along_opt_dfrt_nns1.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()

# Plot relative energy of target function in specific part of NN spectrum,
# for NNs with 2 layers of different width
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(ts, gt_rel_energies_2L, linewidth=2)
plt.plot(ts, gt_rel_energies_2L_33000, linewidth=2)
plt.plot(ts, gt_rel_energies_2L_66000, linewidth=2)
plt.legend(['2L, 256', '2L, 33000', '2L, 66000'], loc='lower right')
plt.xlabel('$t$');
plt.ylabel('Energy');
#plt.ylabel('$E_t(\\bar{y}, ' + str(sp_thr) + ')$');
filename = output_path + '/gt_energy_along_opt_dfrt_nns2.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()


# Plot relative energy of f in specific part of NN spectrum
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(ts, f_rel_energies_2L, linewidth=2)
plt.plot(ts, f_rel_energies_4L, linewidth=2)
plt.plot(ts, f_rel_energies_6L, linewidth=2)
plt.plot(ts, f_rel_energies_2L_33000, linewidth=2)
plt.plot(ts, f_rel_energies_2L_66000, linewidth=2)
plt.legend(['2L, 256W, $|\\theta| = 1025$',
            '4L, 256W, $|\\theta| = 132609$',
            '6L, 256W, $|\\theta| = 264193$',
            '2L, 33000W, $|\\theta| = 132001$',
            '2L, 66000W, $|\\theta| = 264001$'], loc='center left', prop={'size': 20}, bbox_to_anchor=(1, 0.5))
plt.xlabel('$t$');
plt.ylabel('$E_t(\\bar{f}_t, ' + str(sp_thr) + ')$');
filename = output_path + '/f_energy_along_opt_dfrt_nns1.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0.05, transparent=True, dpi=200)
plt.close()

