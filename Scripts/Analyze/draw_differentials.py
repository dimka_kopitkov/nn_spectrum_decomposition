import numpy as np
import sys
import os
os.chdir('../')
sys.path.insert(0, './Utils/')
from os import listdir
from os.path import isfile, join
from utils import *
import matplotlib.pyplot as plt
plt.ioff()
from matplotlib import rc
plt.rcParams.update({'font.size': 30})


# Load configuration
config = read_config('./train.properties')
res_dir = config['res_dir']
eig_path = res_dir + '/nn_eigs_dec/'

output_path = res_dir + '/dfrntial_drawings'
ensure_path_exists(output_path)

info_file = np.load(res_dir + '/nn_stats/nn_spectrum_info.npz')
points = info_file['points']
points_num = points.shape[0]

summ_file = np.load(eig_path + '/summery.npz')
ts = summ_file['ts']
fs = summ_file['fs']
GMs = summ_file['GMs']
lrates = summ_file['lrates']


# Times at which to compute differential
before_times = np.arange(np.min(ts), np.max(ts), 5000)
after_times = before_times + 1
res = np.intersect1d(ts, before_times, return_indices=True)
before_indeces = res[1]
before_times = ts[before_indeces]
res = np.intersect1d(ts, after_times, return_indices=True)
after_indeces = res[1]
after_times = ts[after_indeces]

# Compute real differentials
f_before = fs[before_indeces,:]
f_after = fs[after_indeces,:]
real_differential = f_after - f_before

# Compute estimated differentials
GMs_before = GMs[before_indeces,:]
lrates_before = lrates[before_indeces]
est_differential = -GMs_before*lrates_before[:,np.newaxis]/float(points_num)
higher_order_differential =  real_differential - est_differential

# Compute spectrum of higher order differentials
high_prjs_list = []
for i, t in enumerate(before_times):
    time_step = str(t).zfill(8)

    cov_file = np.load(res_dir + '/nn_eigs_dec/nn_cov_' + time_step + '.npz')
    vecs = cov_file['eigenvecs']
    vecs = vecs.astype(np.float64)
    vecs = vecs.T

    high_prjs = np.matmul(vecs.T, higher_order_differential[i,:].astype(np.float64).T)
    high_prjs_list.append(high_prjs)

high_prjs = np.stack(high_prjs_list)
high_prjs_squared = np.square(high_prjs)
high_prjs_l2 = np.sum(high_prjs_squared, axis = 1)
high_prjs_acc_energy = np.cumsum(high_prjs_squared, axis = 1)
high_prjs_rel_acc_energy = high_prjs_acc_energy / high_prjs_l2[:,np.newaxis]
high_prjs_rel_acc_energy.shape


# Plot relative energy of higher order differentials in specific part of NN spectrum
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
sp_thrs = [5, 10, 20, 50, 100, 1000, 4000]
sp_thrs = [t for t in sp_thrs if t < points_num]
for sp_thr in sp_thrs:
    plt.plot(before_times, high_prjs_rel_acc_energy[:,sp_thr], linewidth=2)
plt.legend(sp_thrs, loc='center left', bbox_to_anchor=(1, 0.5))
plt.xlabel('$t$');
plt.ylabel('Energy');
filename = output_path + '/higher_energy_along_opt1.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()

fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
sp_thrs = [60, 90, 120, 200, 400, 1000, 4000]
sp_thrs = [t for t in sp_thrs if t < points_num]
for sp_thr in sp_thrs:
    plt.plot(before_times, high_prjs_rel_acc_energy[:,sp_thr], linewidth=2)
en1 = high_prjs_rel_acc_energy[:,:]
en1 = en1[:,sp_thrs]
plt.ylim(np.min(en1), 1.0)
plt.legend(sp_thrs, loc='center left', bbox_to_anchor=(1, 0.5))
plt.xlabel('$t$');
plt.ylabel('Energy');
filename = output_path + '/higher_energy_along_opt2.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()


# Space grid for interpolation
x1 = np.linspace(0.0, 1.0, 1192)
x2 = np.linspace(0.0, 1.0, 800)
mesh_x1, mesh_x2 = np.meshgrid(x1, x2)

# Draw all the differentials
for i, t in enumerate(before_times):
    time_step = str(t).zfill(8)
    v = interpolate_surf_image(real_differential[i,:], points, mesh_x1, mesh_x2)
    filename = output_path + '/real_' + time_step + '.png'
    draw_image_and_save(v, filename, draw_colorbar = True, cm = 'gray')

    v = interpolate_surf_image(est_differential[i,:], points, mesh_x1, mesh_x2)
    filename = output_path + '/first_order_' + time_step + '.png'
    draw_image_and_save(v, filename, draw_colorbar = True, cm = 'gray')

    v = interpolate_surf_image(higher_order_differential[i,:], points, mesh_x1, mesh_x2)
    filename = output_path + '/higher_' + time_step + '.png'
    draw_image_and_save(v, filename, draw_colorbar = True, cm = 'gray')
