import numpy as np
import sys
import os
os.chdir('../')
sys.path.insert(0, './Utils/')
from os import listdir
from os.path import isfile, join
from utils import *
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 30})


# Load configuration
res_dir = '/media/dima/Storage01/NNSpectrum'
eig_path_2L = res_dir + '/TrainOutcome_2L/nn_eigs_dec/'
eig_path_4L = res_dir + '/TrainOutcome_4L/nn_eigs_dec/'
eig_path_6L = res_dir + '/TrainOutcome_6L/nn_eigs_dec/'
eig_path_2L_33000 = res_dir + '/TrainOutcome_2L_33000/nn_eigs_dec/'
eig_path_2L_66000 = res_dir + '/TrainOutcome_2L_66000/nn_eigs_dec/'

output_path = res_dir + '/figures'
ensure_path_exists(output_path)


def get_training_error_and_eigvals(eig_path):
    summ_file = np.load(eig_path + '/summery.npz')
    ts = summ_file['ts']
    fs = summ_file['fs']
    all_vals = summ_file['all_vals']

    info_file = np.load(eig_path + '/../nn_stats/nn_spectrum_info.npz')
    labels = info_file['labels']
    labels = labels.ravel()

    residual = fs - labels[np.newaxis, :]
    train_loss = 0.5 * np.mean(np.square(residual), axis=1)

    return [ts, train_loss, all_vals]

# Compute training error for each trained NN
[ts, te_2L, vals_2L] = get_training_error_and_eigvals(eig_path_2L)
[ts, te_4L, vals_4L] = get_training_error_and_eigvals(eig_path_4L)
[ts, te_6L, vals_6L] = get_training_error_and_eigvals(eig_path_6L)
[ts, te_2L_33000, vals_2L_33000] = get_training_error_and_eigvals(eig_path_2L_33000)
[ts, te_2L_66000, vals_2L_66000] = get_training_error_and_eigvals(eig_path_2L_66000)

# Times at which to consider NN dynamics
draw_times = np.arange(np.min(ts), np.max(ts), 5000)
res = np.intersect1d(ts, draw_times, return_indices=True)
draw_indeces = res[1]

# Plot training error
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.plot(ts[draw_indeces], te_2L[draw_indeces], linewidth=2)
plt.plot(ts[draw_indeces], te_4L[draw_indeces], linewidth=2)
plt.plot(ts[draw_indeces], te_6L[draw_indeces], linewidth=2)
plt.plot(ts[draw_indeces], te_2L_33000[draw_indeces], linewidth=2)
plt.plot(ts[draw_indeces], te_2L_66000[draw_indeces], linewidth=2)
plt.legend(['2L, 256W, $|\\theta| = 1025$',
            '4L, 256W, $|\\theta| = 132609$',
            '6L, 256W, $|\\theta| = 264193$',
            '2L, 33000W, $|\\theta| = 132001$',
            '2L, 66000W, $|\\theta| = 264001$'], loc='upper right', prop={'size': 16})
plt.xlabel('$t$');
plt.ylabel('Train Loss');
plt.yscale('log')
filename = output_path + '/train_loss_dfrt_nns.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()


# Plot eigenvalues of each model at time t=600000
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
plt.plot(vals_2L[-1,:], linewidth=2)
plt.plot(vals_4L[-1,:], linewidth=2)
plt.plot(vals_6L[-1,:], linewidth=2)
plt.plot(vals_2L_33000[-1,:], linewidth=2)
plt.plot(vals_2L_66000[-1,:], linewidth=2)
plt.legend(['2L, 256W, $|\\theta| = 1025$',
            '4L, 256W, $|\\theta| = 132609$',
            '6L, 256W, $|\\theta| = 264193$',
            '2L, 33000W, $|\\theta| = 132001$',
            '2L, 66000W, $|\\theta| = 264001$'], loc='upper right', prop={'size': 16})
plt.xlabel('$i$');
plt.ylabel('$\lambda_i$');
plt.yscale('log')
filename = output_path + '/eigvalues_dfrt_nns.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()
