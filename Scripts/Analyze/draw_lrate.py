import numpy as np
import sys
import os
os.chdir('../')
sys.path.insert(0, './Utils/')
from os import listdir
from os.path import isfile, join
from utils import *
import matplotlib.pyplot as plt
from matplotlib import rc
#rc('font',**{'family':str('sans-serif'),'sans-serif':['Helvetica'], 'size': 30})
#rc('font',**{'family':'serif','serif':['Palatino'], 'size': 30})
#rc('text', usetex=True)
plt.rcParams.update({'font.size': 30})
#plt.rcParams.update({'font.weight': 'bold'})


# Load configuration
config = read_config('./train.properties')
res_dir = config['res_dir']
eig_path = res_dir + '/nn_eigs_dec/'

output_path = res_dir + '/figures'
ensure_path_exists(output_path)

summ_file = np.load(eig_path + '/summery.npz')
ts = summ_file['ts']
lrates = summ_file['lrates']
all_vals = summ_file['all_vals']
max_vals = all_vals[:,0]
points_num = all_vals.shape[1]
up_thr1 = (2.0*float(points_num))/max_vals
up_thr2 = (1.0*float(points_num))/max_vals

# Times at which to consider NN dynamics
draw_times = np.arange(np.min(ts), np.max(ts), 5000)
res = np.intersect1d(ts, draw_times, return_indices=True)
draw_indeces = res[1]
draw_lrates = lrates[draw_indeces]
draw_times = ts[draw_indeces]

decay_times = np.arange(100000, 600000, 100000)
res = np.intersect1d(draw_times, decay_times, return_indices=True)
decay_indeces = res[1]
decay_times = draw_times[decay_indeces]
before_decay_lrates = draw_lrates[decay_indeces - 1]
before_decay_times = decay_times - 1

lrates_with_decay = np.concatenate([draw_lrates, before_decay_lrates])
times_with_decay = np.concatenate([draw_times, before_decay_times])
indeces = np.argsort(times_with_decay)
lrates_with_decay = lrates_with_decay[indeces]
times_with_decay = times_with_decay[indeces]

# Plot learning rate and its up stability boundary
fig = plt.figure(figsize=(8, 6))
ax = fig.add_subplot(111)
for x in range(100000, 500001, 100000):
    plt.axvline(x=x, color = 'k', linewidth = 0.5, dashes=[6, 10], label='_nolegend_')
plt.step(times_with_decay, lrates_with_decay, linewidth=2)
plt.plot(ts[draw_indeces], up_thr1[draw_indeces], linewidth=2)
#plt.plot(ts[draw_indeces], up_thr2[draw_indeces], linewidth=2)
plt.yscale('log')
plt.xlabel('$t$');
plt.legend(['$\\delta_t$', '$\\frac{2N}{\lambda_{max}^{t}}$'], loc='upper right')
filename = output_path + '/lrate_and_boundary.eps'
plt.savefig(filename, bbox_inches='tight', pad_inches=0, transparent=True, dpi=200)
plt.close()
