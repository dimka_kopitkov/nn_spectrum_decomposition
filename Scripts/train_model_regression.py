import sys
import numpy as np
import tensorflow as tf
from shutil import copyfile
sys.path.insert(0, './Models/')
sys.path.insert(0, './Utils/')
from utils import *
from image_utils import *
from ImageFnctn import *
from FCNNModel import *
from NNSpectrumCollector import *
from GeneratorDataset import *
from NumpyGenerator import *
from tensorflow.keras import datasets


# Read run configuration
config_fn = './train.properties'
config = read_config(config_fn)
res_dir = config['res_dir']
ensure_path_exists(res_dir)
copyfile(config_fn, res_dir + config_fn)
model_dir = res_dir + '/trained_model/'
ensure_path_exists(model_dir)
temp_dir = config['temp_dir']
ensure_path_exists(temp_dir)
image_output_dir = res_dir + '/images/'
ensure_path_exists(image_output_dir)
it_num = int(float(config['it_num']))
dtype = np.float32
dtype_tf = npdtype_2_tfdtype(dtype)
lrate = float(config['lrate'])
calc_nn_spectrum = (config['calc_nn_spectrum'] == 'True')

# Prepare dataset
dataset_size = int(float(config['dataset_size']))
dataset_type = str(config['dataset_type'])
train_map = dict()

if (dataset_type == 'buzz'):
    dt_file = np.load('./Datasets/buzz.npz')
    points = dt_file['points']
    labels = dt_file['labels']
    dt_file.close()

    train_map['points'] = points[0:dataset_size,:]
    train_map['labels'] = labels[0:dataset_size,:]
    train_map['test_points'] = points[dataset_size:(dataset_size + 10000),:]
    train_map['test_labels'] = labels[dataset_size:(dataset_size + 10000),:]
elif (dataset_type == 'cifar100'):
    # CIFAR100 dataset
    (train_images, train_labels), (test_images, test_labels) = datasets.cifar100.load_data()
    [train_images, train_labels] = reduce_dataset_size(train_images, train_labels, dataset_size)
    [test_images, test_labels] = reduce_dataset_size(test_images, test_labels, dataset_size)
    train_images = np.reshape(train_images, [train_images.shape[0], -1]).astype(np.float64)
    test_images = np.reshape(test_images, [test_images.shape[0], -1]).astype(np.float64)
    train_map['points'] = train_images
    train_map['labels'] = train_labels
    train_map['test_points'] = test_images
    train_map['test_labels'] = test_labels
elif (dataset_type == 'mnist'):
    # MNIST dataset
    (train_images, train_labels), (test_images, test_labels) = datasets.mnist.load_data()
    [train_images, train_labels] = reduce_dataset_size(train_images, train_labels, dataset_size)
    [test_images, test_labels] = reduce_dataset_size(test_images, test_labels, dataset_size)
    train_images = np.reshape(train_images, [train_images.shape[0], -1]).astype(np.float64)
    test_images = np.reshape(test_images, [test_images.shape[0], -1]).astype(np.float64)
    indeces = np.argsort(test_labels)
    test_images = test_images[indeces,...]
    test_labels = test_labels[indeces]
    train_map['points'] = train_images
    train_map['labels'] = train_labels
    train_map['test_points'] = test_images
    train_map['test_labels'] = test_labels
elif (dataset_type == 'mnist_noisy'):
    # MNIST dataset with noise
    (train_images, train_labels), (test_images, test_labels) = datasets.mnist.load_data()
    [train_images, train_labels] = reduce_dataset_size(train_images, train_labels, dataset_size)
    [test_images, test_labels] = reduce_dataset_size(test_images, test_labels, dataset_size)
    train_images = np.reshape(train_images, [train_images.shape[0], -1]).astype(np.float64)
    test_images = np.reshape(test_images, [test_images.shape[0], -1]).astype(np.float64)
    train_images = train_images + 0.01*np.random.randn(train_images.shape[0], train_images.shape[1])
    test_images = test_images + 0.01*np.random.randn(test_images.shape[0], test_images.shape[1])
    train_labels = train_labels.astype(np.float64)
    #train_labels = train_labels + np.random.randn(train_labels.shape[0])
    train_labels = train_labels[:,np.newaxis]
    test_labels = test_labels.astype(np.float64)
    #test_labels = test_labels + np.random.randn(test_labels.shape[0])
    test_labels = test_labels[:,np.newaxis]
    train_map['points'] = train_images
    train_map['labels'] = train_labels
    train_map['test_points'] = test_images
    train_map['test_labels'] = test_labels
elif (dataset_type == 'lisa'):
    # Lisa dataset
    train_map['points'] = np.random.uniform(size=[dataset_size, 2])
    train_map['test_points'] = np.random.uniform(size=[dataset_size, 2])
    gt_func = ImageFnctn('./lisa_blurred.jpg')
    train_map['labels'] = gt_func.func_output(train_map['points'])
    train_map['test_labels'] = gt_func.func_output(train_map['test_points'])


# Dataset normalization
train_map['means'] = np.mean(train_map['points'], axis = 0)
train_map['stds'] = np.std(train_map['points'], axis = 0)
generated_dataset_file = res_dir + 'generated_dataset.npz'
np.savez(generated_dataset_file, **train_map)


def construct_lr_tensor(init_lrate, global_step, lrate_constant):
    if (lrate_constant):
        lr_tensor = tf.constant(init_lrate, dtype = dtype_tf)
    else:
        lr_tensor = tf.train.exponential_decay(
                                            learning_rate = init_lrate,
                                            global_step = global_step,
                                            decay_steps = 100000,
                                            decay_rate = 0.5,
                                            staircase = True)

    return lr_tensor

    # p1_end = 450000
    # p2_end = 600000
    # p3_end = 1000000
    # #p1_end = 4500
    #
    # def p1(): return tf.train.exponential_decay(
    #                                     learning_rate = init_lrate,
    #                                     global_step = global_step,
    #                                     decay_steps = 100000,
    #                                     decay_rate = 0.5,
    #                                     staircase = True)
    #
    # def p2(): return init_lrate/16.0
    #
    # def p3(): return init_lrate / 32.0
    #
    # def p4(): return init_lrate / 64.0
    #
    # lr_tensor = tf.case({tf.less(global_step, np.array(p1_end, dtype = np.int64)): p1,
    #                      is_between(global_step, np.array(p1_end, dtype=np.int64), np.array(p2_end, dtype=np.int64)): p2,
    #                      is_between(global_step, np.array(p2_end, dtype=np.int64), np.array(p3_end, dtype=np.int64)): p3,
    #                      tf.greater_equal(global_step, np.array(p3_end, dtype = np.int64)): p4},
    #                     default=p1, exclusive=False)
    # return lr_tensor


# Create model on gpu for better performance
with tf.device(config['device']):
    model = FCNNModel(config, train_map['means'], train_map['stds'])

    # Create train and test losses
    opt_name = str(config['opt_name'])
    if (opt_name == 'SGD'):
        dtype_map = dict()
        dtype_map['points'] = dtype;
        dtype_map['labels'] = dtype;
        batch_size = int(float(str(config['batch_size'])))
        gen = NumpyGenerator({'points': train_map['points'], 'labels': train_map['labels']},
                             dtype_map=dtype_map,
                             batch_size=batch_size,
                             is_infinite_loop_mode=True);
        with tf.device('/cpu:0'):
            dt_generator = GeneratorDataset(gen)
            dt_iterator = dt_generator.get_dt_object().make_initializable_iterator()
            train_data_element = dt_iterator.get_next()
        X_tensor = train_data_element['points']
        Y_tensor = train_data_element['labels']
    else:
        X_tensor = tf.convert_to_tensor(train_map['points'], dtype=dtype_tf)
        Y_tensor = tf.convert_to_tensor(train_map['labels'], dtype=dtype_tf)

    train_sqr_loss = construct_sqr_loss_from_tensors(X_tensor, Y_tensor, model)
    test_sqr_loss = construct_sqr_loss_from_data(train_map['test_points'], train_map['test_labels'], model, dtype_tf)

    # Define learning rate
    global_step = tf.train.get_or_create_global_step()
    lrate_constant = (config['lrate_constant'] == 'True')
    lr_tensor = construct_lr_tensor(lrate, global_step, lrate_constant)

    # Define an optimizer
    if (opt_name == 'GD' or opt_name == 'SGD'):
        optimizer = tf.train.GradientDescentOptimizer(learning_rate=lr_tensor)
    elif (opt_name == 'Adam'):
        optimizer = tf.train.AdamOptimizer(learning_rate=lr_tensor, beta1=0.75, beta2=0.999, epsilon=1e-10)
    total_model_vars = model.get_list_of_model_variables()
    print_dim_of_variables(total_model_vars)
    train_op = optimizer.minimize(train_sqr_loss, var_list = total_model_vars, global_step = global_step)

    if (dataset_type == 'lisa'):
        # Tensors for drawing NN output as image
        image_points = gt_func.imagePoints
        image_points = cartesian_product(image_points)
        image_points_tensor = tf.convert_to_tensor(image_points, dtype=dtype_tf)
        with tf.device('/cpu:0'):
            dt = tf.data.Dataset.from_tensor_slices({'data': image_points_tensor})
            batch_dt = dt.batch(10000)
            iterator = batch_dt.make_initializable_iterator()
        next_element = iterator.get_next()
        image_points_output = model.create_model_graph(next_element['data'])

    # Create spectrum collector
    files_output_dir = res_dir + '/nn_stats/'
    ensure_path_exists(files_output_dir)
    # If training dataset is too large, we will calculate G on first 10000 training points,
    # since computing G on huge training dataset is intractable
    if (dataset_size > 10000):
        spec_map = dict()
        spec_map['points'] = train_map['points'][0:10000,...]
        spec_map['labels'] = train_map['labels'][0:10000,...]
        spec_map['test_points'] = train_map['test_points'][0:10000,...]
        spec_map['test_labels'] = train_map['test_labels'][0:10000,...]
    else:
        spec_map = train_map
    spec_collector = NNSpectrumCollector(dtype = dtype, output_dir = files_output_dir, config = config, train_map = spec_map)
    spec_collector.create_tensors(model, lr_tensor)



saver = tf.train.Saver(total_model_vars)

with tf.Session() as sess:
    # Intialize the model
    sess.run(tf.global_variables_initializer())
    if (opt_name == 'SGD'):
        sess.run(dt_iterator.initializer)

    # Optimize NN
    ts = []
    train_losses = []
    test_losses = []
    for step in range(it_num):

        if(calc_nn_spectrum):
            # Collect NN spectrum
            spec_collector.update_before_iteration_start(sess, step);

        # Compute current performance
        if (step % 100 == 0) or (step == it_num - 1):
            data = sess.run([train_sqr_loss, test_sqr_loss])
            curr_train_loss = data[0]
            curr_test_loss = data[1]
            ts.append(step)
            train_losses.append(curr_train_loss)
            test_losses.append(curr_test_loss)

            msg = "\r- Iteration: {0:>6}, Train L2 Loss is: {1:<13}, Test L2 Loss is: {2:<13}".format(step, curr_train_loss, curr_test_loss)
            sys.stdout.write(msg)
            sys.stdout.flush()

        # Draw NN output as image
        if (dataset_type == 'lisa' and step % 5000 == 0):
            sess.run(iterator.initializer)
            image_values = fetch_all_tensor_data(image_points_output, sess)
            image_values = np.reshape(image_values, gt_func.imageDims)
            image = image_values * gt_func.imageVol
            base_filename = 'image_';
            output_filename = image_output_dir + base_filename + str(step).zfill(8) + '.jpg'
            save_image(image, output_filename, clip_if_needed=True, range_low_limit=0.0, range_high_limit=1.0)

        # Perform training operation
        data = sess.run([train_op])

    # Save performance during the optimization to file
    ts = np.array(ts)
    train_losses = np.array(train_losses)
    test_losses = np.array(test_losses)
    losses_file = res_dir + 'losses.npz'
    content = dict();
    content['ts'] = ts
    content['train_losses'] = train_losses
    content['test_losses'] = test_losses
    np.savez(losses_file, **content)

    # Save the variables to disk.
    save_path = saver.save(sess, model_dir + "model.ckpt")
    print("Model saved in path: %s" % save_path)