import math
import numpy as np
import tensorflow as tf
slim = tf.contrib.slim
import os
import sys
from utils import *


########################################################################


class FCNNModel:
    """
    """

    def __init__(self, config, data_means, data_stds):
        
        self.config = config
        self.mdlVarsSet = set()
        self.hsize = int(self.config['fc_size']);
        self.layers_num = int(self.config['fc_layer_num']);
        self.activation_fn = eval(self.config['act_func'])
        self.data_means = data_means
        self.data_stds = data_stds
        self.use_shortcuts = (self.config['use_shortcuts'] == 'True')

        
    def get_list_of_model_variables(self):
        return sort_tensors(self.mdlVarsSet)


    def create_model_graph(self,
                           data_tensor,
                           is_training = True,
                           custom_getter=None):

        # Normalize NN input
        data_tensor = (data_tensor - self.data_means) / self.data_stds

        nn_func = self.create_fc_network('reg_net',
                                        data_tensor,
                                        self.hsize,
                                        self.layers_num,
                                        1,
                                        self.activation_fn,
                                        final_activ = False,
                                        custom_getter=custom_getter,
                                        trainable=is_training)


        return nn_func;


    def create_model_graph_with_reparametrization(self,
                           data_tensor,
                           is_training = True):

        # For reparametrization mode, after first model creation join all variables into one vector
        if not hasattr(self, 'theta'):
            self.join_vars()

        # If variables were reparametrized, then construct appropriate model graph
        if hasattr(self, 'theta'):
           custom_getter = self.build_reparam_var_getter()
        else:
            print('There are no reparametrization variables!')

        nn_func = self.create_model_graph(data_tensor, is_training, custom_getter)
        return [nn_func, self.theta]


    def create_fc_network(self, 
                           net_name, 
                           input_tensor, 
                           hsize,
                           layers_num,
                           out_dim, 
                           activation_fn,
                           final_activ = False,
                           custom_getter=None,
                           trainable=True):

        is_first_creation = len(self.mdlVarsSet) == 0

        # Xavier initialization
        weights_initializer = tf.contrib.layers.variance_scaling_initializer(factor=1.0, mode='FAN_AVG', uniform=True)
        biases_initializer = tf.zeros_initializer()

        layer_output = input_tensor
        for i in range(layers_num - 1):
            # Layer i
            space_dim = hsize;
            scope_name = net_name + '/layer' + str(i+1)
            
            v, u, W, b, var_list = fully_connected_layer(layer_output, 
                                               space_dim, 
                                               activation_fn,
                                               custom_getter=custom_getter,
                                               weights_initializer = weights_initializer,
                                               biases_initializer = biases_initializer,
                                               scope=scope_name, 
                                               reuse=tf.AUTO_REUSE, 
                                               trainable=trainable)

            if (is_first_creation):
                self.mdlVarsSet = self.mdlVarsSet.union(set(var_list))

            if (self.use_shortcuts and i > 0):
                v = v + layer_output

            layer_output = v

        # Output for f(X)
        out_layer_dim = out_dim
        out_scope_name = net_name + '/out_layer'
        if (final_activ):
            final_activ_fn = activation_fn
        else:
            final_activ_fn = None
        v, u, W, b, var_list = fully_connected_layer(layer_output, 
                                           out_layer_dim, 
                                           final_activ_fn,
                                           custom_getter=custom_getter,
                                           weights_initializer = weights_initializer,
                                           scope=out_scope_name, 
                                           reuse=tf.AUTO_REUSE, 
                                           trainable=trainable)

        if (is_first_creation):
            self.mdlVarsSet = self.mdlVarsSet.union(set(var_list))

        return v


    def join_vars(self):
        variables = sort_tensors(self.mdlVarsSet)
        self.theta = flatten_into_vector(variables)
        self.param_vars = dict()

        curr_index = 0;
        for v in variables:
            dims = v.get_shape().as_list()
            s = np.prod(dims)
            pv = self.theta[curr_index:curr_index + s]
            pv = tf.reshape(pv, dims)
            self.param_vars[v.name] = pv
            curr_index = curr_index + s

    def build_reparam_var_getter(self):
        def var_getter(getter, name, *args, **kwargs):
            var = self.param_vars[name + ':0']
            return var

        return var_getter

########################################################################


