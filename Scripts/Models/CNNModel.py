import math
import numpy as np
import tensorflow as tf
slim = tf.contrib.slim
import os
import sys
from utils import *


########################################################################


class CNNModel:
    """
    """

    def __init__(self, config):
        
        self.config = config
        self.mdlVarsSet = set()
        self.activation_fn = eval(self.config['act_func'])

        
    def get_list_of_model_variables(self):
        return sort_tensors(self.mdlVarsSet)


    def create_model_graph(self,
                           data_tensor,
                           is_training = True):

       nn_func = self.create_cnn_network2(data_tensor,
                                        self.activation_fn,
                                        trainable=is_training)
         
       return nn_func;


    def create_cnn_network(self,
                           input_tensor,
                           activation_fn,
                           trainable=True):
        channel_output = input_tensor
        weights_initializer = tf.keras.initializers.get('glorot_uniform')
        bias_initializer = tf.zeros_initializer()

        channel_output, var_list = conv2d(
           inputs=channel_output,
           filters=8,
           kernel_size=[2, 2],
           strides=(1, 1),
           padding="same",
           activation=activation_fn,
           reuse=tf.AUTO_REUSE,
           kernel_initializer=weights_initializer,
           bias_initializer=bias_initializer,
           trainable=trainable,
           name='conv1')
        self.mdlVarsSet = self.mdlVarsSet.union(set(var_list))

        channel_output, var_list = conv2d(
           inputs=channel_output,
           filters=16,
           kernel_size=[4, 4],
           strides=(2, 2),
           padding="same",
           activation=activation_fn,
           reuse=tf.AUTO_REUSE,
           kernel_initializer=weights_initializer,
           bias_initializer=bias_initializer,
           trainable=trainable,
           name='conv2')
        self.mdlVarsSet = self.mdlVarsSet.union(set(var_list))

        channel_output, var_list = conv2d(
           inputs=channel_output,
           filters=32,
           kernel_size=[4, 4],
           strides=(2, 2),
           padding="same",
           activation=activation_fn,
           reuse=tf.AUTO_REUSE,
           kernel_initializer=weights_initializer,
           bias_initializer=bias_initializer,
           trainable=trainable,
           name='conv3')
        self.mdlVarsSet = self.mdlVarsSet.union(set(var_list))

        channel_output, var_list = conv2d(
           inputs=channel_output,
           filters=64,
           kernel_size=[4, 4],
           strides=(2, 2),
           padding="same",
           activation=activation_fn,
           reuse=tf.AUTO_REUSE,
           kernel_initializer=weights_initializer,
           bias_initializer=bias_initializer,
           trainable=trainable,
           name='conv4')
        self.mdlVarsSet = self.mdlVarsSet.union(set(var_list))

        channel_output, var_list = conv2d(
           inputs=channel_output,
           filters=1,
           kernel_size=[4, 4],
           strides=(1, 1),
           padding="valid",
           activation=None,
           reuse=tf.AUTO_REUSE,
           kernel_initializer=weights_initializer,
           bias_initializer=bias_initializer,
           trainable=trainable,
           name='conv_last')
        self.mdlVarsSet = self.mdlVarsSet.union(set(var_list))

        channel_output = flatten_data_dims(channel_output)
        return channel_output


    def create_cnn_network2(self,
                           input_tensor,
                           activation_fn,
                           trainable=True):
        channel_output = input_tensor
        weights_initializer = tf.keras.initializers.get('glorot_uniform')
        bias_initializer = tf.zeros_initializer()

        channel_output, var_list = conv2d(
           inputs=channel_output,
           filters=32,
           kernel_size=[3, 3],
           strides=(1, 1),
           padding="valid",
           activation=activation_fn,
           reuse=tf.AUTO_REUSE,
           kernel_initializer=weights_initializer,
           bias_initializer=bias_initializer,
           trainable=trainable,
           name='conv1')
        self.mdlVarsSet = self.mdlVarsSet.union(set(var_list))

        maxpool_layer = tf.keras.layers.MaxPool2D((2, 2))
        channel_output = maxpool_layer.apply(channel_output)

        channel_output, var_list = conv2d(
           inputs=channel_output,
           filters=64,
           kernel_size=[3, 3],
           strides=(1, 1),
           padding="valid",
           activation=activation_fn,
           reuse=tf.AUTO_REUSE,
           kernel_initializer=weights_initializer,
           bias_initializer=bias_initializer,
           trainable=trainable,
           name='conv2')
        self.mdlVarsSet = self.mdlVarsSet.union(set(var_list))

        maxpool_layer = tf.keras.layers.MaxPool2D((2, 2))
        channel_output = maxpool_layer.apply(channel_output)

        channel_output, var_list = conv2d(
           inputs=channel_output,
           filters=64,
           kernel_size=[3, 3],
           strides=(1, 1),
           padding="valid",
           activation=activation_fn,
           reuse=tf.AUTO_REUSE,
           kernel_initializer=weights_initializer,
           bias_initializer=bias_initializer,
           trainable=trainable,
           name='conv3')
        self.mdlVarsSet = self.mdlVarsSet.union(set(var_list))

        channel_output = flatten_data_dims(channel_output)

        channel_output, u, W, b, var_list = fully_connected_layer(channel_output,
                                                     64,
                                                     activation_fn,
                                                     weights_initializer=weights_initializer,
                                                     biases_initializer=bias_initializer,
                                                     scope='fc1',
                                                     reuse=tf.AUTO_REUSE,
                                                     trainable=trainable)
        self.mdlVarsSet = self.mdlVarsSet.union(set(var_list))

        channel_output, u, W, b, var_list = fully_connected_layer(channel_output,
                                           1,
                                           None,
                                           weights_initializer = weights_initializer,
                                           biases_initializer=bias_initializer,
                                           scope='fc2',
                                           reuse=tf.AUTO_REUSE,
                                           trainable=trainable)
        self.mdlVarsSet = self.mdlVarsSet.union(set(var_list))

        return channel_output

########################################################################


