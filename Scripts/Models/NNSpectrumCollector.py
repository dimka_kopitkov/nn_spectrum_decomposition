import math
import numpy as np
import tensorflow as tf
import os
import sys
from utils import *
from H5PyDataCollector import *
import tempfile
import sklearn
from scipy.linalg import eigh


########################################################################


class NNSpectrumCollector:
    """
    """

    def __init__(self, dtype, output_dir, config, train_map):
        
        self.config = config
        self.dtype = dtype
        self.dtype_tf = npdtype_2_tfdtype(self.dtype)
        self.output_dir = output_dir
        self.calc_hessians = (self.config['calc_hessians'] == 'True')

        # Indeces at which to collect the NN spectrum
        end_step = int(float(config['it_num']))
        time_period = 5000
        vec_i = np.arange(0, end_step, time_period).tolist()
        vec_i = vec_i + np.arange(1, 11).tolist()
        vec_i = vec_i + np.arange(100, 1000, 100).tolist()
        vec_i = vec_i + np.arange(1000, 10000, 1000).tolist()
        vec_i = vec_i + np.arange(100, 105).tolist()
        vec_i = vec_i + np.arange(500, 505).tolist()
        vec_i = vec_i + np.arange(20000, 20005).tolist()
        vec_i = vec_i + np.arange(50000, 50005).tolist()
        vec_i = vec_i + np.arange(100000, 100010).tolist()
        vec_i = vec_i + np.arange(150000, 150010).tolist()
        vec_i = vec_i + [55000]
        vec_i = vec_i + [595000]
        vec_i = list(set(vec_i))
        vec_i = sorted(vec_i)
        vec_i2 = (np.array(vec_i) + 1).tolist()
        vec_i = vec_i + vec_i2
        vec_i = list(set(vec_i))
        vec_i = sorted(vec_i)

        self.collect_steps = vec_i
        self.collect_test_steps = [595000]

        self.points = train_map['points']
        self.labels = train_map['labels']
        self.test_points = train_map['test_points']
        self.test_labels = train_map['test_labels']
        self.points_num = self.points.shape[0]
        self.data_shape = list(self.points.shape[1:])
        points_dist = sklearn.metrics.pairwise.euclidean_distances(np.reshape(self.points, [self.points_num, -1]))

        # Save points for further analysis
        base_filename = 'nn_spectrum_info'
        output_filename = self.output_dir + base_filename + '.npz'
        content = dict();
        content['points'] = self.points
        content['labels'] = self.labels
        content['dist'] = points_dist
        np.savez(output_filename, **content)

        temp_dir = self.config['temp_dir']
        if (temp_dir == 'default'):
            temp_dir = tempfile.gettempdir()
        self.temp_dir = temp_dir


    def create_tensors(self, nn_model, lr_tensor):

        self.nn_model = nn_model
        self.lr_tensor = lr_tensor
        model_vars = self.nn_model.get_list_of_model_variables()

        with tf.device('/gpu:0'):
            self.X = tf.placeholder(self.dtype_tf, shape = [1] + self.data_shape)

            if (self.calc_hessians):
                [self.f_X, self.theta_tensor] = self.nn_model.create_model_graph_with_reparametrization(self.X)
                self.grad_tensor = tf.gradients(self.f_X[0], self.theta_tensor)[0]
                self.hessian_tensor = tf.hessians(self.f_X[0], self.theta_tensor)[0]
            else:
                self.f_X = self.nn_model.create_model_graph(self.X)
                grad = tf.gradients(self.f_X[0], model_vars)
                self.grad_tensor = flatten_into_vector(grad)
                self.theta_tensor = flatten_into_vector(model_vars)

            self.g_size = self.grad_tensor.shape[0].value


    def update_before_iteration_start(self, sess, global_step):
        # Compute Gramian for train points
        if (global_step in self.collect_steps):
            try:
                self.collect_nn_spectrum(sess, global_step, train_points = True)
            except Exception as e:
                print(e)

        # Compute Gramian for test points
        if (global_step in self.collect_test_steps):
            try:
                self.collect_nn_spectrum(sess, global_step, train_points = False)
            except Exception as e:
                print(e)


    def collect_nn_spectrum(self, sess, global_step, train_points):
        if (train_points):
            base_filename = 'nn_cov_'
        else:
            base_filename = 'test_nn_cov_'

        output_filename = self.output_dir + base_filename + str(global_step).zfill(8) + '.npz'
        content = dict();

        [grads_data_bank,
         hessians_data_bank,
         f,
         theta,
         lrate] = self.compute_nn_gradients(sess, train_points)
        grads_data_bank.open_data_bank_in_read_mode();
        gs = grads_data_bank.get_data_obj()

        if (self.calc_hessians):
            hessians_data_bank.open_data_bank_in_read_mode();
            hessians = hessians_data_bank.get_data_obj()
            A = np.array(gs).T
            hessians = np.array(hessians)
            hessians_data_bank.close_and_delete_bank()
            del hessians_data_bank
            content['A'] = A
            content['hessians'] = hessians
        content['theta'] = theta

        # Compute gradient similarity
        cov_dc = H5PyDataCollector(self.temp_dir, [self.points_num, self.points_num])
        entire_cov_obj = cov_dc.get_data_obj()
        calc_dot_product_in_chunks(gs, entire_cov_obj, chunk_size = 1000)
        grads_data_bank.close_and_delete_bank()
        del grads_data_bank
        cov_dc.close_data_bank();
        cov = cov_dc.read_data_and_delete_bank();
        del cov_dc

        content['cov'] = cov
        content['f'] = f
        content['lrate'] = lrate
        #content['max_eigval'] = max_eigval

        np.savez(output_filename, **content)

    
    def compute_nn_gradients(self, sess, train_points):
        dc_grad = H5PyDataCollector(self.temp_dir, [self.points_num, self.g_size])
        if (self.calc_hessians):
            dc_hessian = H5PyDataCollector(self.temp_dir, [self.points_num, self.g_size, self.g_size])
        else:
            dc_hessian = None
        fs = []

        if (train_points):
            ps = self.points
        else:
            ps = self.test_points

        tensors_to_calc = [self.grad_tensor, self.f_X]
        if (self.calc_hessians):
            tensors_to_calc.append(self.hessian_tensor)

        for i in range(self.points_num):
            data = sess.run(tensors_to_calc,
                            feed_dict={self.X: ps[i:i+1,...]})
            curr_grad = data[0][np.newaxis,:]
            curr_f = data[1]
            fs.append(curr_f)
            dc_grad.append_data(curr_grad);

            if (self.calc_hessians):
                curr_hessian = data[2][np.newaxis, ...]
                dc_hessian.append_data(curr_hessian);

            msg = "\r- Computed gradients: {0} / {1}".format(dc_grad.get_row_num(), self.points_num)
            sys.stdout.write(msg)
            sys.stdout.flush()
        
        dc_grad.close_data_bank();
        f_vec = np.squeeze(np.concatenate(fs))
        if (self.calc_hessians):
            dc_hessian.close_data_bank();

        [curr_theta, curr_lrate] = sess.run([self.theta_tensor, self.lr_tensor])

        return [dc_grad, dc_hessian, f_vec, curr_theta, curr_lrate]

                        
########################################################################
