import argparse
import numpy as np
import sys
import os
sys.path.insert(0, './Utils/')
from scipy.sparse.linalg import eigsh
from scipy.linalg import eigh
from os import listdir
from os.path import isfile, join
from utils import *
from scipy.linalg import svdvals
import scipy as sp


# Load configuration
config = read_config('./train.properties')
res_dir = config['res_dir']
opt_task = config['opt_task']
files_dir = res_dir + '/nn_stats/'

output_path = res_dir + '/nn_eigs_dec/'
ensure_path_exists(output_path)

info_file = np.load(files_dir + '/nn_spectrum_info.npz')
gt = info_file['labels']
gt = gt.ravel()
if (opt_task == 'classification'):
    gt = gt >= 5
    zero_indeces = np.where(np.logical_not(gt))[0]
    gt_labels = gt.astype(np.float32)
    gt = np.ones_like(gt_labels)
    gt[zero_indeces] = -1.0
    gt = gt*1000

basename = 'nn_cov_'
onlyfiles = [f for f in listdir(files_dir) if isfile(join(files_dir, f))]
onlyfiles = sorted(onlyfiles)

ts = []
fs = []
lrates = []
gt_prjs = []
f_prjs = []
Ms = []
M_prjs = []
GMs = []
GM_prjs = []
gt_rel_energies = []
f_rel_energies = []
all_vals = []
all_d_entries = []
all_inv_diag_entries = []
M_0 = None
M0_prjs = []

for i, f_name in enumerate(onlyfiles):
    if f_name.startswith(basename) and f_name.endswith(".npz"):
        curr_filename = os.path.splitext(f_name)[0]
        p1 = curr_filename.split('_')[2]
        p1 = int(p1)
        ts.append(p1)

        dt_file = np.load(join(files_dir, f_name))
        f = dt_file['f']
        fs.append(f)

        if (opt_task == 'regression'):
            curr_M = f - gt
        else:
            curr_M_pos = 1.0/(1.0 + np.exp(-f)) - gt_labels
            f_exp = np.exp(f)
            curr_M_neg = f_exp / (1.0 + f_exp) - gt_labels
            curr_M_pos[f < 0.0] = 0.0
            curr_M_neg[f >= 0.0] = 0.0
            curr_M = curr_M_pos + curr_M_neg
            #curr_M = -1.0/(np.exp(f) + 1.0)
            #curr_M[zero_indeces] = 1.0 + curr_M[zero_indeces]
        Ms.append(curr_M)
        if (len(Ms) == 1):
            M_0 = curr_M

        lrate = dt_file['lrate']
        lrates.append(lrate)

        cov = dt_file['cov'].astype(np.float128)
        # hessians = dt_file['hessians']
        # hessians = np.reshape(hessians, [hessians.shape[0], -1])
        # cov = np.dot(hessians, hessians.T)
        # del hessians
        # cov = cov.astype(np.float128)

        dim = cov.shape[0]
        #[eigenvals, eigenvecs] = eigsh(cov, k=10, which='SM')
        [eigenvals, eigenvecs] = eigh(cov)
        d_entries = np.diag(cov)
        indeces = np.argsort(eigenvals)
        indeces = np.flip(indeces)
        eigenvals = eigenvals[indeces]
        eigenvecs = (eigenvecs.T)[indeces,:]
        sinvals = svdvals(cov)
        indeces = np.argsort(sinvals)
        indeces = np.flip(indeces, axis = 0)
        sinvals = sinvals[indeces]
        inv_cov , info = sp.linalg.lapack.dpotri(cov)
        inv_diag_entries = np.diag(inv_cov)
        #inv_diag_entries = np.expand_dims(inv_diag_entries, axis = -1)

        # Compute projections and energy
        gt_projections = np.matmul(eigenvecs, gt)
        gt_prjs.append(gt_projections)
        f_projections = np.matmul(eigenvecs, f)
        f_prjs.append(f_projections)

        M_projections = np.matmul(eigenvecs, curr_M)
        M_prjs.append(M_projections)
        curr_GM = np.matmul(cov, curr_M).astype(np.float64)
        GMs.append(curr_GM)
        GM_projections = np.matmul(eigenvecs, curr_GM)
        GM_prjs.append(GM_projections)
        M0_projections = np.matmul(eigenvecs, M_0)
        M0_prjs.append(M0_projections)

        curr_rel_acc_energy = compute_rel_energy(gt, gt_projections)
        gt_rel_energies.append(curr_rel_acc_energy)
        curr_f_rel_acc_energy = compute_rel_energy(f, f_projections)
        f_rel_energies.append(curr_f_rel_acc_energy)

        # Compress numeric precision
        eigenvals = eigenvals.astype(np.float64)
        eigenvecs = eigenvecs.astype(np.float32)
        d_entries = d_entries.astype(np.float64)
        sinvals = sinvals.astype(np.float64)
        inv_diag_entries = inv_diag_entries.astype(np.float64)

        # Apply simple cutoff of neglectable eigenvalues
        eig_threshold = np.max(eigenvals)*1e-8
        eigenvals[eigenvals < eig_threshold] = 0.0

        # Save eigendecomposition to file
        res_file = join(output_path, f_name)
        content = dict();
        content['eigenvals'] = eigenvals
        content['eigenvecs'] = eigenvecs
        content['d_entries'] = d_entries
        content['sinvals'] = sinvals
        content['inv_diag_entries'] = inv_diag_entries
        np.savez(res_file, **content)

        all_vals.append(eigenvals)
        all_d_entries.append(d_entries)
        all_inv_diag_entries.append(inv_diag_entries)

        print(curr_filename)

fs = np.stack(fs)
ts = np.array(ts)
lrates = np.array(lrates)
Ms = np.stack(Ms)
Ms_norm = np.linalg.norm(Ms, axis = 1)
gt_prjs = np.stack(gt_prjs)
f_prjs = np.stack(f_prjs)
M_prjs = np.stack(M_prjs)
GMs = np.stack(GMs)
GM_prjs = np.stack(GM_prjs)
M0_prjs = np.stack(M0_prjs)
gt_rel_energies = np.stack(gt_rel_energies)
f_rel_energies = np.stack(f_rel_energies)
all_vals = np.stack(all_vals)
all_d_entries = np.stack(all_d_entries)
all_inv_diag_entries = np.stack(all_inv_diag_entries)

res_file = join(output_path, 'summery.npz')
content = dict();
content['fs'] = fs
content['ts'] = ts
content['lrates'] = lrates
content['Ms'] = Ms
content['Ms_norm'] = Ms_norm
content['gt'] = gt
content['gt_prjs'] = gt_prjs
content['f_prjs'] = f_prjs
content['M_prjs'] = M_prjs
content['GMs'] = GMs
content['GM_prjs'] = GM_prjs
content['M0_prjs'] = M0_prjs
content['gt_rel_energies'] = gt_rel_energies
content['f_rel_energies'] = f_rel_energies
content['all_vals'] = all_vals
content['all_d_entries'] = all_d_entries
content['all_inv_diag_entries'] = all_inv_diag_entries
np.savez(res_file, **content)

    
print('end')


