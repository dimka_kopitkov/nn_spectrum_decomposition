import sys
import numpy as np
import tensorflow as tf
sys.path.insert(0, './Models/')
sys.path.insert(0, './Utils/')
from utils import *
from image_utils import *
from ImageFnctn import *
from GeneratorDataset import *
from NumpyGenerator import *
from PDFSamplesGenerator import *
from FCNNModel import *
from NNSpectrumCollector import *
from tensorflow.keras import datasets


# Read run configuration
config = read_config('./train.properties')
res_dir = config['res_dir']
ensure_path_exists(res_dir)
model_dir = res_dir + '/trained_model/'
ensure_path_exists(model_dir)
temp_dir = config['temp_dir']
ensure_path_exists(temp_dir)
image_output_dir = res_dir + '/images/'
ensure_path_exists(image_output_dir)
it_num = int(float(config['it_num']))
dtype = np.float32
dtype_tf = npdtype_2_tfdtype(dtype)
lrate = float(config['lrate'])

# Prepare dataset
#dataset_size = int(float(config['dataset_size']))
train_dataset_size = int(float(1e5))
batch_size = 1000
test_dataset_size = int(float(1e4))
train_map = dict()
gt_func = ImageFnctn('./lisa_blurred.jpg')

# Sample from pdf function that is proportional to image landscape
def sample_points(N):
    M = 4600.0
    gen = PDFSamplesGenerator(gt_func.dim, gt_func.func_output, batch_size = N, sample_size = 15000, M = M)

    gen.wait_till_samples_generated(N)
    samples_iterator = gen()
    samples_batch = next(samples_iterator)
    samples = samples_batch['data']
    pdf_values = samples_batch['pdf_values']

    return samples, np.log(pdf_values)

# Construct train and test datasets
[train_map['points'], train_map['labels']] = sample_points(train_dataset_size)
[train_map['test_points'], train_map['test_labels']] = sample_points(test_dataset_size)
train_map['noise_points'] = np.random.uniform(size=[train_dataset_size, 2])
train_map['test_noise_points'] = np.random.uniform(size=[test_dataset_size, 2])
train_map['means'] = np.mean(train_map['points'], axis = 0)
train_map['stds'] = np.std(train_map['points'], axis = 0)
generated_dataset_file = res_dir + 'generated_dataset.npz'
np.savez(generated_dataset_file, **train_map)

# Construct train tensors
dtype_map = dict()
dtype_map['points'] = dtype;
dtype_map['noise_points'] = dtype;

gen = NumpyGenerator({'points': train_map['points'], 'noise_points': train_map['noise_points']},
                     dtype_map=dtype_map,
                     batch_size=batch_size,
                     is_infinite_loop_mode=True);
dt_generator = GeneratorDataset(gen)
dt_iterator = dt_generator.get_dt_object().make_initializable_iterator()
train_data_element = dt_iterator.get_next()
train_points_tensor = train_data_element['points']
train_noise_tensor = train_data_element['noise_points']

# Construct test tensors
test_points_tensor = tf.convert_to_tensor(train_map['test_points'], dtype=dtype_tf)
test_noise_tensor = tf.convert_to_tensor(train_map['test_noise_points'], dtype=dtype_tf)


def construct_loss(data_tensor, noise_tensor, model):
    data_nn_output = model.create_model_graph(data_tensor)
    noise_nn_output = model.create_model_graph(noise_tensor)

    # Since noise is Uniform over [0, 1]^2, its pdf is constant 1
    up_term = tf.log(1.0 + tf.exp(- data_nn_output))
    down_term = tf.log(1.0 + tf.exp(noise_nn_output))
    nce_loss = tf.reduce_mean(up_term + down_term)

    return nce_loss


def construct_lr_tensor(init_lrate, global_step):
    lr_tensor = tf.train.exponential_decay(
                                        learning_rate = init_lrate,
                                        global_step = global_step,
                                        decay_steps = 100000,
                                        decay_rate = 0.5,
                                        staircase = True)

    return lr_tensor


# Create model on gpu for better performance
with tf.device(config['device']):
    model = FCNNModel(config, train_map['means'], train_map['stds'])

    # Create train and test losses
    train_nce_loss = construct_loss(train_points_tensor, train_noise_tensor, model)
    test_nce_loss = construct_loss(test_points_tensor, test_noise_tensor, model)

    # Define learning rate
    global_step = tf.train.get_or_create_global_step()
    lr_tensor = construct_lr_tensor(lrate, global_step)

    # Define an optimizer
    optimizer = tf.train.GradientDescentOptimizer(learning_rate=lr_tensor)
    total_model_vars = model.get_list_of_model_variables()
    print_dim_of_variables(total_model_vars)
    train_op = optimizer.minimize(train_nce_loss, var_list = total_model_vars, global_step = global_step)

    # Tensors for drawing NN output as image
    image_points = gt_func.imagePoints
    image_points = cartesian_product(image_points)
    image_points_tensor = tf.convert_to_tensor(image_points, dtype=dtype_tf)
    with tf.device('/cpu:0'):
        dt = tf.data.Dataset.from_tensor_slices({'data': image_points_tensor})
        batch_dt = dt.batch(10000)
        iterator = batch_dt.make_initializable_iterator()
    next_element = iterator.get_next()
    image_points_output = model.create_model_graph(next_element['data'])
    # NCE target log pdf of data
    # Now we recover the pdf itself, which is also the approximation of image landscape
    image_points_output = tf.exp(image_points_output)

    # Create spectrum collector
    files_output_dir = res_dir + '/nn_stats/'
    ensure_path_exists(files_output_dir)
    # For NCE, we will calculate G on testing points, since training dataset is too big
    spec_map = dict()
    spec_map['points'] = train_map['test_points']
    spec_map['labels'] = train_map['test_labels']
    spec_map['test_points'] = train_map['test_points']
    spec_map['test_labels'] = train_map['test_labels']
    spec_collector = NNSpectrumCollector(dtype = dtype, output_dir = files_output_dir, config = config, train_map = spec_map)
    spec_collector.create_tensors(model, lr_tensor)



saver = tf.train.Saver(total_model_vars)

with tf.Session() as sess:
    # Intialize the model
    sess.run(tf.global_variables_initializer())
    sess.run(dt_iterator.initializer)
        
    # Optimize NN
    ts = []
    train_losses = []
    test_losses = []
    for step in range(it_num):

        # Collect NN spectrum
        spec_collector.update_before_iteration_start(sess, step);

        # Compute current performance
        if (step % 100 == 0) or (step == it_num - 1):
            data = sess.run([train_nce_loss, test_nce_loss])
            curr_train_loss = data[0]
            curr_test_loss = data[1]
            ts.append(step)
            train_losses.append(curr_train_loss)
            test_losses.append(curr_test_loss)

            msg = "\r- Iteration: {0:>6}, Train NCE Loss is: {1:<13}, Test NCE Loss is: {2:<13}".format(step, curr_train_loss, curr_test_loss)
            sys.stdout.write(msg)
            sys.stdout.flush()

        # Draw NN output as image
        if (step % 5000 == 0):
            sess.run(iterator.initializer)
            image_values = fetch_all_tensor_data(image_points_output, sess)
            image_values = np.reshape(image_values, gt_func.imageDims)
            image = image_values * gt_func.imageVol
            base_filename = 'image_';
            output_filename = image_output_dir + base_filename + str(step).zfill(8) + '.jpg'
            save_image(image, output_filename, clip_if_needed=True, range_low_limit=0.0, range_high_limit=1.0)

        # Perform training operation
        data = sess.run([train_op])

    # Save performance during the optimization to file
    ts = np.array(ts)
    train_losses = np.array(train_losses)
    test_losses = np.array(test_losses)
    losses_file = res_dir + 'losses.npz'
    content = dict();
    content['ts'] = ts
    content['train_losses'] = train_losses
    content['test_losses'] = test_losses
    np.savez(losses_file, **content)

    # Save the variables to disk.
    save_path = saver.save(sess, model_dir + "model.ckpt")
    print("Model saved in path: %s" % save_path)