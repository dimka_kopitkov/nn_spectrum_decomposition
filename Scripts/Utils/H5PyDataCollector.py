import numpy as np
import time
import sys
import datetime
import uuid
import tables
import os
import h5py


########################################################################


class H5PyDataCollector:
    """
    """

    def __init__(self, temp_dir, shape):
        # Calculate data
        filename = 'temp_big_data_collector_' + uuid.uuid4().hex
        self.data_path = temp_dir + '/' + filename
        self.shape = shape
        self.f = h5py.File(self.data_path, 'w')
        self.dset = self.f.create_dataset("data", self.shape)
        self.curr_index = 0
        

    def append_data(self, vec):
        self.dset[self.curr_index,:] = vec
        self.curr_index += 1


    def close_data_bank(self):
        self.f.close()


    def open_data_bank_in_read_mode(self):
        self.f = h5py.File(self.data_path, 'r')
        self.dset = self.f['data']


    def get_data_obj(self):
        return self.dset


    def close_and_delete_bank(self):
        self.f.close()
        os.remove(self.data_path);


    def read_data_and_delete_bank(self):
        self.open_data_bank_in_read_mode()
        data = self.dset[:,:]
        self.close_and_delete_bank()
        return data


    def get_row_num(self):
        return self.curr_index

        
########################################################################


