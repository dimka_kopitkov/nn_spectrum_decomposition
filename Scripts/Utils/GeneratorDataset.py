import numpy as np
import tensorflow as tf
import sys


########################################################################


class GeneratorDataset:
    """
    """

    def __init__(self, generator, do_repeat = False):
        # Build dataset from generator
        batch_Dt = tf.data.Dataset.from_generator(generator, generator.types, generator.shapes)
        
        if (do_repeat):
            batch_Dt = batch_Dt.repeat();
        
        # Give name to input tensor
        #batch_Dt = batch_Dt.map(lambda x: {'data' : x})

        # Prefetch samples
        self.batch_Dt = batch_Dt.prefetch(100);


    def get_dt_object(self):
        return self.batch_Dt


########################################################################


