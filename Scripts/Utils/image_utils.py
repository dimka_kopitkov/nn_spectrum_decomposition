import numpy as np
import math
import sys
import cv2
from PIL import Image
from sklearn.preprocessing import MinMaxScaler


def blur_images(x, sigma = 0.3, ksize = (3,3)):
    
    images = []
    for i in range(x.shape[0]):
        curr_im = x[i]
        curr_im = cv2.GaussianBlur(curr_im,ksize,sigma)
        #curr_im = cv2.blur(curr_im,(3,3))
        #curr_im = cv2.medianBlur(curr_im,3)
        images.append(curr_im)
    x = np.stack(images)

    return x


def save_image(img, file_path, clip_if_needed = True, range_low_limit = -1.0, range_high_limit = 1.0, to_scale = False):
    img = np.copy(img)
    if (to_scale):
        scaler = MinMaxScaler(feature_range=(range_low_limit, range_high_limit))
        
    if (clip_if_needed):
        if (np.any(np.logical_or(img > range_high_limit, img < range_low_limit))):
            print('\n Parameter image has invalid range! \n')
            print(np.min(img))
            print(np.max(img))
            
            if (to_scale):
                print('Rescaling it!')
                #scaler.fit(img)
                #img = scaler.transform(img)
                img = img/np.abs(np.max(img))
            else:
                print('Clipping it!')
                img = np.clip(img, range_low_limit, range_high_limit)
                #img = img/np.abs(np.max(img))
    
    range_length = range_high_limit - range_low_limit
    img -= range_low_limit
    img *= (255.0/range_length)
    #img += 1.
    #img *= 127.5
    img = img.astype(np.uint8)
    if (img.ndim > 2 and img.shape[2] == 3):
        img = img[:,:,[2,1,0]]
    img = Image.fromarray(img)
    img.save(file_path)
    
    
def resize_images(x, shape = [10, 10]):
    images = []
    for i in range(x.shape[0]):
        curr_im = x[i]
        curr_im = cv2.resize(curr_im, tuple(shape), interpolation=cv2.INTER_LINEAR) 
        images.append(curr_im)
    x = np.stack(images)
    return x


def normalize_image_norm(x, norm = 3.0):
    orig_shape = x.shape
    x = np.reshape(x, [x.shape[0], -1])
    x = (x/np.linalg.norm(x, axis = 1)[:,np.newaxis])*norm
    x = np.reshape(x, orig_shape)
    return x


def rotate_images(x, angle = 90):
    rows = x.shape[1]
    cols = x.shape[2]
    M = cv2.getRotationMatrix2D((cols/2,rows/2),angle,1)
    images = []
    for i in range(x.shape[0]):
        curr_im = x[i]
        curr_im = cv2.warpAffine(curr_im,M,(cols,rows))
        images.append(curr_im)
    x = np.stack(images)
    return x


def read_float_image(file_name):
    im = cv2.imread(file_name)
    im = im.astype(np.float32)/255.0
    #floatImage = img_as_float(im)
    return im
