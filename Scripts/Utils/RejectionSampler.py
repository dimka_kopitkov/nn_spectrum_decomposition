import numpy as np
import tensorflow as tf
import sys
import math
from utils import *
from scipy.stats import norm


########################################################################


class RejectionSampler:
    """
    """

    def __init__(self):
        # Just to fill line (python stupidity)
        self.name = ''


    def sample_pdf(self, pdf_func, dim, M, samples_min_num):
        reject_sample_num = int(math.ceil(samples_min_num*M))
        
        total_points = None
        total_pdf_values = None
        sampled_num = 0

        while (sampled_num < samples_min_num):
            Y, Y_p = self.sample_known_distribution(dim, reject_sample_num)
            T_p = pdf_func(Y)
                    
            Y_p_scaled = M*Y_p
            ratio = T_p/Y_p_scaled
            ratio = ratio.flatten()
            if (np.any(ratio > 1)):
                print('Warning M is too small!!! Use at least M:')
                print(np.max(ratio)*M)
                exit()
    
            U = np.random.uniform(size=[reject_sample_num])
            valid_indeces = np.where(U < ratio)
            valid_indeces = valid_indeces[0]
            points = Y[valid_indeces,:]
            pdf_values = T_p[valid_indeces,:]
            
            if (total_points is None):
                total_points = points
                total_pdf_values = pdf_values
            else:
                total_points = np.concatenate((total_points, points), axis = 0)
                total_pdf_values = np.concatenate((total_pdf_values, pdf_values), axis = 0)
            sampled_num = total_points.shape[0]

        return total_points, total_pdf_values
        
        
    def sample_known_distribution(self, dim, samples_num):
        max_dist = 6.0;
        points = np.random.uniform(low=-max_dist, high=max_dist, size=[samples_num, dim])
        points_p = np.ones(points.shape, dtype = np.float32)
        points_p = points_p * (1/(2*max_dist))
        points_p = np.prod(points_p, axis=1, keepdims=True)
        
        return points, points_p

########################################################################


