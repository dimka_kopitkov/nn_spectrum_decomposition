import numpy as np
import sys
import tensorflow as tf
from Dense import *
from configparser import SafeConfigParser
from configparser import ConfigParser
import six
from scipy.interpolate import griddata
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import shutil
import cv2
import string


def cartesian_product(arrays):
    la = len(arrays)
    dtype = np.result_type(*arrays)
    arr = np.empty([len(a) for a in arrays] + [la], dtype=dtype)
    for i, a in enumerate(np.ix_(*arrays)):
        arr[...,i] = a
    return arr.reshape(-1, la)


def tfdtype_2_npdtype(dtype):
    if (dtype is None):
        return None
    else:
        return dtype.as_numpy_dtype;


def npdtype_2_tfdtype(dtype):
    if (dtype is None):
        return None
    else:
        return tf.as_dtype(dtype);
    
    
def sort_tensors(tensors):
    def getKey(tnsr):
        n = tnsr.name
        n = n.replace('bias', '2222')
        n = n.replace('kernel', '1111')
        return n
    sorted_tensr_list = sorted(list(tensors), key=getKey)

    return sorted_tensr_list


def fully_connected_layer(inputs,
                        num_outputs,
                        activation_fn,
                        custom_getter = None,
                        weights_initializer=tf.contrib.layers.xavier_initializer(),
                        weights_regularizer=None,
                        weight_norm = False,
                        biases_initializer=tf.zeros_initializer(),
                        biases_regularizer=None,
                        reuse=None,
                        trainable=True,
                        scope=None,
                        use_layer_norm=False,
                        use_batch_norm=False):
    with tf.variable_scope(
        scope,
        'fully_connected', [inputs],
        reuse=reuse,
        custom_getter=custom_getter) as sc:
        inputs = tf.convert_to_tensor(inputs)
        dtype = inputs.dtype.base_dtype
        inp_dim = inputs.shape[-1].value
            
        use_bias = not use_batch_norm and not use_layer_norm and biases_initializer
        layer = Dense(num_outputs,
                      activation=None,
                      weight_norm=weight_norm,
                      use_bias=use_bias,
                      kernel_initializer=weights_initializer,
                      bias_initializer=biases_initializer,
                      kernel_regularizer=weights_regularizer,
                      bias_regularizer=biases_regularizer,
                      activity_regularizer=None,
                      kernel_constraint=None,
                      bias_constraint=None,
                      trainable=trainable,
                      dtype=dtype,
                      _scope=sc,
                      _reuse=reuse)
        u = layer.apply(inputs)
        W = layer.kernel
        b = layer.bias
        var_list = layer.myVars

        # Apply normalizer function / layer.
        if use_layer_norm:
            vars_col = sc.name + '/layer_norm'
            u = tf.contrib.layers.layer_norm(u, reuse=reuse, trainable=trainable, scope=sc.name, variables_collections=[vars_col])
            ln_vars = tf.get_collection(vars_col)
            graph = tf.get_default_graph()
            graph.clear_collection(vars_col)
            var_list = var_list + ln_vars
        elif use_batch_norm:
            bn_layer = tf.layers.BatchNormalization(axis=-1,
                                                    dtype=dtype,
                                                    _reuse=reuse,
                                                    _scope=sc)
            u = bn_layer.apply(u, training=True)
            gamma = bn_layer.gamma
            beta = bn_layer.beta
            var_list = var_list + [gamma, beta]
  

        if activation_fn is not None:
            v = activation_fn(u)
        else:
            v = u
      
    return v, u, W, b, var_list


def conv2d(inputs,
           filters,
           kernel_size,
           strides=(1, 1),
           padding='valid',
           data_format='channels_last',
           dilation_rate=(1, 1),
           activation=None,
           use_bias=True,
           kernel_initializer=None,
           bias_initializer=tf.zeros_initializer(),
           kernel_regularizer=None,
           bias_regularizer=None,
           activity_regularizer=None,
           kernel_constraint=None,
           bias_constraint=None,
           trainable=True,
           name=None,
           reuse=None):
    layer = tf.layers.Conv2D(
        filters=filters,
        kernel_size=kernel_size,
        strides=strides,
        padding=padding,
        data_format=data_format,
        dilation_rate=dilation_rate,
        activation=activation,
        use_bias=use_bias,
        kernel_initializer=kernel_initializer,
        bias_initializer=bias_initializer,
        kernel_regularizer=kernel_regularizer,
        bias_regularizer=bias_regularizer,
        activity_regularizer=activity_regularizer,
        kernel_constraint=kernel_constraint,
        bias_constraint=bias_constraint,
        trainable=trainable,
        name=name,
        _reuse=reuse,
        _scope=name)

    out = layer.apply(inputs)
    return out, layer.trainable_variables


def flatten_data_dims(x):
    shape = x.get_shape().as_list()

    if (len(shape) <= 2):
        return x

    dim = np.prod(shape[1:])
    y = tf.reshape(x, [-1, dim])
    return y


def ensure_path_exists(path):
    if not os.path.exists(path):
        os.makedirs(path)


def remove_if_path_exists(path):
    if os.path.exists(path):
        shutil.rmtree(path)


def is_python_3():
    version3 = (3, 0)
    cur_version = sys.version_info
    if (cur_version >= version3):
        return True
    else:
        return False


def read_config(fname):
    if (isinstance(fname, (list,))):
        fn_list = fname
    else:
        fn_list = [fname]

    if is_python_3():
        from io import StringIO
    else:
        from StringIO import StringIO

    file_contents = [];
    for fn in fn_list:
        content = open(fn, 'r').read()
        file_contents.append(content)

    unified_content = '\n'.join(file_contents)

    ini_str = '[root]\n' + unified_content
    ini_fp = StringIO(ini_str)
    env = os.environ
    env = {k: v for (k, v) in env.items() if '%' not in k and '%' not in v}
    parser = SafeConfigParser(env)
    parser.readfp(ini_fp)
    ini_fp.close()
    config = dict(parser['root'])
    # config = {k.encode('ascii','ignore'):v.encode('ascii','ignore') for (k, v) in config.items()}
    return config


def write_config(fname, config):
    parser = ConfigParser()

    parser.add_section('root')
    for key in config.keys():
        parser.set('root', key, config[key])

    if is_python_3():
        from io import StringIO
    else:
        from StringIO import StringIO

    output = StringIO()
    parser.write(output)
    config_uni = output.getvalue()
    output.close();
    # config_str = config_uni.encode('utf8')

    config_str = config_uni
    config_str = config_str.split("\n", 1)
    config_str = config_str[-1]

    f = open(fname, 'w')
    f.write(config_str)
    f.close()


def flatten_into_vector(tensor_list):
    flatten_tnsrs = [tf.reshape(t, [-1]) for t in tensor_list]
    res = tf.concat(flatten_tnsrs, 0)
    return res


def is_between(x, a, b):
    return tf.logical_and(tf.less_equal(a, x), tf.less(x, b))


def calc_dot_product_in_chunks(gs, cov_to_return, chunk_size=1000):
    dim = gs.shape[0]
    chunk_starts = np.arange(0, dim, chunk_size, dtype=np.int32)
    chunk_ends = chunk_starts + chunk_size
    if (chunk_ends[-1] > dim):
        chunk_ends[-1] = dim
    chunk_num = chunk_starts.shape[0]

    def save_cov_block(cov, i, j):
        cov_to_return[chunk_starts[i]:chunk_ends[i], chunk_starts[j]:chunk_ends[j]] = cov
        cov_to_return[chunk_starts[j]:chunk_ends[j], chunk_starts[i]:chunk_ends[i]] = cov.T

    print()
    for i in range(chunk_num):
        i_gs = gs[chunk_starts[i]:chunk_ends[i], :]
        for j in range(i, chunk_num):
            j_gs = gs[chunk_starts[j]:chunk_ends[j], :]

            # points_dist = sklearn.metrics.pairwise.euclidean_distances(i_gs, j_gs)
            # ij_cov = scipy.stats.norm(0, 1).pdf(points_dist)
            ij_cov = np.dot(i_gs, j_gs.T)
            # ij_cov = np.matmul(i_gs, j_gs.T, dtype = np.float64);
            del j_gs
            save_cov_block(ij_cov, i, j)
            del ij_cov

            msg = "\r- Saved: i {0}, j {1}".format(i, j)
            sys.stdout.write(msg)
            sys.stdout.flush()

        del i_gs


def get_path_of_tfvariable(var):
    v_name = var.name.split(':')[0]
    return v_name


def print_dim_of_variables(vars):
    theta_size = 0
    for v in vars:
        print(get_path_of_tfvariable(v))
        dims = v.shape.dims
        dims = [d.value for d in dims]
        print('Dims: ' + str(dims))
        size = np.prod(dims)
        print('Size: ' + str(size))
        theta_size += size
    print('Theta Size: ' + str(theta_size))


def interpolate_surf_image(f, points, grid_X1, grid_X2):
    # interpolate
    zi = griddata(points,f.flatten(),(grid_X1,grid_X2),fill_value=0,method='linear')
    zi = zi.T;
    return zi


def set_ax_font_size(ax, font_size):
    for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
                 ax.get_xticklabels() + ax.get_yticklabels()):
        item.set_fontsize(font_size)


def draw_image_and_save(mtr, fn, draw_colorbar = True, draw_point = None, cm = 'viridis', title = None, font_size = None, fig_size = None, dpi = 200):
    if (draw_colorbar):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        if (font_size is not None):
            set_ax_font_size(ax, font_size)
        if (title is not None):
            ax.set_title(title, fontsize = font_size)
        plt.axis('off')
        plt.set_cmap(cm)
        im = plt.imshow(mtr)
        if (draw_point is not None):
            plt.plot(draw_point[1]*float(mtr.shape[1]), draw_point[0]*float(mtr.shape[0]), color='red', marker='+', linewidth=2, markersize=10)
        # im = ax.imshow(mtr)
        # fig.colorbar(cax, ax = ax)
        # plt.gca().invert_yaxis()
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="5%", pad=0.05)
        cbar = plt.colorbar(im, cax=cax)
        if (font_size is not None):
            cbar.ax.tick_params(labelsize=font_size)
        # extent = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
        # fig.subplots_adjust(bottom = 0)
        # fig.subplots_adjust(top = 1)
        # fig.subplots_adjust(right = 1)
        # fig.subplots_adjust(left = 0)
        # manager = plt.get_current_fig_manager()
        # manager.resize(*manager.window.maxsize())
        #fig.tight_layout()
        if (fig_size is not None):
            fig.set_size_inches(fig_size[0], fig_size[1])
        plt.savefig(fn, bbox_inches='tight', pad_inches=0, transparent=True, dpi=dpi)
        # plt.savefig("test.png", bbox_inches=extent)
    else:
        fig = plt.figure(frameon=False)
        fig.set_size_inches(float(mtr.shape[1]) / float(dpi), float(mtr.shape[0]) / float(dpi))
        ax = plt.Axes(fig, [0., 0., 1., 1.])
        if (title is not None):
            ax.set_title(title, fontsize = font_size)
        ax.set_axis_off()
        plt.set_cmap(cm)
        fig.add_axes(ax)
        im = ax.imshow(mtr)
        if (draw_point is not None):
            plt.plot(draw_point[1]*float(mtr.shape[1]), draw_point[0]*float(mtr.shape[0]), color='red', marker='+', linewidth=2, markersize=10)
        extent = im.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
        plt.savefig(fn, bbox_inches=extent, dpi=dpi)
    plt.close()


def contourf_with_colorbar_and_save(mtr, x1, x2, fn, cm = 'viridis', font_size = None, dpi=200):
    fig = plt.figure()
    ax = fig.add_subplot(111)
    if (font_size is not None):
        ax.tick_params(labelsize=font_size)
    plt.set_cmap(cm)
    plt.axis('equal')
    im = ax.contourf(x1, x2, mtr, 100, zorder=-20)
    # Use zorder to reduce file size
    ax.set_rasterization_zorder(-10)
    #plt.xlabel('f1', fontsize=16)
    #plt.ylabel('f2', fontsize=16)
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(im, cax=cax)
    if (font_size is not None):
        cbar.ax.tick_params(labelsize=font_size)
    fig.tight_layout()
    plt.savefig(fn, bbox_inches='tight', pad_inches=0, transparent=True, dpi=dpi)
    plt.close()


def draw_func_interpolation_along_time(fs, ts, f_name, temp_path, points, mesh_x1, mesh_x2, fig_size = None, dpi=200):
    ensure_path_exists(temp_path)

    fns = []
    for i, t in enumerate(ts):
        curr_f = fs[i]

        v = interpolate_surf_image(curr_f, points, mesh_x1, mesh_x2)
        filename = temp_path + '/' + str(t).zfill(8) + '.png'
        draw_image_and_save(v, filename, title = str(t).zfill(8), fig_size = fig_size, font_size=15, dpi=dpi)
        fns.append(filename)

    # Create video with drawings
    create_video_from_images(fns, f_name, 5)
    remove_if_path_exists(temp_path)


def merge_two_dicts(x, y):
    z = x.copy()
    z.update(y)
    return z


def add_content_to_npz(npz_file_name, new_content):
    npz = np.load(npz_file_name)
    old_content = dict()
    for f in npz.files:
        old_content[f] = npz[f]

    content = merge_two_dicts(old_content, new_content)
    np.savez(npz_file_name, **content)


def create_video_from_images(image_fns, video_fn, framerate):
    frame = cv2.imread(image_fns[0])
    height, width, layers = frame.shape
    video = cv2.VideoWriter(video_fn, cv2.VideoWriter_fourcc(*'XVID'), framerate, (width, height))

    for fn in image_fns:
        curr_im = cv2.imread(fn)
        curr_height, curr_width, curr_layers = curr_im.shape
        if (curr_height != height or curr_width != width):
            curr_im = cv2.resize(curr_im, (width, height))
        video.write(curr_im)

    cv2.destroyAllWindows()
    video.release()


def fetch_all_tensor_data(tnsr, sess):
    data_list = []
    while (True):
        try:
            data = sess.run(tnsr)
            data_list.append(data)
        except tf.errors.OutOfRangeError:
            break

    return np.concatenate(data_list, axis = 0)


def compute_rel_energy(v, projs):
    vvv_l2 = np.sum(np.square(v))
    vvv_projections_energy = np.square(projs)
    vvv_projections_acc_energy = np.cumsum(vvv_projections_energy)
    vvv_projections_rel_acc_energy = vvv_projections_acc_energy / vvv_l2
    return vvv_projections_rel_acc_energy


def running_mean(x, N):
    #cumsum = np.cumsum(np.insert(x, 0, 0))
    #return (cumsum[N:] - cumsum[:-N]) / float(N)
    return np.convolve(x, np.ones((N,))/float(N), mode='same')


def reduce_dataset_size(train_images, train_labels, dataset_size):
    class_num = np.unique(train_labels).shape[0]
    points_per_class = int(dataset_size/class_num)

    points_to_use = []
    labels_to_use = []
    for curr_class in range(class_num):
        point_indeces = np.where(train_labels == curr_class)[0]
        point_indeces_to_use = point_indeces[0:points_per_class]
        curr_points = train_images[point_indeces_to_use]
        curr_labels = train_labels[point_indeces_to_use]
        points_to_use.append(curr_points)
        labels_to_use.append(curr_labels)
    reduced_train_images = np.concatenate(points_to_use)
    reduced_train_labels = np.concatenate(labels_to_use)
    return [reduced_train_images, reduced_train_labels]


def is_between(x, a, b):
    return tf.logical_and(tf.less_equal(a, x), tf.less(x, b))


def move_model_to_new_theta(new_theta_vec, tf_model_vars, sess):
    curr_index = 0;
    ops = []
    for v in tf_model_vars:
        dims = v.get_shape().as_list()
        if len(dims) == 0:
            ll = 1
        else:
            ll = np.prod(dims)
        new_value = new_theta_vec[curr_index:curr_index + ll]
        new_value = tf.reshape(new_value, dims)
        op = tf.assign(v, new_value)
        ops.append(op)
        curr_index = curr_index + ll
    sess.run(ops)


class PartialFormatter(string.Formatter):
    def __init__(self, missing='None', bad_fmt='!!'):
        self.missing, self.bad_fmt=missing, bad_fmt

    def get_field(self, field_name, args, kwargs):
        # Handle a key not found
        try:
            val=super(PartialFormatter, self).get_field(field_name, args, kwargs)
            # Python 3, 'super().get_field(field_name, args, kwargs)' works
        except (KeyError, AttributeError):
            val=None,field_name
        return val

    def format_field(self, value, spec):
        # handle an invalid format
        if value==None: return self.missing
        try:
            return super(PartialFormatter, self).format_field(value, spec)
        except ValueError:
            if self.bad_fmt is not None: return self.bad_fmt
            else: raise


def construct_sqr_loss_from_data(points, labels, model, dtype):
    X_tensor = tf.convert_to_tensor(points, dtype=dtype)
    Y_tensor = tf.convert_to_tensor(labels, dtype=dtype)

    sqr_loss = construct_sqr_loss_from_tensors(X_tensor, Y_tensor, model)

    return sqr_loss


def construct_sqr_loss_from_tensors(X_tensor, Y_tensor, model):
    nn_output = model.create_model_graph(X_tensor)
    sqr_errors = tf.square(nn_output - Y_tensor)
    sqr_loss = 0.5*tf.reduce_mean(sqr_errors)

    return sqr_loss
