import numpy as np
from utils import *
from image_utils import *
import scipy
from skimage import color


########################################################################


class ImageFnctn:
    """
    """

    def __init__(self, file_name):
        self.floatImage = read_float_image(file_name)

        if (self.floatImage.ndim > 2 and self.floatImage.shape[2] == 3):
            self.floatImage = color.rgb2gray(self.floatImage)

        self.dim = 2;
        self.imageDims = self.floatImage.shape
        self.imagePoints = [np.linspace(0.0, 1.0, d, endpoint = True, dtype = np.float32) for d in self.imageDims]
        
        xi = np.trapz(self.floatImage, x = self.imagePoints[0], axis = 0)
        self.imageVol = np.trapz(xi, x = self.imagePoints[1], axis = 0)
        self.normImage = self.floatImage/self.imageVol
        self.surfInterpolator = scipy.interpolate.RegularGridInterpolator(self.imagePoints, 
                                                                          self.normImage, 
                                                                          bounds_error=False,
                                                                          fill_value=0.0)


    def func_output(self, points):
        outputs = self.surfInterpolator(points)
        outputs = outputs[:, np.newaxis]
        
        return outputs

########################################################################


