import numpy as np
import tensorflow as tf
import sys
from utils import *
import threading


########################################################################


class NumpyGenerator:
    """
    """

    def __init__(self, vars_dict, 
                       batch_size = 300, 
                       dtype_map = None, 
                       do_shuffle = True, 
                       is_infinite_loop_mode = False, 
                       sample_with_replacement = False,
                       make_thread_safe = False,
                       hide_batch_size_dim = True):
        #self.vars_dict = vars_dict
        self.batch_size = batch_size
        self.dtype_map = dtype_map
        self.is_batch_mode = self.batch_size > 1
        self.hide_batch_size_dim = hide_batch_size_dim
        self.is_one_row_mode = self.batch_size <= 1
        self.is_infinite_loop_mode = is_infinite_loop_mode
        self.do_shuffle = do_shuffle
        self.sample_with_replacement = sample_with_replacement
        self.make_thread_safe = make_thread_safe
        
        if (self.make_thread_safe):
            self.lock = threading.Lock()
            
        # Convert data to tensors        
        self.matrices = []
        self.keys = []
        self._types = []
        self._shapes = []
        self.slicers = []
        for key in vars_dict:
            curr_value = vars_dict[key]
            if type(curr_value) is not np.ndarray:
                curr_value = np.array(curr_value)
            
            self.dtype_map[key]
            if (self.dtype_map is not None and key in self.dtype_map and curr_value.dtype != self.dtype_map[key]):
                curr_value = curr_value.astype(self.dtype_map[key])
            
            self.matrices.append(curr_value)
            self.keys.append(key)
            self._types.append(npdtype_2_tfdtype(curr_value.dtype))
            curr_shape = list(curr_value.shape)
            if (self.is_batch_mode):
                if (self.hide_batch_size_dim):
                    curr_shape[0] = None
                else:
                    curr_shape[0] = self.batch_size
            else:
                curr_shape = curr_shape[1:]
            self._shapes.append(tf.TensorShape(curr_shape))

        self.rows_Num = self.matrices[0].shape[0]
        if (self.rows_Num == self.batch_size):
            self.do_shuffle = False
            
        self.mat_Num = len(self.matrices)
        self.types = {self.keys[i]:self._types[i] for i in range(self.mat_Num)}
        self.shapes = {self.keys[i]:self._shapes[i] for i in range(self.mat_Num)}
        
        if (self.rows_Num < self.batch_size):
            raise ValueError("Dataset size must be bigger than batch size!")

        if (self.is_infinite_loop_mode):
            self.curr_row_index = 0;
            self.sample_indeces = np.arange(self.rows_Num)
        else:
            permIndeces = np.arange(self.rows_Num)
            if (self.do_shuffle):
                # Shuffle rows        
                np.random.shuffle(permIndeces)
            
            # Augment matrices to have specific number of full batches
            self.batch_Num = int(np.ceil(1.0*self.rows_Num/self.batch_size))
            req_rows_Num = self.batch_Num*self.batch_size;
            miss_rows_Num = req_rows_Num - self.rows_Num 
            if (miss_rows_Num > 0):
                permIndeces = np.concatenate((permIndeces, permIndeces[0:miss_rows_Num]))
    
            for m in range(self.mat_Num):
                self.matrices[m] = self.matrices[m][permIndeces]


    def __call__(self):
        if (self.is_batch_mode and not self.is_infinite_loop_mode):
            for i in range(self.batch_Num):
                start_ind = i*self.batch_size
                end_ind = start_ind + self.batch_size
                yield {self.keys[m]:self.matrices[m][start_ind:end_ind] for m in range(self.mat_Num)}
        elif (self.is_one_row_mode and not self.is_infinite_loop_mode):
            for i in range(self.rows_Num):
                yield {self.keys[m]:self.matrices[m][i] for m in range(self.mat_Num)}
        elif (self.is_infinite_loop_mode):
            while (True):
                if (self.make_thread_safe):
                    self.lock.acquire()
                    try:
                        curr_res = self.retrieve_next_batch()
                        yield curr_res
                    finally:
                        self.lock.release()
                else:
                    curr_res = self.retrieve_next_batch()
                    yield curr_res
                    
        elif (self.sample_with_replacement):
            while (True):
                row_indeces = np.random.choice(self.rows_Num, self.batch_size)
                curr_res = {self.keys[m]:self.matrices[m][row_indeces] for m in range(self.mat_Num)}
                yield curr_res


    def retrieve_next_batch(self, batch_size = None):
        if (batch_size is None):
            batch_size = self.batch_size
        
        start_index = self.curr_row_index
        end_index = start_index + batch_size
        
        started_again = False
        if (end_index > self.rows_Num):
            if (start_index == self.rows_Num):
                start_index = 0
                end_index = start_index + batch_size
                row_indeces = np.r_[start_index:end_index]
            else:
                end_index = end_index % self.rows_Num
                row_indeces = np.r_[start_index:self.rows_Num,0:end_index]
            started_again = True
        else:
            row_indeces = np.r_[start_index:end_index]

        
        ind = self.sample_indeces[row_indeces]
        curr_res = {self.keys[m]:self.matrices[m][ind] for m in range(self.mat_Num)}
        self.curr_row_index = end_index

        # If finished passing over buffer data            
        if (started_again and self.do_shuffle):
            # Then shuffle buffer rows
            #print('\n \nFinished passing over full buffer data.')
            #print(np.mean(self.matrices[0][:,0]))
            #print('Shuffle it and start again.')
            np.random.shuffle(self.sample_indeces)
            #permIndeces = np.arange(self.rows_Num)
            #np.random.shuffle(permIndeces)
            #for m in range(self.mat_Num):
            #    self.matrices[m] = self.matrices[m][permIndeces]
                
        return curr_res


########################################################################


