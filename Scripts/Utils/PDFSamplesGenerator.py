import numpy as np
import tensorflow as tf
import sys
import math
from utils import *
from scipy.stats import norm
from BaseAggregateSamplesGenerator import *
from RejectionSampler import *


########################################################################


class PDFSamplesGenerator(BaseAggregateSamplesGenerator):
    """
    """

    def __init__(self, var_dim, pdf_func, batch_size = 300, aggr_max_size = 300000000, sample_size = 200000, M = 20.0):
        self.var_dim = var_dim
        self.pdf_func = pdf_func
        self.M = M
        self.sampler = RejectionSampler()
        BaseAggregateSamplesGenerator.__init__(self, (None, self.var_dim), batch_size = batch_size, aggr_max_size = aggr_max_size, sample_size = sample_size)


    def sample_distribution(self, samples_min_num):
        points, pdf_values = self.sampler.sample_pdf(self.pdf_func, self.var_dim, self.M, samples_min_num);
        return points, pdf_values

                
########################################################################


