import numpy as np
import tensorflow as tf
import sys
import math
import time

version3 = (3, 0)
cur_version = sys.version_info
if cur_version >= version3:
    import _thread as thread
else:
    import thread as thread

from utils import *
from scipy.stats import norm


########################################################################


class BaseAggregateSamplesGenerator:
    """
    """

    def __init__(self, data_shape, batch_size = 300, aggr_max_size = 300000000, sample_size = 200000):
        self.batch_size = batch_size
        self.types = {'data' : tf.float32, 'pdf_values' : tf.float32}
        self.shapes = {'data' : data_shape, 'pdf_values' : (None, 1)}
        self.data_buffer = None
        self.pdf_values_buffer = None
        self.curr_row_index = 0;
        self.aggr_max_size = aggr_max_size
        
        # Get data for first batches
        self.sample_size = sample_size
        self.data_buffer, self.pdf_values_buffer = self.sample_distribution(self.sample_size)
        self.reduce_buffer_if_needed();
        
        # Opening thread that samples more data
        self.sampled_pairs = []
        thread.start_new_thread(self.load_data, ())


    def __call__(self):
        while (True):
            end_index = self.curr_row_index + self.batch_size
            if (end_index > self.data_buffer.shape[0]):
                self.unite_sampled_data()

            start_index = self.curr_row_index
            end_index = start_index + self.batch_size
            
            started_again = False
            if (end_index > self.data_buffer.shape[0]):
                end_index = end_index % self.data_buffer.shape[0]
                row_indeces = np.r_[start_index:self.data_buffer.shape[0],0:end_index]
                started_again = True
            else:
                row_indeces = np.r_[start_index:end_index]

            points = self.data_buffer[row_indeces]
            pdf_values = self.pdf_values_buffer[row_indeces]
            self.curr_row_index = end_index

            # If finished passing over buffer data            
            if (started_again):
                if (self.is_buffer_full()):
                    # Then shuffle buffer rows
                    print('\n \nFinished passing over full buffer data.')
                    print(np.mean(self.data_buffer[:,0]))
                    print('Shuffle it and start again.')
                    permIndeces = np.arange(self.data_buffer.shape[0])
                    np.random.shuffle(permIndeces)
                    self.data_buffer = self.data_buffer[permIndeces]
                    self.pdf_values_buffer = self.pdf_values_buffer[permIndeces]
                else:
                    print('Start dataset from beginning again')
            
            yield {'data' : points, 'pdf_values' : pdf_values}


    def is_buffer_full(self):
        res = self.get_current_buffer_size() >= self.aggr_max_size
        return res
    
        
    def load_data(self):
        points, pdf_values = self.sample_distribution(self.sample_size)
        self.sampled_pairs.append((points, pdf_values))
        
        if (not self.is_buffer_full()):
            thread.start_new_thread(self.load_data, ())


    def unite_sampled_data(self):
        if (len(self.sampled_pairs) > 0):
            sampled_pairs = list(self.sampled_pairs)
            self.sampled_pairs = []
            
            sampled_data_list = [p[0] for p in sampled_pairs]
            all_data = [self.data_buffer] + sampled_data_list
            self.data_buffer = np.concatenate(all_data, axis = 0)
            
            sampled_pdf_values_list = [p[1] for p in sampled_pairs]
            all_pdf_values = [self.pdf_values_buffer] + sampled_pdf_values_list
            self.pdf_values_buffer = np.concatenate(all_pdf_values, axis = 0)

            if (self.data_buffer.shape[0] != self.pdf_values_buffer.shape[0]):
                print('Length of data and pdf_values is different!!!')
                exit()
            
            self.reduce_buffer_if_needed();

            print('\n\n United. New size is:')
            print(self.data_buffer.shape[0])
            

    def reduce_buffer_if_needed(self):
        if (self.data_buffer.shape[0] > self.aggr_max_size):
            self.data_buffer = self.data_buffer[0:self.aggr_max_size,:]
            self.pdf_values_buffer = self.pdf_values_buffer[0:self.aggr_max_size,:]

        
    def get_current_buffer_size(self):
        return self.data_buffer.shape[0]


    def wait_till_samples_generated(self, samples_num):
        while (self.get_current_buffer_size() < samples_num):
            time.sleep(5)
            self.unite_sampled_data()
    
        
    def sample_distribution(self, samples_min_num):
        raise NotImplementedError("Please Implement this method")
        
########################################################################


