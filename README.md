# nn_spectrum_decomposition

Basic code to analyze NN spectrum


Instructions:

1. Execute "train_model_regression.py" to train the model f_{\theta}(X) with L2 loss. Alternatively, run "train_model_nce.py" to perform NCE approach. Main learning settings can be configured via 'train.properties'.
2. Execute "compute_eigs_dcmpstn.py" and "compute_additional_prjs.py" to calculate Gramian spectrum along the optimization and to save it on disk. 
3. Execute "compute_analytics.py" to compute all the figures of the paper (e.g. eigenvectors at different times, relative energy of various signals, evolution of eigenvalues, etc.).

Otherwise, execute "run_all.py" to run all the above scripts sequentially. May take several days to accomplish. 

Required libraries:
- TensorFlow, was tested on version 1.10

In case of additional questions, please ask them via e-mail:
dimkak@gmail.com
